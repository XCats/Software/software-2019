import math



def calculate(distance, camera_angle):
    target_height = 28.5
    camera_height = 44.25
    
    angle_offset = math.degrees(math.atan((target_height - camera_height) / distance)) - camera_angle
    print angle_offset
    return angle_offset
    
# calculate(41.5, -14.8)

sum = 0
sum += calculate(32 + 5/16., -19.35)
sum += calculate(52.125, -9.73)
sum += calculate(67 + 5/8., -5.96)
sum += calculate(75 + 3.16, -4.727)

print "Answer: ", (sum / 4)


# calculate(5.52, 40.441)