package org.xcats.frc.deep_space;

import java.util.ArrayList;
import java.util.List;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;

public class CameraSimulator extends BaseCameraSimulator {
	private static final double CARGO_SIDE_X_OFFSET = 27.5;
	private static final double CARGO_FRONT_X_OFFSET = 10;

	private static final double CARGO_SLOT_1_Y = 20;
	private static final double CARGO_SLOT_2_Y = 42;
	private static final double CARGO_SLOT_3_Y = 64;
	private static final double CARGO_SLOT_FRONT_Y = 105;

	private static final double ROCKET_SHIP_CENTER_Y = 95;
	private static final double ROCKET_SHIP_OFFSET_CLOSE_Y = 81;
	private static final double ROCKET_SHIP_OFFSET_FAR_Y = 111;

	private static final double ROCKET_SHIP_CENTER_X = 134;
	private static final double ROCKET_SHIP_OFFSET_X = 144;

	private static final double HP_STATION_Y = 324;
	private static final double HP_STATION_OFFSET = 135;

	private static final double CAMERA_FOV = 50;
	private static final double CAMERA_MAX_DISTANCE = 120;

	private static final List<TargetLocation> VISION_TARGETS;

	private final Robot mRobot;
	private final NetworkTable mMockLimelightTable;

	static {
		VISION_TARGETS = new ArrayList<>();

		// Red
		VISION_TARGETS.add(new TargetLocation("Red Cargo  S1E", CARGO_SIDE_X_OFFSET, -CARGO_SLOT_1_Y, -90, CARGO_SIDE_X_OFFSET, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Red Cargo  S2E", CARGO_SIDE_X_OFFSET, -CARGO_SLOT_2_Y, -90, CARGO_SIDE_X_OFFSET, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Red Cargo  S3E", CARGO_SIDE_X_OFFSET, -CARGO_SLOT_3_Y, -90, CARGO_SIDE_X_OFFSET, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Red Cargo  S1W", -CARGO_SIDE_X_OFFSET, -CARGO_SLOT_1_Y, 90, null, null, -CARGO_SIDE_X_OFFSET, null));
		VISION_TARGETS.add(new TargetLocation("Red Cargo  S2W", -CARGO_SIDE_X_OFFSET, -CARGO_SLOT_2_Y, 90, null, null, -CARGO_SIDE_X_OFFSET, null));
		VISION_TARGETS.add(new TargetLocation("Red Cargo  S3W", -CARGO_SIDE_X_OFFSET, -CARGO_SLOT_3_Y, 90, null, null, -CARGO_SIDE_X_OFFSET, null));
		VISION_TARGETS.add(new TargetLocation("Red Cargo  FW ", -CARGO_FRONT_X_OFFSET, -CARGO_SLOT_FRONT_Y, -180, null, null, null, -ROCKET_SHIP_CENTER_Y));
		VISION_TARGETS.add(new TargetLocation("Red Cargo  FE ", CARGO_FRONT_X_OFFSET, -CARGO_SLOT_FRONT_Y, -180, null, null, null, -ROCKET_SHIP_CENTER_Y));
		VISION_TARGETS.add(new TargetLocation("Red Rocket WN ", -ROCKET_SHIP_OFFSET_X, -ROCKET_SHIP_OFFSET_CLOSE_Y, 0, null, null, 0.0, null));
		VISION_TARGETS.add(new TargetLocation("Red Rocket WC ", -ROCKET_SHIP_CENTER_X, -ROCKET_SHIP_CENTER_Y, 0, null, null, 0.0, null));
		VISION_TARGETS.add(new TargetLocation("Red Rocket WS ", -ROCKET_SHIP_OFFSET_X, -ROCKET_SHIP_OFFSET_FAR_Y, 0, null, null, 0.0, null));
		VISION_TARGETS.add(new TargetLocation("Red Rocket EN ", ROCKET_SHIP_OFFSET_X, -ROCKET_SHIP_OFFSET_CLOSE_Y, 0, 0.0, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Red Rocket EC ", ROCKET_SHIP_CENTER_X, -ROCKET_SHIP_CENTER_Y, 0, 0.0, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Red Rocket ES ", ROCKET_SHIP_OFFSET_X, -ROCKET_SHIP_OFFSET_FAR_Y, 0, 0.0, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Red Rocket E  ", HP_STATION_OFFSET, -HP_STATION_Y, 0, null, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Red Rocket W  ", -HP_STATION_OFFSET, -HP_STATION_Y, 0, null, null, null, null));

		// Blue
		VISION_TARGETS.add(new TargetLocation("Blue Cargo  S1E", CARGO_SIDE_X_OFFSET, CARGO_SLOT_1_Y, -90, CARGO_SIDE_X_OFFSET, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Blue Cargo  S2E", CARGO_SIDE_X_OFFSET, CARGO_SLOT_2_Y, -90, CARGO_SIDE_X_OFFSET, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Blue Cargo  S3E", CARGO_SIDE_X_OFFSET, CARGO_SLOT_3_Y, -90, CARGO_SIDE_X_OFFSET, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Blue Cargo  S1W", -CARGO_SIDE_X_OFFSET, CARGO_SLOT_1_Y, 90, null, null, -CARGO_SIDE_X_OFFSET, null));
		VISION_TARGETS.add(new TargetLocation("Blue Cargo  S2W", -CARGO_SIDE_X_OFFSET, CARGO_SLOT_2_Y, 90, null, null, -CARGO_SIDE_X_OFFSET, null));
		VISION_TARGETS.add(new TargetLocation("Blue Cargo  S3W", -CARGO_SIDE_X_OFFSET, CARGO_SLOT_3_Y, 90, null, null, -CARGO_SIDE_X_OFFSET, null));
		VISION_TARGETS.add(new TargetLocation("Blue Cargo  FW ", -CARGO_FRONT_X_OFFSET, CARGO_SLOT_FRONT_Y, 180, null, ROCKET_SHIP_CENTER_Y, null, null));
		VISION_TARGETS.add(new TargetLocation("Blue Cargo  FE ", CARGO_FRONT_X_OFFSET, CARGO_SLOT_FRONT_Y, 180, null, ROCKET_SHIP_CENTER_Y, null, null));
		VISION_TARGETS.add(new TargetLocation("Blue Rocket WN ", -ROCKET_SHIP_OFFSET_X, ROCKET_SHIP_OFFSET_CLOSE_Y, 0, null, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Blue Rocket WC ", -ROCKET_SHIP_CENTER_X, ROCKET_SHIP_CENTER_Y, 0, null, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Blue Rocket WS ", -ROCKET_SHIP_OFFSET_X, ROCKET_SHIP_OFFSET_FAR_Y, 0, null, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Blue Rocket EN ", ROCKET_SHIP_OFFSET_X, ROCKET_SHIP_OFFSET_CLOSE_Y, 0, null, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Blue Rocket EC ", ROCKET_SHIP_CENTER_X, ROCKET_SHIP_CENTER_Y, 0, null, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Blue Rocket ES ", ROCKET_SHIP_OFFSET_X, ROCKET_SHIP_OFFSET_FAR_Y, 0, null, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Blue Rocket E  ", HP_STATION_OFFSET, HP_STATION_Y, 0, null, null, null, null));
		VISION_TARGETS.add(new TargetLocation("Blue Rocket W  ", -HP_STATION_OFFSET, HP_STATION_Y, 0, null, null, null, null));
	}

	public CameraSimulator(Robot aRobot) {
		super(CAMERA_FOV);

		mRobot = aRobot;
		mMockLimelightTable = NetworkTableInstance.getDefault().getTable("limelight");
	}

	@SuppressWarnings("PMD")
	public void update() {
		double robotX = mRobot.getProcessorManager().getPositioner().getX();
		double robotY = mRobot.getProcessorManager().getPositioner().getY();
		double robotAngle = mRobot.getProcessorManager().getPositioner().getAngle();

		double minDistance = Double.MAX_VALUE;
		TargetDeltaStruct bestTargetDelta = null;

		for (TargetLocation loc : VISION_TARGETS) {

			double dx = loc.mX - robotX;
			double dy = loc.mY - robotY;
			double dAngle = Math.toDegrees(Math.atan2(dx, dy));
			double angleFromRobot = dAngle - robotAngle;

			while (angleFromRobot < -180) {
				angleFromRobot += 360;
			}

			while (angleFromRobot > 180) {
				angleFromRobot -= 360;
			}

			TargetDeltaStruct deltaStruct = new TargetDeltaStruct();
			deltaStruct.mDistance = Math.sqrt(dx * dx + dy * dy);
			deltaStruct.mAngle = angleFromRobot;

			if (isTargetValid(deltaStruct, robotX, robotY, robotAngle, loc) == RejectionReason.VALID) {
				if (deltaStruct.mDistance < minDistance && deltaStruct.mDistance < CAMERA_MAX_DISTANCE) {
					bestTargetDelta = deltaStruct;
					minDistance = deltaStruct.mDistance;
				}
			}
		}

		if (bestTargetDelta == null) {
			mMockLimelightTable.getEntry("tv").setNumber(0);
		} else {
			double el = Math.toDegrees(
					Math.atan2(DeepSpaceProperties.CAMERA_TO_TARGET_HEIGHT.getValue(), bestTargetDelta.mDistance));
			el -= DeepSpaceProperties.CAMERA_EL_ANGLE_OFFSET.getValue();
			double az = bestTargetDelta.mAngle;
			mMockLimelightTable.getEntry("tv").setNumber(1);
			mMockLimelightTable.getEntry("tx").setNumber(az);
			mMockLimelightTable.getEntry("ty").setNumber(el);
		}

	}
}
