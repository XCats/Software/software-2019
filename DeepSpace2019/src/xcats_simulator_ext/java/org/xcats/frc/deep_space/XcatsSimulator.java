package org.xcats.frc.deep_space;

import com.snobot.simulator.ASimulator;
import com.snobot.simulator.SensorActuatorRegistry;
import com.snobot.simulator.robot_container.IRobotClassContainer;
import com.snobot.simulator.robot_container.JavaRobotContainer;
import com.snobot.simulator.simulator_components.smart_sc.BaseCanSmartSpeedController;

public class XcatsSimulator extends ASimulator {

	private CameraSimulator mCameraSimulator;
	private ClimberSimulator mClimber;

	@Override
	public void setRobot(IRobotClassContainer aRobot) {
		setRobot((Robot) ((JavaRobotContainer) aRobot).getJavaRobot());
	}

	public void setRobot(Robot aRobot) {
		mCameraSimulator = new CameraSimulator(aRobot);

		mClimber = new ClimberSimulator(SensorActuatorRegistry.get().getGyros().get(213),
				SensorActuatorRegistry.get().getSpeedControllers()
						.get(PortMappings.LEFT_NEO + BaseCanSmartSpeedController.sCAN_SC_OFFSET));
	}

	@Override
	public void update() {
		mCameraSimulator.update();
		mClimber.update();
	}

}
