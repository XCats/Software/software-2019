package org.xcats.frc.deep_space;

import com.snobot.simulator.module_wrapper.interfaces.IGyroWrapper;
import com.snobot.simulator.module_wrapper.interfaces.IPwmWrapper;

public class ClimberSimulator {
	private final IGyroWrapper mTiltGyro;
	private final IPwmWrapper mFourbarMotor;

	public ClimberSimulator(IGyroWrapper tiltGyro, IPwmWrapper fourbarMotor) {
		mTiltGyro = tiltGyro;
		mFourbarMotor = fourbarMotor;
	}

	public void update() {
		mTiltGyro.setAngle(mFourbarMotor.getPosition());
	}

}
