package org.xcats.frc.deep_space.auto;

import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Supplier;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xcats.frc.deep_space.DeepSpaceProperties;
import org.xcats.frc.deep_space.ProcessorManager;
import org.xcats.frc.deep_space.SubsystemManager;
import org.xcats.frc.deep_space.auto.commands.DoNothing;
import org.xcats.frc.deep_space.auto.mode_factories.ScriptedAutonomousModesFactory;

import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class AutonomousFactory {

	private static final Logger LOGGER = LogManager.getLogger(AutonomousFactory.class);

	private final SendableChooser<Supplier<AutoBase>> mAutonChooser;

	public AutonomousFactory(SubsystemManager subsystemManager, ProcessorManager processManager) {
		mAutonChooser = new SendableChooser<>();

		mAutonChooser.addOption("DO NOTHING", this::getDoNothing);

		if (DeepSpaceProperties.ADD_TEST_AUTONS.getValue()) {
			ScriptedAutonomousModesFactory scriptedFactory = new ScriptedAutonomousModesFactory(subsystemManager,
					processManager);
			addModes(scriptedFactory.getScriptedModes());
		}

		SmartDashboard.putData("Autonomous Mode", mAutonChooser);
	}

	private AutoBase getDoNothing() {
		return new DoNothing(false);
	}

	@SuppressWarnings("PMD.UnusedPrivateMethod") // Bug in PMD
	private void addModes(Map<String, Supplier<AutoBase>> autoModesMap) {
		for (Entry<String, Supplier<AutoBase>> pair : autoModesMap.entrySet()) {
			mAutonChooser.addOption(pair.getKey(), pair.getValue());
		}
	}

	public AutoBase getSelectedMode() {
		Supplier<AutoBase> selectedModeFunction = mAutonChooser.getSelected();
		if (selectedModeFunction != null) {
			return selectedModeFunction.get();
		}

		LOGGER.log(Level.WARN, "No auto mode selected!");
		return null;
	}
}
