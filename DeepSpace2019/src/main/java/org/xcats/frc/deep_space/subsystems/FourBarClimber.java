package org.xcats.frc.deep_space.subsystems;

import org.xcats.frc.deep_space.DeepSpaceProperties;
import org.xcats.frc.lib.XSubsystem;
import org.xcats.frc.lib.devices.sensors.XEncoder;
import org.xcats.frc.lib.devices.sensors.XLimitSwitch;
import org.xcats.frc.lib.logging.XLogString;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.SpeedController;

/**
 * This class contains the code required to run the Four Bar Linkage-based climber on the 2019 Destination Deep Space
 * robot.
 */
public class FourBarClimber implements XSubsystem {

	/**
	 * A controller class that contains all variables that are modified to control
	 * the subsystem
	 */
	public class Controller {
		private double mSpeed;

		/**
		 * Sets the speed of the lifting motors
		 * @param speed the new speed (-1 to 1)
		 */
		public void setSpeed(double speed) {
			this.mSpeed = speed;
		}
	}

	/**
	 * A state class is built that will represent the state of the subsystem
	 */
	public class State {
		private double mSpeed;
		private double mEncoderValue;
		private boolean mLimited;

		public double getSpeed() {
			return mSpeed;
		}

		public double getEncoderValue() {
			return mEncoderValue;
		}

		public boolean getLimited() {
			return mLimited;
		}

//		public Lifter.State getLifterState() {
//			return mLifter.getCurrentState();
//		}


	}

	// Holds the desired state of the subsystem
	private final State mDesiredState = new State();

	// Holds the actual state of the subsystem
	private final State mCurrentState = new State();

	private final Controller mController = new Controller();

	// Injected Dependencies

	/**
	 * Creates a new FourBarClimber object, based on injected dependencies
	 * @param motor the Speed Controller for the lift motor
	 * @param encoder the encoder in the neo on the linkage
	 */
	private final XEncoder mEncoder;

	private final SpeedController mMotor;

	private final XLimitSwitch mLimitSwitch;

	private final NetworkTable mNetworkTable;


	public FourBarClimber(SpeedController motor, XEncoder encoder, XLimitSwitch limitSwitch) {
		this.mMotor = motor;
		this.mEncoder = encoder;
		this.mLimitSwitch = limitSwitch;
//		this.mLifter = lifter;

		mNetworkTable = NetworkTableInstance.getDefault().getTable("RobotData").getSubTable("Lift Mechanism");
	}

	public State getDesiredState() {
		return this.mDesiredState;
	}

	public State getCurrentState() {
		return this.mCurrentState;
	}

	/**
	 * Gets the controller of the climber, which may then be used to control it.
	 * @return the controller object for the climber
	 */
	public Controller getController() {
		return this.mController;
	}

	@Override
	public void stop() {
		// No motion state here
	}

	@Override
	public void initialize() {
		/*
		 * This is where mDesiredState should be set to whatever the default state for the subsystem
		 * should be, and mActualState should be initialized with values read from the
		 * subsystem itself, or with values from mDesiredState.
		 */
	}

	@Override
	public XLogString getLogString() {
		return null;
	}

	/**
	 * Reads the speed values from the controller into the desired state
	 *
	 * Reads the speed values from the encoder into the current state
	 */
	@Override
	public void getPeriodicInput() {
		// Read values from controller into the desired state
		this.mDesiredState.mSpeed = this.getController().mSpeed;

		// Read value from sensors into current state (because this is current)
		this.mCurrentState.mEncoderValue = this.mEncoder.getLinearDistance();
		this.mCurrentState.mLimited = this.mLimitSwitch.getCurrentValue();

//		this.mLifter.getPeriodicInput();
	}

	/**
	 * Sets the output of the lift motors based on the values in the desired state.
	 */
	@Override
	public void writePeriodicOutput() {
		if (this.mCurrentState.mLimited && this.mDesiredState.mSpeed < 0) {
			mMotor.set(0);
		} else {
			mMotor.set(this.mDesiredState.mSpeed);
		}

		this.mCurrentState.mSpeed = mMotor.get();

//		mLifter.writePeriodicOutput();

//		this.mCurrentState.mLifterState = this.mLifter.getCurrentState();

		if (DeepSpaceProperties.SMART_DASHBOARD_DEBUG.getValue()) {
			mNetworkTable.getEntry("Limit Switch").setBoolean(this.mCurrentState.mLimited);
		}
	}
}
