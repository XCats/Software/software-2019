package org.xcats.frc.deep_space.auto;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableValue;
import edu.wpi.first.networktables.TableEntryListener;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.xcats.frc.deep_space.processors.Positioner;
import org.xcats.frc.deep_space.utils.ObservableSendableChooser;

public class StartingPositionManager {

	private ObservableSendableChooser<StartingPosition> mPositionChooser;

	private static final double STARTING_X_OFFSET = 3.5 * 12;
	private static final double LEVEL1_Y = -22 * 12;
	private static final double LEVEL2_Y = -25 * 12;

	public enum StartingPosition {

		LEFT_LEVEL1_HAB(-STARTING_X_OFFSET, LEVEL1_Y, 0), 
		LEFT_LEVEL2_HAB(-STARTING_X_OFFSET, LEVEL2_Y, 0),
		CENTER_LEVEL1_HAB(0, LEVEL1_Y, 0), 
		RIGHT_LEVEL1_HAB(STARTING_X_OFFSET, LEVEL1_Y, 0),
		RIGHT_LEVEL2_HAB(STARTING_X_OFFSET, LEVEL2_Y, 0), 
		CENTER_FIELD(0, 0, 0);

		public final double mX;
		public final double mY;
		public final double mAngle;

		StartingPosition(double x, double y, double angle) {
			mX = x;
			mY = y;
			mAngle = angle;
		}
	}

	@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
	public StartingPositionManager(Positioner positioner) {

		mPositionChooser = new ObservableSendableChooser<>();
		for (StartingPosition position : StartingPosition.values()) {
			mPositionChooser.addOption(position.name(), position);
		}

		SmartDashboard.putData("Starting Position", mPositionChooser);
		mPositionChooser.addSelectionChangedListener(new TableEntryListener() {

			@Override
			public void valueChanged(NetworkTable table, String key, NetworkTableEntry entry, NetworkTableValue value,
					int flags) {
				StartingPosition position = mPositionChooser.getSelected();
				positioner.setPosition(position.mX, position.mY, position.mAngle);
			}
		});
	}

}
