package org.xcats.frc.deep_space;

import org.xcats.frc.deep_space.utils.MotionMagicPropertyManager;
import org.xcats.frc.deep_space.utils.PropertyManager;
import org.xcats.frc.deep_space.utils.PropertyManager.IProperty;

public class DeepSpaceProperties {

	// Debug
	public static final IProperty<Boolean> SMART_DASHBOARD_DEBUG = new PropertyManager.ConstantProperty<>("SmartDashboardDebug", true);

	////////////////////////////////////////////////
	// Autonomous
	////////////////////////////////////////////////
	// Autonomous Factory
	public static final IProperty<Boolean> ADD_TEST_AUTONS = new PropertyManager.ConstantProperty<>("AddTestAutons", true);

	// TurnToAngle
	public static final IProperty<Double> TURN_TO_ANGLE_KP = new PropertyManager.DoubleProperty("TurnToAngleKP", .02);
	public static final IProperty<Double> TURN_TO_ANGLE_KD = new PropertyManager.DoubleProperty("TurnToAngleKD", 0.0);

	// Pure Pursuit Driving
	public static final IProperty<Double> PURE_PURSUIT_KVFF = new PropertyManager.DoubleProperty("PurePursuitKff", 0.007);
	public static final IProperty<Double> PURE_PURSUIT_KVP = new PropertyManager.DoubleProperty("PurePursuitKp", 0.0001);
	public static final IProperty<Double> PURE_PURSUIT_DEFAULT_VELOCITY = new PropertyManager.DoubleProperty("PurePursuitDefaultVelocity", 84);
	public static final IProperty<Double> PURE_PURSUIT_DEFAULT_ACCELERATION = new PropertyManager.DoubleProperty("PurePursuitDefaultAcceleration", 72);

	// Drive To Position Auto
	public static final IProperty<Double> DRIVE_TO_POSITION_DISTANCE_KP = new PropertyManager.DoubleProperty("DriveToPointDistanceKp", 0.0);
	public static final IProperty<Double> DRIVE_TO_POSITION_ANGLE_KP = new PropertyManager.DoubleProperty("DriveToPointTurnKp", 0.0);

	// Path following
	public static final IProperty<Double> DRIVE_PATH_KP = new PropertyManager.DoubleProperty("PathKp", 0.01);
	public static final IProperty<Double> DRIVE_PATH_KV = new PropertyManager.DoubleProperty("PathKv", 0.0056);
	public static final IProperty<Double> DRIVE_PATH_KA = new PropertyManager.DoubleProperty("PathKa", 0.0035);
	public static final IProperty<Double> SPLINE_TURN_FACTOR = new PropertyManager.DoubleProperty("PathTurnKp", 0.028);

	// Limelight Driving
	public static final IProperty<Double> LIMELIGHT_TARGET_AREA_EXPECTED = new PropertyManager.DoubleProperty("LimelightExpectedTargetArea", 5.2);
	public static final IProperty<Double> LIMELIGHT_TARGET_AREA_TOLERANCE = new PropertyManager.DoubleProperty("LimelightTargetAreaTolerance", 0.01);
	public static final IProperty<Double> LIMELIGHT_TARGET_AREA_KP = new PropertyManager.DoubleProperty("LimelightTargetAreaKp", 0.01);
	public static final IProperty<Double> LIMELIGHT_YAW_ERROR_TOLERANCE = new PropertyManager.DoubleProperty("LimelightYawErrorTolerance", 0.01);
	public static final IProperty<Double> LIMELIGHT_YAW_ERROR_KP = new PropertyManager.DoubleProperty("LimelightYawErrorKp", 0.01);
	public static final IProperty<Double> LIMELIGHT_DISTANCE_MAX_ERROR = new PropertyManager.DoubleProperty("LimelightDistanceMaxError", 35); // inches
	public static final IProperty<Double> LIMELIGHT_DISTANCE_TOLERANCE = new PropertyManager.DoubleProperty("LimelightDistanceTolerance", 2);
	public static final IProperty<Double> LIMELIGHT_DISTANCE_KP = new PropertyManager.DoubleProperty("LimelightDistanceKp", 0.006);
	
	public static final IProperty<Double> LIMELIGHT_STEERING_KP = new PropertyManager.DoubleProperty("LimelightSteeringKp", 0.03);

	////////////////////////////////////////////////
	// Tele / Mechanisms
	////////////////////////////////////////////////

	// Camera
	public static final IProperty<Double> CAMERA_TO_TARGET_HEIGHT = new PropertyManager.DoubleProperty("CameraToTargetHeight", -15.75); // Inches
	public static final IProperty<Double> CAMERA_EL_ANGLE_OFFSET = new PropertyManager.DoubleProperty("CameraAngleOffset", -6.883); // Angle

	// Fourbar
	public static final IProperty<Double> XBOX_JS_DEADBAND = new PropertyManager.ConstantProperty<>("FourbarDeadband", .1); // out of 1
	public static final IProperty<Double> FOURBAR_COEFFICIENT = new PropertyManager.ConstantProperty<>("Fourbar Coeff", .25);

	// Cargo Aquisition
	public static final IProperty<Double> CARGO_ACQUISITION_SPEED = new PropertyManager.ConstantProperty<>("Cargo Intake Speed", -1.0);
	
	// Drivetrain
	public static final double DRIVETRAIN_DISTANCE_PER_ROTATION = 7.375 * Math.PI; // C = pi * d, in inches. Used on startup, so no point in making a property

	public static final IProperty<Double> DRIVER_JS_DEADBAND = new PropertyManager.ConstantProperty<>("Driver JS Deadband", 0.1);
	public static final IProperty<Double> DRIVER_JS_ADD = new PropertyManager.ConstantProperty<>("Driver JS Adjustment", 0.1);

	// Lead screw
	public static final IProperty<Double> LEAD_SCREW_SPEED = new PropertyManager.ConstantProperty<>("Leedscre Speed", 0.5);
	public static final IProperty<Double> LEAD_SCREW_DESIRED_ANGLE = new PropertyManager.ConstantProperty<>("Leadscrew Angle", 8.0);

	// Elevator
	
	// The next two are read on startup, so making them a property has no affect.
	public static final double ELEVATOR_DISTANCE_PER_ROTATION = 7.75;
	public static final double ELEVATOR_HEIGHT_FROM_GROUND = 19.0; // Height from ground to the "bottom" of the elevator
	public static final IProperty<Double> ELEVATOR_GO_TO_HEIGHT_TOLERANCE = new PropertyManager.ConstantProperty<>("Elevator Height Deadband", 3.0);
	public static final IProperty<Double> ELEVATOR_LOWER_LIMIT_SPEED = new PropertyManager.DoubleProperty("Elevator Slowdown Throttle", .1);

	public static final MotionMagicPropertyManager ELEVATOR_MM = new MotionMagicPropertyManager("Elevator", false, 
			0.2, 0, 0, .85, 1725, 1700, 0);

	// Target Heights
	public static final double HATCH_ROCKET_SHIP_LOW_PORT = 7;
	public static final double HATCH_ROCKET_SHIP_MED_PORT = 30;
	public static final double HATCH_ROCKET_SHIP_HIGH_PORT = 46;

	public static final double CARGO_ROCKET_SHIP_LOW_PORT = HATCH_ROCKET_SHIP_LOW_PORT;
	public static final double CARGO_ROCKET_SHIP_MED_PORT = HATCH_ROCKET_SHIP_MED_PORT;
	public static final double CARGO_ROCKET_SHIP_HIGH_PORT = HATCH_ROCKET_SHIP_HIGH_PORT;

	public static final double HATCH_OUT_OF_HP_STATION = 20;
	public static final double CARGO_CARGO_SHIP = 20;
}

