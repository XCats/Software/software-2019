package org.xcats.frc.deep_space.auto.commands;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.xcats.frc.deep_space.DeepSpaceProperties;
import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.processors.Positioner;
import org.xcats.frc.deep_space.subsystems.DeepSpaceDriveBase;

public class DriveToPoint implements AutoBase {
	private static final Logger LOGGER = LogManager.getLogger(DriveToPoint.class);

	private final Positioner mPositioner;
	private final DeepSpaceDriveBase mDrivetrain;

	private double mDistanceError;
	private double mAngleError;

	private double mGoalX;
	private double mGoalY;

	public DriveToPoint(Positioner position, DeepSpaceDriveBase drivetrain) {
		this(position, drivetrain, 0, 0);
	}

	public DriveToPoint(Positioner position, DeepSpaceDriveBase drivetrain, double x, double y) {
		mPositioner = position;
		mDrivetrain = drivetrain;
		mGoalX = x;
		mGoalY = y;
	}

	public void setGoal(double x, double y) {
		mGoalX = x;
		mGoalY = y;
		mDistanceError = Double.MAX_VALUE;
	}

	@Override
	public void init() {
		// Nothing to do
	}

	@Override
	public boolean isCompleted() {
		return mDistanceError < 6;
	}

	@Override
	public void execute() {
		double xError = mGoalX - mPositioner.getX();
		double yError = mGoalY - mPositioner.getY();
		double desiredAngle = Math.toDegrees(Math.atan2(xError, yError));

		mDistanceError = Math.sqrt(xError * xError + yError * yError);
		mAngleError = desiredAngle - mPositioner.getAngle();

		while (mAngleError > 180) {
			mAngleError -= 360;
		}
		while (mAngleError < -180) {
			mAngleError += 360;
		}

		double distancekP = DeepSpaceProperties.DRIVE_TO_POSITION_DISTANCE_KP.getValue();
		double anglekP = DeepSpaceProperties.DRIVE_TO_POSITION_ANGLE_KP.getValue();
		double distanceSpeed = getDistanceError() * distancekP;
		if (distanceSpeed >= 1.0) { // NOPMD
			distanceSpeed = 1.0;
		}
		double angleSpeed = anglekP * getAngleError();
		double motorRight = distanceSpeed - angleSpeed;
		double motorLeft = distanceSpeed + angleSpeed;

		if (mAngleError > 90 || mAngleError < -90) {
			LOGGER.log(Level.DEBUG, "Taking shortcut backwards");
			motorRight *= -1;
			motorLeft *= -1;
		}

		mDrivetrain.getController().setLeftSpeed(motorLeft);
		mDrivetrain.getController().setRightSpeed(motorRight);

		LOGGER.log(Level.DEBUG,
				"DriveToPosition\n" 
				+ "  Current: (" +  mPositioner.getX() + ", " +  mPositioner.getY() + "," + mPositioner.getAngle() + ")\n"
				+ "  Desired: (" +  mGoalX + ", " + mGoalY + ", " + desiredAngle + ")\n"
				+ "  Derr : " + mDistanceError + ", Dtheta: " + mAngleError + "\n"
				+ "  Speed right: " + motorRight + ", Speed left: " + motorLeft + "\n"
				+ "  angle speed: " + angleSpeed + ", distance speed" + distanceSpeed);

	}

	public double getDistanceError() {
		return mDistanceError;
	}

	public double getAngleError() {
		return mAngleError;
	}

	@Override
	public void stop() {
		mDrivetrain.getController().setLeftSpeed(0);
		mDrivetrain.getController().setRightSpeed(0);
	}
}
