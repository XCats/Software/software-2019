package org.xcats.frc.deep_space.pure_pursuit;

import java.text.DecimalFormat;
import java.util.List;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;

public class PurePursuitNetworkTable {

	private final NetworkTable mPurePursuitTable;

	public PurePursuitNetworkTable() {
		mPurePursuitTable = NetworkTableInstance.getDefault().getTable("CoordinateGui").getSubTable("PurePursuit");
		mPurePursuitTable.getEntry(".type").setString("PurePursuit");
	}

	private String convertWaypoints(List<PurePursuitWaypoint> waypoints) {
		StringBuilder builder = new StringBuilder();

		DecimalFormat df = new DecimalFormat("#.###");

		for (PurePursuitWaypoint waypoint : waypoints) {
			builder.append(df.format(waypoint.mX / 12.0)).append(',').append(df.format(waypoint.mY / 12.0)).append(',');
		}

		return builder.toString();
	}

	public void sendCurrentPointInformation(int index, double robotX, double robotY, double preCalculatedVelocity,
			double limitedPreCalculatedVelocity, double leftVelocity, double leftGoalVelocity, double rightVelocity,
			double rightGoalVelocity) {
		StringBuilder output = new StringBuilder();
		output.append(index).append(',')
				.append(robotX / 12.0).append(',')
				.append(robotY / 12.0).append(',')
				.append(preCalculatedVelocity / 12.0).append(',')
				.append(limitedPreCalculatedVelocity / 12.0).append(',')
				.append(leftVelocity / 12.0).append(',')
				.append(leftGoalVelocity / 12.0).append(',')
				.append(rightVelocity / 12.0).append(',')
				.append(rightGoalVelocity / 12.0);

		mPurePursuitTable.getEntry("CurrentPoint").setValue(output.toString());
	}

	public void sendConfigToNetworkTable(List<PurePursuitWaypoint> waypoints, List<PurePursuitWaypoint> upsampled,
			List<PurePursuitWaypoint> smoothed) {
		mPurePursuitTable.getEntry("Waypoints").setString(convertWaypoints(waypoints));
		mPurePursuitTable.getEntry("UpSampled").setString(convertWaypoints(upsampled));
		mPurePursuitTable.getEntry("Smoothed").setString(convertWaypoints(smoothed));
	}

	public void sendLookahead(Vector2D robotPosition, Vector2D lookaheadPoint) {
		StringBuilder builder = new StringBuilder();
		builder.append(robotPosition.getX() / 12.0).append(',').append(robotPosition.getY() / 12.0).append(',')
				.append(lookaheadPoint.getX() / 12.0).append(',').append(lookaheadPoint.getY() / 12.0);

		mPurePursuitTable.getEntry("Lookahead").setValue(builder.toString());
	}

	public void clearTable() {
		mPurePursuitTable.getEntry("Waypoints").setString("");
		mPurePursuitTable.getEntry("UpSampled").setString("");
		mPurePursuitTable.getEntry("Smoothed").setString("");
		mPurePursuitTable.getEntry("Lookahead").setValue("");
	}
}
