package org.xcats.frc.deep_space.auto.commands;

import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.subsystems.PullThroughCargo;

public class AquireCargoCommand implements AutoBase {
	private final PullThroughCargo mAquisition;

	public AquireCargoCommand(PullThroughCargo aquisition) {
		mAquisition = aquisition;
	}

	@Override
	public void init() {

	}

	@Override
	public boolean isCompleted() {
		return mAquisition.getCurrentState().getHasBall();
	}

	@Override
	public void execute() {
		mAquisition.getController().setRollerState(PullThroughCargo.RollerState.IN);
	}

	@Override
	public void stop() {
		mAquisition.getController().setRollerState(PullThroughCargo.RollerState.OFF);
	}
}
