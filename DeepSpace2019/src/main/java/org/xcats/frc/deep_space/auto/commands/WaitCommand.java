package org.xcats.frc.deep_space.auto.commands;

import org.xcats.frc.deep_space.auto.AutoBase;

import edu.wpi.first.wpilibj.Timer;

public class WaitCommand implements AutoBase {

	private final Timer mTimer;
	private final double mTime;

	public WaitCommand(double time) {
		mTimer = new Timer();
		mTime = time;
	}

	@Override
	public void init() {
		mTimer.start();
	}

	@Override
	public boolean isCompleted() {
		// TODO Auto-generated method stub
		return mTimer.get() > mTime;
	}

	@Override
	public void execute() {

	}

	@Override
	public void stop() {

	}

}
