package org.xcats.frc.deep_space;

public class PortMappings {

	////////////////////////////////
	// CAN Devices
	////////////////////////////////
	// Drive Motor CAN IDs
	public static final int DRIVE_MOTOR_LEFT_MASTER = 1;
	public static final int DRIVE_MOTOR_LEFT_FOLLOWER = 2;
	public static final int DRIVE_MOTOR_RIGHT_MASTER = 3;
	public static final int DRIVE_MOTOR_RIGHT_FOLLOWER = 4;

	// Elevator IDs
	public static final int ELEVATOR_MASTER = 5;
	public static final int ELEVATOR_FOLLOWER = 6;
	
	// Cargo Acquisition CAN IDs
	public static final int CARGO_MOTOR = 7;

	// Four Bar Can IDs
	public static final int LEFT_NEO = 10;

	////////////////////////////////
	// PWM
	////////////////////////////////

	// Blinkin
	public static final int BLINKEN_PWM = 3;
	
	////////////////////////////////
	// DIO
	////////////////////////////////

	// Elevator Sensors
	public static final int ELEVATOR_LOW_LIMIT = 0;
	public static final int ELEVATOR_UPPER_LIMIT = 2;

	////////////////////////////////
	// Pneumatics
	////////////////////////////////

	// Velcro Pneumatic Hatch
	public static final int ACTIVATOR_OUT_SOLENOID = 0;
	public static final int ACTIVATOR_IN_SOLENOID = 1;
	public static final int HANDLER_OUT_SOLENOID = 2;
	public static final int HANDLER_IN_SOLENOID = 3;

	////////////////////////////////
	// Joysticks
	////////////////////////////////
	public static final int LEFT_DRIVER_JS = 1;
	public static final int RIGHT_DRIVER_JS = 2;
	public static final int OPERATOR_JS = 0;

}
