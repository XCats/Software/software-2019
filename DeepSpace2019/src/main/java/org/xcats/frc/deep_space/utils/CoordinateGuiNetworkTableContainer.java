package org.xcats.frc.deep_space.utils;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import jaci.pathfinder.Trajectory;
import jaci.pathfinder.Trajectory.Segment;

public class CoordinateGuiNetworkTableContainer {
	private static final String TYPE_STRING = ".type";

	private final NetworkTable mPositionTable;
	private final NetworkTable mCameraRayTable;
	private final NetworkTable mGoToPositionTable;

	private final NetworkTableEntry mIdealTableEntry;
	private final NetworkTableEntry mMeasuredTableEntry;
	private final NetworkTableEntry mWaypointTableEntry;

	private int mRobotPositionCtr;

	public CoordinateGuiNetworkTableContainer() {
		NetworkTable topContainer = NetworkTableInstance.getDefault().getTable("CoordinateGui");
		topContainer.getEntry(TYPE_STRING).setString("CoordinateGui");

		mPositionTable = topContainer.getSubTable("RobotPosition");
		mPositionTable.getEntry(TYPE_STRING).setString("RobotPosition");

		mCameraRayTable = topContainer.getSubTable("CameraSim");
		mCameraRayTable.getEntry(TYPE_STRING).setString("CameraSim");

		mGoToPositionTable = topContainer.getSubTable("GoToPosition");
		mGoToPositionTable.getEntry(TYPE_STRING).setString("GoToPosition");

		NetworkTable trajectoryTable = topContainer.getSubTable("Spline Namespace");
		trajectoryTable.getEntry(TYPE_STRING).setString("Spline Namespace");

		mIdealTableEntry = trajectoryTable.getEntry("Ideal Spline Points");
		mMeasuredTableEntry = trajectoryTable.getEntry("Real Spline Point");
		mWaypointTableEntry = trajectoryTable.getEntry("Spline Waypoints");
	}

	public void setCameraRays(String aRays) {
		mCameraRayTable.getEntry("Positions").setString(aRays);
	}

	public void setRobotPosition(double aX, double aY, double aAngle) {
		mPositionTable.getEntry("X").setDouble(aX);
		mPositionTable.getEntry("Y").setDouble(aY);
		mPositionTable.getEntry("Angle").setDouble(aAngle);

		mPositionTable.getEntry("Ctr").setDouble(++mRobotPositionCtr);
	}

	public void setGoToXyData(double aX, double aY) {
		mGoToPositionTable.getEntry("Position").setString(aX + "," + aY);
	}

	public boolean isIdealTrajectorySet() {
		return mIdealTableEntry.getString("").isEmpty();
	}

	public void setMeasuredTrajectory(boolean isPathfinder, int segmentCtr,
			double leftPosition, double leftVelocity,
			double rightPosition, double rightVelocity,
			double averageX, double averageY, double averageHeading) {
		
		Segment displayAverageSegment = new Segment(0, averageX, averageY, 0, 0, 0, 0, averageHeading);
		Segment displayLeftSegment = new Segment(0, 0, 0, leftPosition, leftVelocity, 0, 0, 0);
		Segment displayRightSegment = new Segment(0, 0, 0, rightPosition, rightVelocity, 0, 0, 0);

		String pointInfo = segmentCtr + "," + IdealSplineSerializer.serializePathPoint(isPathfinder,
				displayAverageSegment,
				displayLeftSegment, displayRightSegment);
		
		mMeasuredTableEntry.setString(pointInfo);
	}

	public void setIdealTrajectoryWaypoints(String waypoints) {
		mWaypointTableEntry.setString(waypoints);
	}

	public void setIdealTrajectory(boolean isPathfinder, Trajectory centerTrajectory, Trajectory leftTrajectory,
			Trajectory rightTrajectory) {
		mIdealTableEntry.setString(IdealSplineSerializer.serializePath(isPathfinder, centerTrajectory.segments,
				leftTrajectory.segments, rightTrajectory.segments));
	}
}
