package org.xcats.frc.deep_space;

import java.util.Arrays;

import org.xcats.frc.deep_space.subsystems.DeepSpaceDriveBase;
import org.xcats.frc.deep_space.subsystems.Elevator;
import org.xcats.frc.deep_space.subsystems.FourBarClimber;
import org.xcats.frc.deep_space.subsystems.PullThroughCargo;
import org.xcats.frc.deep_space.subsystems.VelcroPneumaticHatch;
import org.xcats.frc.lib.XSystemManager;
import org.xcats.frc.lib.devices.actuation.pneumatics.impl.XPneumaticCylinder;
import org.xcats.frc.lib.devices.sensors.XEncoder;
import org.xcats.frc.lib.devices.sensors.XEncoder.Units;
import org.xcats.frc.lib.devices.sensors.XInu;
import org.xcats.frc.lib.devices.sensors.XLimitSwitch;
import org.xcats.frc.lib.devices.sensors.impl.Navx;
import org.xcats.frc.lib.devices.sensors.impl.PigeonWrapper;
import org.xcats.frc.lib.devices.sensors.impl.XSparkMaxEncoder;
import org.xcats.frc.lib.devices.sensors.impl.XSparkMaxLimitSwitch;
import org.xcats.frc.lib.devices.sensors.impl.XTalonSRXEncoder;
import org.xcats.frc.lib.devices.sensors.impl.XWpiLimitSwitch;
import org.xcats.frc.lib.wrappers.XJoystick;

import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced;
import com.ctre.phoenix.motorcontrol.VelocityMeasPeriod;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotBase;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.SpeedControllerGroup;

@SuppressWarnings("PMD.ExcessiveImports")
public class SubsystemManager extends XSystemManager {

	private final boolean mFinal;
	private XInu mInu;

	private DriverStation mDriverStation;

	private DeepSpaceDriveBase mDriveBase;
	private Elevator mElevator;
	private PullThroughCargo mCargo;
	private VelcroPneumaticHatch mHatchMech;
	private FourBarClimber mFourBar;

	public SubsystemManager(boolean isFinal) {
		mFinal = isFinal;
	}

	@Override
	@SuppressWarnings({ "PMD.ExcessiveMethodLength", "PMD.NcssCount" })
	public void initialize() {
		initializeDriveTrain();
		initializeElevator();
		initializeHatchMech();
		initializeCargo();
		initializeFourBar();
		initializeDriverStation();

		mSubsystems = Arrays.asList(
				mDriveBase,
				mHatchMech,
				mCargo,
				mElevator,
				mFourBar
		);

		mCurrentController = mDriverStation;
	}

	private void initializeCargo() {
		SpeedController cargoSpeedController = new WPI_TalonSRX(PortMappings.CARGO_MOTOR);

		mCargo = new PullThroughCargo(cargoSpeedController);
	}

	private void initializeDriveTrain() {
		WPI_TalonSRX leftMaster = new WPI_TalonSRX(PortMappings.DRIVE_MOTOR_LEFT_MASTER);
		WPI_TalonSRX rightMaster = new WPI_TalonSRX(PortMappings.DRIVE_MOTOR_RIGHT_MASTER);

		// Get faster feedback for the encoder
		leftMaster.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0, 10, 100);
		leftMaster.setStatusFramePeriod(StatusFrameEnhanced.Status_3_Quadrature, 10);
		leftMaster.configVelocityMeasurementPeriod(VelocityMeasPeriod.Period_50Ms);
		leftMaster.configVelocityMeasurementWindow(8);

		rightMaster.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0, 10, 100);
		rightMaster.setStatusFramePeriod(StatusFrameEnhanced.Status_3_Quadrature, 10);
		rightMaster.configVelocityMeasurementPeriod(VelocityMeasPeriod.Period_50Ms);
		rightMaster.configVelocityMeasurementWindow(8);

		double leftInversion;
		if (RobotBase.isReal()) {
			leftInversion = -1;
		} else {
			leftInversion = 1;
		}

		final XTalonSRXEncoder leftDriveEncoder = new XTalonSRXEncoder("Left Drive Encoder", leftMaster,
				leftInversion * DeepSpaceProperties.DRIVETRAIN_DISTANCE_PER_ROTATION, Units.Inches);
		final XTalonSRXEncoder rightDriveEncoder = new XTalonSRXEncoder("Right Drive Encoder", rightMaster,
				DeepSpaceProperties.DRIVETRAIN_DISTANCE_PER_ROTATION, Units.Inches);

		SpeedController leftFollower;
		SpeedController rightFollower;

		if (!mFinal) {
			leftFollower = new WPI_TalonSRX(PortMappings.DRIVE_MOTOR_LEFT_FOLLOWER);
			rightFollower = new WPI_TalonSRX(PortMappings.DRIVE_MOTOR_RIGHT_FOLLOWER);
		} else {
			leftFollower = new WPI_VictorSPX(PortMappings.DRIVE_MOTOR_LEFT_FOLLOWER);
			rightFollower = new WPI_VictorSPX(PortMappings.DRIVE_MOTOR_RIGHT_FOLLOWER);
		}

		final SpeedControllerGroup leftDriveControlGroup = new SpeedControllerGroup(leftMaster, leftFollower);
		final SpeedControllerGroup rightDriveControlGroup = new SpeedControllerGroup(rightMaster, rightFollower);

		rightDriveControlGroup.setInverted(true);

		mDriveBase = new DeepSpaceDriveBase(leftDriveControlGroup, rightDriveControlGroup, leftDriveEncoder,
				rightDriveEncoder);
	}

	private void initializeHatchMech() {
		DoubleSolenoid hatchActivatorSolenoid = new DoubleSolenoid(PortMappings.ACTIVATOR_IN_SOLENOID, PortMappings.ACTIVATOR_OUT_SOLENOID);
		DoubleSolenoid hatchHandlerSolenoid = new DoubleSolenoid(PortMappings.HANDLER_OUT_SOLENOID, PortMappings.HANDLER_IN_SOLENOID);

		XPneumaticCylinder handlerCylinder = new XPneumaticCylinder("Hatch Handler Cylinder", hatchHandlerSolenoid, false);
		XPneumaticCylinder activatorCylinder = new XPneumaticCylinder("Hatch Mechanism Activator Cylinder", hatchActivatorSolenoid, false);

		mHatchMech = new VelcroPneumaticHatch(handlerCylinder, activatorCylinder);
	}

	private void initializeElevator() {
		WPI_TalonSRX elevatorMaster = new WPI_TalonSRX(PortMappings.ELEVATOR_MASTER);
		WPI_TalonSRX lowerFollower = new WPI_TalonSRX(PortMappings.ELEVATOR_FOLLOWER);

		elevatorMaster.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative);
		elevatorMaster.setSensorPhase(true);
		lowerFollower.follow(elevatorMaster);

		if (RobotBase.isReal()) {
			elevatorMaster.setInverted(true);
			lowerFollower.setInverted(false);
		} else {
			elevatorMaster.setInverted(false);
			lowerFollower.setInverted(true);
		}

		XEncoder elevatorEncoder = new XTalonSRXEncoder("Elevator Encoder", elevatorMaster,
				DeepSpaceProperties.ELEVATOR_DISTANCE_PER_ROTATION, Units.Inches);
		XLimitSwitch elevatorUpperLimit = new XWpiLimitSwitch("Upper Limit", PortMappings.ELEVATOR_UPPER_LIMIT);
		XLimitSwitch elevatorLowLimit = new XWpiLimitSwitch("Low Limit Switch", PortMappings.ELEVATOR_LOW_LIMIT);

		// TODO fix simulator
		if (RobotBase.isReal()) {
			mInu = new PigeonWrapper(elevatorMaster, XInu.GyroDirection.Roll);
		} else {
			mInu = new Navx();
		}

		mElevator = new Elevator(elevatorMaster, elevatorEncoder, elevatorUpperLimit, elevatorLowLimit);
	}

	private void initializeDriverStation() {
		final XJoystick leftDriverJS = new XJoystick(PortMappings.LEFT_DRIVER_JS);
		final XJoystick rightDriverJS = new XJoystick(PortMappings.RIGHT_DRIVER_JS);

		leftDriverJS.setInverted(true);
		rightDriverJS.setInverted(true);

		final Joystick operatorJS = new Joystick(PortMappings.OPERATOR_JS);

		mDriverStation = new DriverStation(leftDriverJS, rightDriverJS, operatorJS, mDriveBase.getController(), mHatchMech.getController(),
				mCargo.getController(),
				mElevator.getController(),
				mFourBar.getController()
		);
	}

	private void initializeFourBar() {
		CANSparkMax fourbarSpeedController = new CANSparkMax(PortMappings.LEFT_NEO, CANSparkMaxLowLevel.MotorType.kBrushless);

		fourbarSpeedController.setInverted(false);

		fourbarSpeedController.setParameter(CANSparkMaxLowLevel.ConfigParameter.kHardLimitFwdEn, false);

		XLimitSwitch fourbarSwitch = new XSparkMaxLimitSwitch("four bar limit", fourbarSpeedController, true, false);
		XEncoder fourbarEncoder = new XSparkMaxEncoder("four bar encoder", fourbarSpeedController, 1, Units.Inches);

		this.mFourBar = new FourBarClimber(fourbarSpeedController, fourbarEncoder, fourbarSwitch);
	}

	public PullThroughCargo getCargoMechanism() {
		return this.mCargo;
	}

	public VelcroPneumaticHatch getHatchMech() {
		return this.mHatchMech;
	}

	public XInu getNavx() {
		return this.mInu;
	}

	public DeepSpaceDriveBase getDrivetrain() {
		return mDriveBase;
	}

	public DriverStation getDriverStation() {
		return mDriverStation;
	}

	public Elevator getElevator() {
		return mElevator;
	}

}

