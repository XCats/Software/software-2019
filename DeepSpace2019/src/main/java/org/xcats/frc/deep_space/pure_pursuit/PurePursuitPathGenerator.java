package org.xcats.frc.deep_space.pure_pursuit;

import java.util.ArrayList;
import java.util.List;

public class PurePursuitPathGenerator {
	private final List<PurePursuitWaypoint> mUpsampled;
	private final List<PurePursuitWaypoint> mSmoothed;

	public PurePursuitPathGenerator(List<PurePursuitWaypoint> waypoints, double spacing, double weightSmooth,
			double tolerance) {
		if (waypoints.size() < 2) { // NOPMD
			throw new IllegalArgumentException("Need to supply at least two points");
		}

		mUpsampled = upSamplePath(waypoints, spacing);
		mSmoothed = smoothPoints(mUpsampled, weightSmooth, tolerance);
	}

	private List<PurePursuitWaypoint> upSamplePath(List<PurePursuitWaypoint> input, double spacing) {
		List<PurePursuitWaypoint> output = new ArrayList<>();

		for (int i = 0; i < input.size() - 1; ++i) {
			Vector2D start = input.get(i).asVector();
			Vector2D end = input.get(i + 1).asVector();
			Vector2D vector = Vector2D.subtract(end, start);

			double numPoints = vector.magnitude() / spacing;
			Vector2D unitVector = Vector2D.normalize(vector);

			for (int j = 0; j < numPoints; ++j) {
				Vector2D newPoint = Vector2D.multiply(unitVector, j * spacing);
				newPoint.add(start);
				output.add(new PurePursuitWaypoint(newPoint)); // NOPMD
			}
		}

		output.add(input.get(input.size() - 1));

		return output;
	}

	private List<PurePursuitWaypoint> smoothPoints(List<PurePursuitWaypoint> input, double weightSmooth,
			double tolerance) {
		double a = 1 - weightSmooth;
		double b = weightSmooth;

		List<PurePursuitWaypoint> output = new ArrayList<>(input.size());
		for (PurePursuitWaypoint waypoint : input) {
			output.add(new PurePursuitWaypoint(waypoint)); // NOPMD
		}

		double change = tolerance;
		while (change >= tolerance) {

			change = 0.0;
			for (int i = 1; i < input.size() - 1; ++i) {
				PurePursuitWaypoint originalPoint = input.get(i);
				PurePursuitWaypoint currentPoint = output.get(i);
				PurePursuitWaypoint previousPoint = output.get(i - 1);
				PurePursuitWaypoint nextPoint = output.get(i + 1);

				double newX = currentPoint.mX + (a * (originalPoint.mX - currentPoint.mX)
						+ b * (previousPoint.mX + nextPoint.mX - 2 * currentPoint.mX));
				double newY = currentPoint.mY + (a * (originalPoint.mY - currentPoint.mY)
						+ b * (previousPoint.mY + nextPoint.mY - 2 * currentPoint.mY));

				change += Math.abs(currentPoint.mX - newX);
				change += Math.abs(currentPoint.mY - newY);

				currentPoint.mX = newX;
				currentPoint.mY = newY;
			}
		}

		return output;
	}

	public List<PurePursuitWaypoint> getUpsampled() {
		return mUpsampled;
	}

	public List<PurePursuitWaypoint> getSmoothed() {
		return mSmoothed;
	}

}
