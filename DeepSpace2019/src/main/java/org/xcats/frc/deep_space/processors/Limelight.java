package org.xcats.frc.deep_space.processors;

import org.xcats.frc.deep_space.DeepSpaceProperties;
import org.xcats.frc.deep_space.utils.CoordinateGuiNetworkTableContainer;
import org.xcats.frc.lib.logging.XLogString;
import org.xcats.frc.lib.processors.XProcessor;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.SendableBase;
import edu.wpi.first.wpilibj.smartdashboard.SendableBuilder;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Limelight implements XProcessor {
	private final Positioner mPositioner;
	private final CoordinateGuiNetworkTableContainer mCoordinateGuiTable;
	private final NetworkTable mLimelightTable;

	private boolean mTargetSeen;
	private double mRawDistance;
	private double mRobotDistance;
	private double mAngle;
	private double mX;
	private double mY;

	private double mRawTargetArea;
	private double mRawPitchError;
	private double mRawYawError;

	private final class LimelightSendable extends SendableBase {

		@Override
		public void initSendable(SendableBuilder builder) {
			builder.addBooleanProperty("Sees Target", Limelight.this::seesTarget, null);
			builder.addDoubleProperty("Target X", Limelight.this::getTargetX, null);
			builder.addDoubleProperty("Target Y", Limelight.this::getTargetY, null);
			builder.addDoubleProperty("Distance", Limelight.this::getDistance, null);
			builder.addDoubleProperty("Angle", Limelight.this::getAngle, null);
		}

	}


	public Limelight(CoordinateGuiNetworkTableContainer coordinateGuiTable, Positioner position) {
		mPositioner = position;
		mCoordinateGuiTable = coordinateGuiTable;
		mLimelightTable = NetworkTableInstance.getDefault().getTable("limelight");

		SmartDashboard.putData("XLimelight", new LimelightSendable());
	}

	@Override
	public void getPeriodicInput() {
		mTargetSeen = mLimelightTable.getEntry("tv").getDouble(0.0) == 1.0;

		mRawTargetArea = mLimelightTable.getEntry("ta").getDouble(0.0);
		mRawPitchError = mLimelightTable.getEntry("ty").getDouble(0);
		mRawYawError = mLimelightTable.getEntry("tx").getDouble(0);

		if (mTargetSeen) {
			double camerAngle = mRawPitchError;
			double elAngle = DeepSpaceProperties.CAMERA_EL_ANGLE_OFFSET.getValue() + camerAngle;
			double rayDistance = Math.tan(Math.toRadians(elAngle));
			mRawDistance = DeepSpaceProperties.CAMERA_TO_TARGET_HEIGHT.getValue() / rayDistance;
			mRobotDistance = mRawDistance - 20;
			mAngle = mPositioner.getAngle() + mRawYawError;
			mX = mPositioner.getX() + (Math.sin(Math.toRadians(mAngle)) * mRawDistance);
			mY = mPositioner.getY() + (Math.cos(Math.toRadians(mAngle)) * mRawDistance);

			String positionsText = mPositioner.getX() + "," + mPositioner.getY() + "," + mX + "," + mY;
			mCoordinateGuiTable.setCameraRays(positionsText);
		} else {
			mRawDistance = Double.MAX_VALUE;
			mAngle = Double.MAX_VALUE;
			mX = Double.MAX_VALUE;
			mY = Double.MAX_VALUE;
			mCoordinateGuiTable.setCameraRays("");
		}
	}

	public boolean seesTarget() {
		return mTargetSeen;
	}

	public double getDistance() {
		return mRobotDistance;
	}

	public double getAngle() {
		return mAngle;
	}

	public double getTargetX() {
		return mX;
	}

	public double getTargetY() {
		return mY;
	}

	@Override
	public XLogString getLogString() {
		return null;
	}

	@Override
	public void writePeriodicOutput() {

	}

	public void turnOffLeds() {
		mLimelightTable.getEntry("ledMode").setDouble(1);
	}

	public void turnOnLeds() {
		mLimelightTable.getEntry("ledMode").setDouble(0);
	}

	public double getRawTargetArea() {
		return mRawTargetArea;
	}

	public double getRawPitchError() {
		return mRawPitchError;
	}

	public double getRawYawError() {
		return mRawYawError;
	}
}
