package org.xcats.frc.deep_space.subsystems;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xcats.frc.deep_space.DeepSpaceProperties;
import org.xcats.frc.deep_space.IRobotStateManager;
import org.xcats.frc.lib.XSubsystem;
import org.xcats.frc.lib.devices.sensors.XEncoder;
import org.xcats.frc.lib.devices.sensors.XLimitSwitch;
import org.xcats.frc.lib.logging.XLogString;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;


/**
 * This class provides an example implementation of the XSubsystem interface. It
 * implements a simple claw with acquisition wheels. The claw is either open or
 * closed, and the acquisition wheels can be set to run at a given RPM. There is
 * also a sensor for if a ball is in the acquisition. It uses a controller /
 * state design pattern as well as a singleton design pattern.
 */
public class Elevator implements XSubsystem {

	private static final Logger LOGGER = LogManager.getLogger(Elevator.class);
	private static final double LOWER_SLOWDOWN_TICKS = 5000;

	/**
	 * A controller class that contains all variables that are modified to control
	 * the subsystem
	 */

	public enum Height {
		OFF,
		LOW,
		MED,
		HIGH,
		CARGO,
		HATCH_OUT_OF_HP
	}

	public enum MovementState {
		UP, DOWN, NONE;

		public Boolean toBoolean() {
			switch (this) {
				case UP:
					return true;
				case DOWN:
					return false;
				default:
					return null;
			}
		}
	}

	public class Controller {
		private double mSpeed;
		private Height mHeight = Height.OFF;

		public void setSpeed(double speed) {
			mSpeed = speed;
		}

		public void zero() {
			mElevatorEncoder.zero();
		}

		public void setHeight(Height aHeight) {
			this.mHeight = aHeight;
		}

	}

	/**
	 * A state class is built that will represent the state of the subsystem
	 */
	public class State {
		private double mSpeed;
		private Height mSetHeight = Height.OFF;
		private double mSetPoint;
		private boolean mElevatorUpperLimit;
		private boolean mElevatorLowerLimit;
		private double mElevatorHeight;

		private MovementState mMovementState = MovementState.NONE;

		public double getSpeed() {
			return mSpeed;
		}

		public Height getSetHeight() {
			return mSetHeight;
		}

		public double getSetPoint() {
			return mSetPoint;
		}

		public MovementState getMovementState() {
			return mMovementState;
		}

		public boolean isLowerLimitPressed() {
			return mElevatorLowerLimit;
		}

		public boolean isUpperLimitPressed() {
			return mElevatorUpperLimit;
		}

		public double getGoToHeightError() {
			return mSetPoint - mElevatorHeight;
		}

		public boolean isAtSetpoint() {
			return Math.abs(getGoToHeightError()) < DeepSpaceProperties.ELEVATOR_GO_TO_HEIGHT_TOLERANCE.getValue();
		}

		private boolean isLimited() {
			boolean limitedDown = isLowerLimitPressed() && getMovementState() == MovementState.DOWN;
			boolean limitedUp = isUpperLimitPressed() && getMovementState() == MovementState.UP;

			return limitedDown || limitedUp;
		}
	}

	private final Controller mController = new Controller();

	private IRobotStateManager mStateManager;

	// Holds the desired state of the subsystem
	private final State mDesiredState = new State();

	// Holds the actual state of the subsystem
	private final State mCurrentState = new State();

	// Injected Dependencies
	private final WPI_TalonSRX mMasterTalon;
	private final XEncoder mElevatorEncoder;
	private final XLimitSwitch mElevatorUpperLimit;
	private final XLimitSwitch mElevatorLowerLimit;

	private final NetworkTable mElevatorTable;

	public Elevator(WPI_TalonSRX masterTalon, XEncoder elevatorEncoder, XLimitSwitch elevatorUpperLimit,
			XLimitSwitch elevatorLowerLimit) {
		this.mMasterTalon = masterTalon;
		this.mElevatorEncoder = elevatorEncoder;
		this.mElevatorUpperLimit = elevatorUpperLimit;
		this.mElevatorLowerLimit = elevatorLowerLimit;

		this.mElevatorTable = NetworkTableInstance.getDefault().getTable("RobotData").getSubTable("Elevator");
	}

	public void setStateManager(IRobotStateManager stateManager) {
		this.mStateManager = stateManager;
	}

	public State getDesiredState() {
		return this.mDesiredState;
	}

	public State getCurrentState() {
		return this.mCurrentState;
	}

	public Controller getController() {
		return this.mController;
	}

	@Override
	public void stop() {
		// No motion state here
	}

	@Override
	public void initialize() {

	}

	@Override
	public XLogString getLogString() {
		return null;
	}

	@SuppressWarnings("PMD.CyclomaticComplexity")
	@Override
	public void getPeriodicInput() {

		mCurrentState.mElevatorLowerLimit = !mElevatorLowerLimit.getCurrentValue();
		mCurrentState.mElevatorUpperLimit = !mElevatorUpperLimit.getCurrentValue();
		mCurrentState.mElevatorHeight = mElevatorEncoder.getLinearDistance();

		/* TODO: When these get locked in they should be done in initialize, not every loop. It is here now to be
		    able to tweak them from the SD.
		 */
		DeepSpaceProperties.ELEVATOR_MM.writeSettings(0, mMasterTalon);

		// Read values from controller into desired state
		mDesiredState.mSpeed = -getController().mSpeed;
		mDesiredState.mSetHeight = mController.mHeight;

		if (mDesiredState.mSetHeight == Height.OFF) {
			if (mDesiredState.mSpeed > 0) {
				mDesiredState.mMovementState = MovementState.UP;
			} else if (mDesiredState.mSpeed < 0) {
				mDesiredState.mMovementState = MovementState.DOWN;
			} else {
				mDesiredState.mMovementState = MovementState.NONE;
			}
			if (!mDesiredState.mMovementState.equals(mCurrentState.mMovementState)) {
				LOGGER.log(Level.INFO,
						"Elevator changed movement state from " + mCurrentState.mMovementState.name() + "to " + mDesiredState.mMovementState.name());
			}
		}

		parseSetpoint();
	}

	@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
	private void parseSetpoint() {
		double elevatorSetpoint;

		if (this.mStateManager != null) {
			switch (mDesiredState.mSetHeight) {
				case LOW:
					elevatorSetpoint = mStateManager.getElevatorLowHeight();
					break;
				case MED:
					elevatorSetpoint = mStateManager.getElevatorMediumHeight();
					break;
				case HIGH:
					elevatorSetpoint = mStateManager.getElevatorHighHeight();
					break;
				case CARGO:
					elevatorSetpoint = mStateManager.getElevatorCargoHeight();
					break;
				case HATCH_OUT_OF_HP:
					elevatorSetpoint = mStateManager.getHatchOutOfHpHeight();
					break;
				default:
					elevatorSetpoint = 0;
					break;
			}
		} else {
			elevatorSetpoint = 0;
			mDesiredState.mSetHeight = Height.OFF;
			LOGGER.log(Level.WARN, "StateManager is not initialized, defaulting to manual control.");
		}
		mDesiredState.mSetPoint = elevatorSetpoint;

	}

	@Override
	public void writePeriodicOutput() {
		final double setpointTicks = mElevatorEncoder.convertToTicksFromDistance(mDesiredState.mSetPoint);

		mCurrentState.mMovementState = mDesiredState.mMovementState;

		// If motion magic isn't on
		if (this.getController().mHeight == Height.OFF) {
			writeManualControl();
		} else {
			LOGGER.log(Level.INFO, "Elevator: Desired Position " + this.getController().mHeight + "(" + setpointTicks + "), " + mDesiredState.mSetHeight);
			mMasterTalon.set(ControlMode.MotionMagic, setpointTicks);
		}

		final boolean isMotionMagic = mMasterTalon.getControlMode() == ControlMode.MotionMagic;
		mCurrentState.mSpeed = mMasterTalon.getMotorOutputPercent();
		mCurrentState.mSetHeight = mDesiredState.mSetHeight;
		mCurrentState.mSetPoint = mDesiredState.mSetPoint;

		if (DeepSpaceProperties.SMART_DASHBOARD_DEBUG.getValue()) {
			putNetworkTableData(isMotionMagic, setpointTicks);
		}
	}

	private void writeManualControl() {
		double speed = mDesiredState.mSpeed;
		if (mDesiredState.isLimited()) {
			speed = 0;
			LOGGER.log(Level.WARN, "Can't move the elevator because of limit switches");
		} else {
			if (mElevatorEncoder.getRawDistanceTicks() < LOWER_SLOWDOWN_TICKS
					&& mDesiredState.mMovementState == MovementState.DOWN) {
				speed = Math.max(speed, -DeepSpaceProperties.ELEVATOR_LOWER_LIMIT_SPEED.getValue());
				LOGGER.log(Level.WARN, "Slowing down the elevator moving down: " + speed);
			}
		}
		mMasterTalon.set(ControlMode.PercentOutput, speed);
	}

	protected void putNetworkTableData(boolean isMotionMagic, double setpointTicks) {
		mElevatorTable.getEntry("Current Movement State").setString(this.mCurrentState.mMovementState.name());
		mElevatorTable.getEntry("Encoder Ticks").setDouble(mElevatorEncoder.getRawDistanceTicks());
		mElevatorTable.getEntry("Encoder Height").setDouble(mElevatorEncoder.getLinearDistance());
		mElevatorTable.getEntry("Motor Percentage").setDouble(mMasterTalon.getMotorOutputPercent());
		mElevatorTable.getEntry("Elevator Lower Limit").setBoolean(mCurrentState.mElevatorLowerLimit);
		mElevatorTable.getEntry("Elevator Upper Limit").setBoolean(mCurrentState.mElevatorUpperLimit);
		mElevatorTable.getEntry("Current Set Point/Is MM").setBoolean(isMotionMagic);
		mElevatorTable.getEntry("Current Set Point/Name").setString(mCurrentState.mSetHeight.name());
		mElevatorTable.getEntry("Current Set Point/Height").setDouble(mCurrentState.mSetPoint);
		mElevatorTable.getEntry("Current Set Point/Ticks").setDouble(setpointTicks);
		mElevatorTable.getEntry("Current Set Point/Error Ticks").setDouble(isMotionMagic ? mMasterTalon.getClosedLoopError() : -999);
		mElevatorTable.getEntry("Current Set Point/Error Inches")
				.setDouble(isMotionMagic ? mCurrentState.getGoToHeightError() : -999);
	}
}
