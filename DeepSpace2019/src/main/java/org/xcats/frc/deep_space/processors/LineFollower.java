package org.xcats.frc.deep_space.processors;

import org.xcats.frc.lib.devices.sensors.XLimitSwitch;
import org.xcats.frc.lib.logging.XLogString;
import org.xcats.frc.lib.processors.XProcessor;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;

public class LineFollower implements XProcessor {

	private final XLimitSwitch mLeftSensor;
	private final XLimitSwitch mMiddleSensor;
	private final XLimitSwitch mRightSensor;
	private final NetworkTable mNetworkTable;
	private boolean mLeftTripped;
	private boolean mCenterTripped;
	private boolean mRightTripped;
	private int mAsBinary;
	private LineState mState;

	public enum LineState {
		BLIND, HARD_LEFT, LEFT, CENTER, RIGHT, HARD_RIGHT;
	}

	public LineFollower(XLimitSwitch left, XLimitSwitch middle, XLimitSwitch right) {
		mLeftSensor = left;
		mMiddleSensor = middle;
		mRightSensor = right;
		mState = LineState.BLIND;

		mNetworkTable = NetworkTableInstance.getDefault().getTable("RobotData").getSubTable("LineFollower");
	}

	@Override
	public XLogString getLogString() {
		return null;
	}

	@Override
	public void getPeriodicInput() {
		mRightTripped = !mRightSensor.getCurrentValue();
		mCenterTripped = !mMiddleSensor.getCurrentValue();
		mLeftTripped = !mLeftSensor.getCurrentValue();
		mAsBinary = convertToBinary();

		if (mRightTripped) {
			if (mCenterTripped) {
				mState = LineState.RIGHT;
			} else {
				mState = LineState.HARD_RIGHT;
			}
		} else if (mLeftTripped) {
			if (mCenterTripped) {
				mState = LineState.LEFT;
			} else {
				mState = LineState.HARD_LEFT;
			}
		} else if (mCenterTripped) {
			mState = LineState.CENTER;
		}
		// Else, keep on trucking
	}

	private int convertToBinary() {
		int binary = 0;
		if (mLeftTripped) {
			binary += 1 << 2;
		}
		if (mCenterTripped) {
			binary += 1 << 1;
		}
		if (mRightTripped) {
			binary += 1 << 0;
		}

		return binary;
	}

	@Override
	public void writePeriodicOutput() {
		mNetworkTable.getEntry("Left").setBoolean(getLeft());
		mNetworkTable.getEntry("Middle").setBoolean(getMiddle());
		mNetworkTable.getEntry("Right").setBoolean(getRight());
		mNetworkTable.getEntry("Binary").setDouble(mAsBinary);
		mNetworkTable.getEntry("State").setString(mState.toString());
	}

	public boolean getRight() {
		return mRightTripped;
	}

	public boolean getMiddle() {
		return mCenterTripped;
	}

	public boolean getLeft() {
		return mLeftTripped;
	}

	public LineState getState() {
		return mState;
	}
}
