package org.xcats.frc.deep_space.auto.commands;

import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.auto.StartingPositionManager.StartingPosition;
import org.xcats.frc.deep_space.processors.Positioner;

public class SetPosition implements AutoBase {

	private final Positioner mPositioner;
	private final double mX;
	private final double mY;
	private final double mAngle;

	public SetPosition(Positioner positioner, StartingPosition position) {
		this(positioner, position.mX, position.mY, position.mAngle);
	}

	public SetPosition(Positioner positioner, double x, double y, double yaw) {
		mPositioner = positioner;
		mX = x;
		mY = y;
		mAngle = yaw;
	}

	@Override
	public void init() {
		mPositioner.setPosition(mX, mY, mAngle);
	}

	@Override
	public boolean isCompleted() {
		return true;
	}

	@Override
	public void execute() {

	}

	@Override
	public void stop() {
	}

}
