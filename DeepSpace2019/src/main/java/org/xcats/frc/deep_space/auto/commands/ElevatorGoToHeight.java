package org.xcats.frc.deep_space.auto.commands;

import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.subsystems.Elevator;

public class ElevatorGoToHeight implements AutoBase {
	private final Elevator mElevator;
	private final Elevator.Height mPosition;

	public ElevatorGoToHeight(Elevator.Height position, Elevator elevator) {
		this.mPosition = position;
		this.mElevator = elevator;
	}

	@Override
	public void init() {

	}

	@Override
	public boolean isCompleted() {
		return mElevator.getCurrentState().isAtSetpoint();
	}

	@Override
	public void execute() {
		this.mElevator.getController().setHeight(mPosition);

	}

	@Override
	public void stop() {
		mElevator.getController().setHeight(Elevator.Height.OFF);
		mElevator.getController().setSpeed(0);
	}
}
