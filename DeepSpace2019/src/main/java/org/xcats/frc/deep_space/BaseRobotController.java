package org.xcats.frc.deep_space;

import org.xcats.frc.lib.XInitializable;
import org.xcats.frc.lib.XLoopable;
import org.xcats.frc.lib.subsystems.impl.DriveBase;

public abstract class BaseRobotController implements XLoopable, XInitializable {
	protected final DriveBase.Controller mDriveController;

	public BaseRobotController(DriveBase.Controller driveController) {
		this.mDriveController = driveController;
	}
}
