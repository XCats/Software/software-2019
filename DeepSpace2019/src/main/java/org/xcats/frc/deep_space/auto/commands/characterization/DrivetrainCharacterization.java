package org.xcats.frc.deep_space.auto.commands.characterization;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.RobotBase;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.subsystems.DeepSpaceDriveBase;

// Shamelessly stolen. See:
// https://www.chiefdelphi.com/t/paper-frc-drivetrain-characterization/160915
// https://www.chiefdelphi.com/t/the-great-drive-base-performance-characterization-data-sharing-thread/162243
// https://github.com/robotpy/robot-characterization
public class DrivetrainCharacterization implements AutoBase {

	private final NetworkTableEntry mAutoSpeedEntry;
	private final NetworkTableEntry mTelemetryEntry;
	private final Number[] mNumberArray = new Number[9]; // NOPMD
	private double mPriorAutospeed;

	private final DeepSpaceDriveBase mDrivetrain;


	public DrivetrainCharacterization(DeepSpaceDriveBase drivetrain) {
		mDrivetrain = drivetrain;

		mAutoSpeedEntry = NetworkTableInstance.getDefault().getEntry("/robot/autospeed");
		mTelemetryEntry = NetworkTableInstance.getDefault().getEntry("/robot/telemetry");
	}

	@Override
	public void execute() {
		double batteryVoltage = RobotBase.isReal() ? RobotController.getBatteryVoltage() : 12;
		double motorVolts = batteryVoltage * Math.abs(mPriorAutospeed);

		double autospeed = mAutoSpeedEntry.getDouble(0);
		mPriorAutospeed = autospeed;

		mDrivetrain.getController().setLeftSpeed(autospeed);
		mDrivetrain.getController().setRightSpeed(autospeed);

		mNumberArray[0] = Timer.getFPGATimestamp();
		mNumberArray[1] = batteryVoltage;
		mNumberArray[2] = autospeed;
		mNumberArray[3] = motorVolts;
		mNumberArray[4] = motorVolts;
		mNumberArray[5] = mDrivetrain.getCurrentState().getLeftEncoderDistance() / 12;
		mNumberArray[6] = mDrivetrain.getCurrentState().getRightEncoderDistance() / 12;
		mNumberArray[7] = mDrivetrain.getCurrentState().getLeftEncoderVelocity() / 12;
		mNumberArray[8] = mDrivetrain.getCurrentState().getRightEncoderVelocity() / 12;

		mTelemetryEntry.setNumberArray(mNumberArray);

	}

	@Override
	public void init() {
		NetworkTableInstance.getDefault().setUpdateRate(0.010);
	}

	@Override
	public boolean isCompleted() {
		return false;
	}

	@Override
	public void stop() {
		NetworkTableInstance.getDefault().setUpdateRate(0.02);
	}

}
