package org.xcats.frc.deep_space.processors;

import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class DeepSpaceBlinkinDriver extends BaseRevBlinkinDriver {

	private final SendableChooser<Patterns> mChooser;

	public DeepSpaceBlinkinDriver(Spark spark) {
		super(spark);

		mChooser = new SendableChooser<>();
		for (Patterns pattern : Patterns.values()) {
			mChooser.addOption(pattern.name(), pattern);
		}

		SmartDashboard.putData("Blinkin Pattern", mChooser);
	}

	@Override
	public void getPeriodicInput() {
		mPattern = mChooser.getSelected();
	}

}
