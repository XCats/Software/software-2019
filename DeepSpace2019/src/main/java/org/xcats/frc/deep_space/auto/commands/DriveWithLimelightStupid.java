package org.xcats.frc.deep_space.auto.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xcats.frc.deep_space.DeepSpaceProperties;
import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.processors.Limelight;
import org.xcats.frc.deep_space.subsystems.DeepSpaceDriveBase;
import org.xcats.frc.deep_space.utils.PropertyManager.IProperty;

public class DriveWithLimelightStupid implements AutoBase {
	protected static final Logger LOGGER = LogManager.getLogger(DriveWithLimelightStupid.class);

	public enum TrackingVersion {
		TargetArea(DeepSpaceProperties.LIMELIGHT_TARGET_AREA_TOLERANCE, DeepSpaceProperties.LIMELIGHT_TARGET_AREA_KP),
		Distance(DeepSpaceProperties.LIMELIGHT_DISTANCE_TOLERANCE, DeepSpaceProperties.LIMELIGHT_DISTANCE_KP),
		PitchError(DeepSpaceProperties.LIMELIGHT_YAW_ERROR_TOLERANCE, DeepSpaceProperties.LIMELIGHT_YAW_ERROR_KP);

		private final IProperty<Double> mErrorTolerance;
		private final IProperty<Double> mKp;

		TrackingVersion(IProperty<Double> errorTolerance, IProperty<Double> kp) {
			mErrorTolerance = errorTolerance;
			mKp = kp;
		}
		
		public double getErrorTolerance() {
			return mErrorTolerance.getValue();
		}
	}

	private final Limelight mLimelight;
	private final DeepSpaceDriveBase mDrivetrain;

	private final TrackingVersion mTrackingVersion;
	private final double mSteeringTolerance;
	private final double mDrivingErrorTolerance;
	private boolean mIsFinished;

	public DriveWithLimelightStupid(DeepSpaceDriveBase drivetrain, Limelight limelight,
			TrackingVersion trackingVersion) {
		mLimelight = limelight;
		mDrivetrain = drivetrain;
		mTrackingVersion = trackingVersion;
		mDrivingErrorTolerance = trackingVersion.getErrorTolerance();
		mSteeringTolerance = 5; // degrees
	}

	@Override
	public void init() {
		mIsFinished = false;
	}

	@Override
	public boolean isCompleted() {
		return mIsFinished;
	}

	@Override
	public void execute() {
		if (!mLimelight.seesTarget()) {
			mDrivetrain.stop();
			LOGGER.error("Cant see the target");
			mIsFinished = true;
			return;
		}

		double driveError;

		switch (mTrackingVersion) {
			case Distance:
				driveError = mLimelight.getDistance();
				if (driveError > DeepSpaceProperties.LIMELIGHT_DISTANCE_MAX_ERROR.getValue()) {
					LOGGER.warn("Limiting max error for the limelight, original=" + driveError);
					driveError = DeepSpaceProperties.LIMELIGHT_DISTANCE_MAX_ERROR.getValue();
				}
				break;
			case TargetArea:
				driveError = DeepSpaceProperties.LIMELIGHT_TARGET_AREA_EXPECTED.getValue() - mLimelight.getRawTargetArea();
				break;
			case PitchError:
				driveError = mLimelight.getRawPitchError();
				break;
			default:
				throw new IllegalArgumentException();
		}

		double steerError = mLimelight.getRawYawError();
		double steerCommand = steerError * DeepSpaceProperties.LIMELIGHT_STEERING_KP.getValue();

		double driveKp = mTrackingVersion.mKp.getValue();
		double driveCommand = driveError * driveKp;

		double left = driveCommand + steerCommand;
		double right = driveCommand - steerCommand;

		boolean steeringFinished = Math.abs(steerError) < mSteeringTolerance;
		boolean drivingFinished = Math.abs(driveError) < mDrivingErrorTolerance;

		LOGGER.info(
				mTrackingVersion + "\n" + "  se: " + steerError + ", sc: " + steerCommand + "\n" + "  de: " + driveError
						+ ", dc: " + driveCommand + "\n" + "  left: " + left + ", right: " + right + "\n");

		mDrivetrain.getController().setLeftSpeed(left);
		mDrivetrain.getController().setRightSpeed(right);

		mIsFinished = steeringFinished && drivingFinished;
	}

	@Override
	public void stop() {
		mDrivetrain.stop();
	}

}
