package org.xcats.frc.deep_space.auto.commands;

import java.util.List;

import org.xcats.frc.deep_space.auto.AutoBase;

public class SequentialCommand implements AutoBase {

	private final List<AutoBase> mCommands;
	private int mStepCount;

	public SequentialCommand(List<AutoBase> commands) {
		mCommands = commands;
	}

	@Override
	public void init() {
		mStepCount = 0;
		mCommands.get(0).init();
	}

	@Override
	public boolean isCompleted() {
		return mStepCount >= mCommands.size();
	}

	@Override
	public void execute() {
		if (mStepCount >= mCommands.size()) {
			return;
		}

		AutoBase currentCommand = mCommands.get(mStepCount);
		currentCommand.execute();
		if (currentCommand.isCompleted()) {
			currentCommand.stop();

			mStepCount++;
			if (mStepCount < mCommands.size()) {
				mCommands.get(mStepCount).init();
			}
		}
	}

	@Override
	public void stop() {
		if (mStepCount < mCommands.size()) {
			mCommands.get(mStepCount).stop();
		}
	}

}
