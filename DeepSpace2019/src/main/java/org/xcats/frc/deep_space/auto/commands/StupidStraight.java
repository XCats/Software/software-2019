package org.xcats.frc.deep_space.auto.commands;

import edu.wpi.first.wpilibj.Timer;
import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.subsystems.DeepSpaceDriveBase;

public class StupidStraight implements AutoBase {

	private final DeepSpaceDriveBase mSpaceDriveBase;
	private final Timer mTimer = new Timer();

	private final double mTime;
	private final double mSpeed;

	public StupidStraight(DeepSpaceDriveBase spaceDriveBase, double time, double speed) {
		this.mSpaceDriveBase = spaceDriveBase;
		this.mTime = time;
		this.mSpeed = speed;
	}

	@Override
	public void init() {
		mTimer.start();
	}

	@Override
	public boolean isCompleted() {
		return mTimer.get() >= mTime;
	}

	@Override
	public void execute() {
		mSpaceDriveBase.getController().setLeftSpeed(mSpeed);
		mSpaceDriveBase.getController().setRightSpeed(mSpeed);
	}

	@Override
	public void stop() {
		mSpaceDriveBase.getController().setLeftSpeed(0);
		mSpaceDriveBase.getController().setRightSpeed(0);
	}
}

