package org.xcats.frc.deep_space.auto.mode_factories;

public final class TrajectoryNames {

	// Hab to scoring positions
	public static final String LEFT_HAB1_TO_CARGO_SHIP_CLOSE = "LeftHab1.CargoShip.Close";
	public static final String LEFT_HAB1_TO_CARGO_SHIP_MIDDLE = "LeftHab1.CargoShip.Middle";
	public static final String LEFT_HAB1_TO_CARGO_SHIP_FAR = "LeftHab1.CargoShip.Far";

	public static final String RIGHT_HAB1_TO_CARGO_SHIP_CLOSE = "RightHab1.CargoShip.Close";
	public static final String RIGHT_HAB1_TO_CARGO_SHIP_MIDDLE = "RightHab1.CargoShip.Middle";
	public static final String RIGHT_HAB1_TO_CARGO_SHIP_FAR = "RightHab1.CargoShip.Far";

	public static final String LEFT_HAB1_TO_ROCKET_CLOSE = "LeftHab1.Rocket.Close";
	public static final String RIGHT_HAB1_TO_ROCKET_CLOSE = "RightHab1.Rocket.Close";

	// Post score to cargo corral
	public static final String LEFT_CARGO_SHIP_CLOSE_TO_CARGO_CORRAL = "LeftCargoShip.Close.CargoCorral";
	public static final String LEFT_CARGO_SHIP_MIDDLE_TO_CARGO_CORRAL = "LeftCargoShip.Middle.CargoCorral";
	public static final String LEFT_CARGO_SHIP_FAR_TO_CARGO_CORRAL = "LeftCargoShip.Far.CargoCorral";

	public static final String RIGHT_CARGO_SHIP_CLOSE_TO_CARGO_CORRAL = "RightCargoShip.Close.CargoCorral";
	public static final String RIGHT_CARGO_SHIP_MIDDLE_TO_CARGO_CORRAL = "RightCargoShip.Middle.CargoCorral";
	public static final String RIGHT_CARGO_SHIP_FAR_TO_CARGO_CORRAL = "RightCargoShip.Far.CargoCorral";

	// Cargo corral to cargo ship
	public static final String LEFT_CARGO_CORRAL_TO_CLOSE_CARGO_SHIP = "LeftCargoCorral.Close";
	public static final String LEFT_CARGO_CORRAL_TO_MIDDLE_CARGO_SHIP = "LeftCargoCorral.Middle";
	public static final String LEFT_CARGO_CORRAL_TO_FAR_CARGO_SHIP = "LeftCargoCorral.Far";

	public static final String RIGHT_CARGO_CORRAL_TO_CLOSE_CARGO_SHIP = "RightCargoCorral.Close";
	public static final String RIGHT_CARGO_CORRAL_TO_MIDDLE_CARGO_SHIP = "RightCargoCorral.Middle";
	public static final String RIGHT_CARGO_CORRAL_TO_FAR_CARGO_SHIP = "RightCargoCorral.Far";

	private TrajectoryNames() {

	}

}
