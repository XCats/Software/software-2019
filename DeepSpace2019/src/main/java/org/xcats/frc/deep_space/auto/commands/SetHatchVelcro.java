package org.xcats.frc.deep_space.auto.commands;

import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.subsystems.VelcroPneumaticHatch;

public class SetHatchVelcro implements AutoBase {

	private final VelcroPneumaticHatch mHatchMech;
	private final boolean mOut;

	public SetHatchVelcro(VelcroPneumaticHatch hatchMech, boolean out) {
		this.mHatchMech = hatchMech;
		this.mOut = out;
	}

	@Override
	public void init() {

	}

	@Override
	public boolean isCompleted() {
		return mHatchMech.getCurrentState().isVelcroOut() == mOut;
	}

	@Override
	public void execute() {
		mHatchMech.getController().setVelcro(mOut);
	}

	@Override
	public void stop() {
		// Leave the solenoid how it is
	}
}
