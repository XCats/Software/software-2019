package org.xcats.frc.deep_space.auto.scripts;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xcats.frc.deep_space.auto.AutoBase;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.AbstractConstruct;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.ScalarNode;
import org.yaml.snakeyaml.nodes.Tag;

import org.xcats.frc.deep_space.utils.GetDeployDirectory;

public class AutoScriptLoader {
	private static final Logger LOGGER = LogManager.getLogger(AutoScriptLoader.class);
	
	private final CommandFactory mCommandFactory;

	private boolean mParseSuccesful;
	
	public AutoScriptLoader(CommandFactory commandFactory) {
		mCommandFactory = commandFactory;
	}

	public static Path getAutoModesPath() {
		return Paths.get(GetDeployDirectory.getDeployDirectory().getAbsolutePath(), "auto_modes");
	}

	private static class ImportConstruct extends AbstractConstruct {
		@Override
		public Object construct(Node node) {
			if (!(node instanceof ScalarNode)) {
				throw new IllegalArgumentException("Non-scalar !import: " + node.toString());
			}

			final ScalarNode scalarNode = (ScalarNode) node;
			final String value = scalarNode.getValue();

			File file = Paths.get(getAutoModesPath().toString(), value).toFile();
			LOGGER.log(Level.INFO, "importing '" + value + "' (" + file.getAbsolutePath() + ")");
			if (!file.exists()) {
				throw new IllegalArgumentException("File does not exist! '" + file.getAbsolutePath() + "'");
			}

			try {
				final InputStream input = Files.newInputStream(file.toPath());
				final Yaml yaml = new Yaml(CONSTRUCTOR);
				Object output = yaml.load(input);

				return output;
			} catch (IOException ex) {
				LOGGER.log(Level.ERROR, "Could not load file " + file, ex);
			}

			LOGGER.log(Level.ERROR, "Something went horribly wrong...");
			return null;
		}
	}

	private static class MyConstructor extends Constructor {
		public MyConstructor() {
			yamlConstructors.put(new Tag("!include"), new ImportConstruct());
		}
	}

	private static final MyConstructor CONSTRUCTOR = new MyConstructor();

	public List<AutoBase> loadFile(Path filepath) {
		LOGGER.log(Level.INFO, "Loading auto script '" + filepath + "'");
		List<AutoBase> output = new ArrayList<>();
		mParseSuccesful = true;

		File file = filepath.toFile();
		try (InputStream stream = Files.newInputStream(file.toPath())) {
			Yaml yaml = new Yaml(CONSTRUCTOR);

			@SuppressWarnings("unchecked")
			List<Object> yamlContents = (List<Object>) yaml.load(stream);
			List<AutoBase> commands = mCommandFactory.parseCommandList(yamlContents);
			if (commands != null) {
				output.addAll(commands);
			} else {
				mParseSuccesful = false;
			}

		} catch (IOException | IllegalArgumentException ex) {
			LOGGER.log(Level.ERROR, "Could not parse file '" + filepath + "'", ex);
			mParseSuccesful = false;
		}

		if (!mParseSuccesful) {
			return null;
		}

		return output;
	}

	public boolean wasParsedSuccesfully() {
		return mParseSuccesful;
	}

}
