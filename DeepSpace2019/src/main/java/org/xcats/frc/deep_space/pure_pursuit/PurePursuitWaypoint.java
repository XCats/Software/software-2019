package org.xcats.frc.deep_space.pure_pursuit;

public class PurePursuitWaypoint {
	public double mX;
	public double mY;

	private boolean mPreCalculatedValuesValid;
	private double mPrecalculatedVelocity;
	private double mPrecalculatedCurvature;

	public PurePursuitWaypoint() {

	}

	public PurePursuitWaypoint(double x, double y) {
		mX = x;
		mY = y;
	}

	public PurePursuitWaypoint(PurePursuitWaypoint copy) {
		mX = copy.mX;
		mY = copy.mY;
	}

	public PurePursuitWaypoint(Vector2D vector) {
		mX = vector.getX();
		mY = vector.getY();
	}

	public Vector2D asVector() {
		return new Vector2D(mX, mY);
	}

	@Override
	public String toString() {
		StringBuilder output = new StringBuilder(36);
		output.append("PurePursuitWaypoint [mX=" + mX + ", mY=" + mY);

		if (mPreCalculatedValuesValid) {
			output.append(", mPrecalculatedVelocity=").append(mPrecalculatedVelocity);
			output.append(", mPrecalculatedCurvature=").append(mPrecalculatedCurvature);
		}

		output.append(']');

		return output.toString();
	}

	public void setCalculatedValues(double velocity, double curvature) {
		mPreCalculatedValuesValid = true;
		mPrecalculatedVelocity = velocity;
		mPrecalculatedCurvature = curvature;
	}

	public double getPrecalculatedVelocity() {
		return mPrecalculatedVelocity;
	}

	public double getPrecalculatedCurvature() {
		return mPrecalculatedCurvature;
	}

}
