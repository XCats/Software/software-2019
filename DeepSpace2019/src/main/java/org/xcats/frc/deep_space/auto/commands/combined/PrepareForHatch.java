package org.xcats.frc.deep_space.auto.commands.combined;

import java.util.Arrays;

import org.xcats.frc.deep_space.auto.commands.ElevatorGoToHeight;
import org.xcats.frc.deep_space.auto.commands.ParallelCommand;
import org.xcats.frc.deep_space.auto.commands.SetHatchVelcro;
import org.xcats.frc.deep_space.subsystems.Elevator;
import org.xcats.frc.deep_space.subsystems.Elevator.Height;
import org.xcats.frc.deep_space.subsystems.VelcroPneumaticHatch;

public class PrepareForHatch extends ParallelCommand {

	public PrepareForHatch(Elevator elevator, VelcroPneumaticHatch hatchquisition) {
		super(Arrays.asList(
				new ElevatorGoToHeight(Height.LOW, elevator),
				new SetHatchVelcro(hatchquisition, true)), 
				false);
	}

}
