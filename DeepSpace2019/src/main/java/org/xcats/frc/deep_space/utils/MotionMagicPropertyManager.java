package org.xcats.frc.deep_space.utils;

import org.xcats.frc.deep_space.utils.PropertyManager.ConstantProperty;
import org.xcats.frc.deep_space.utils.PropertyManager.DoubleProperty;
import org.xcats.frc.deep_space.utils.PropertyManager.IProperty;
import org.xcats.frc.deep_space.utils.PropertyManager.IntProperty;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

@SuppressWarnings({"PMD.DataClass", "PMD.ExcessiveParameterList"})
public class MotionMagicPropertyManager {

	private final IProperty<Double> mKP;
	private final IProperty<Double> mKI;
	private final IProperty<Double> mKD;
	private final IProperty<Double> mKFF;
	private final IProperty<Integer> mKCruiseVelocity;
	private final IProperty<Integer> mKCruiseAcceleration;
	private final IProperty<Double> mArbFeedForward;

	public MotionMagicPropertyManager(String name, boolean isConstant, double defaultKp, double defaultKi, double defaultKD, double defaultKff,
			int defaultCruiseVelocity, int defaultCruiseAcceleration, double arbFeedForward) {

		if (isConstant) {
			mKP = new ConstantProperty<>(name + "_MM_KP", defaultKp);
			mKI = new ConstantProperty<>(name + "_MM_KI", defaultKi);
			mKD = new ConstantProperty<>(name + "_MM_KD", defaultKD);
			mKFF = new ConstantProperty<>(name + "_MM_KF", defaultKff);
			mKCruiseVelocity = new ConstantProperty<>(name + "_MM_MaxCruiseVelocity", defaultCruiseVelocity);
			mKCruiseAcceleration = new ConstantProperty<>(name + "_MM_MaxAcceleration", defaultCruiseAcceleration);
			mArbFeedForward = new ConstantProperty<>(name + "_MM_ARB_FF", arbFeedForward);
		} else {
			mKP = new DoubleProperty(name + "_MM_KP", defaultKp);
			mKI = new DoubleProperty(name + "_MM_KI", defaultKi);
			mKD = new DoubleProperty(name + "_MM_KD", defaultKD);
			mKFF = new DoubleProperty(name + "_MM_KF", defaultKff);
			mKCruiseVelocity = new IntProperty(name + "_MM_MaxCruiseVelocity", defaultCruiseVelocity);
			mKCruiseAcceleration = new IntProperty(name + "_MM_MaxAcceleration", defaultCruiseAcceleration);
			mArbFeedForward = new DoubleProperty(name + "_MM_ARB_FF", arbFeedForward);
		}
	}

	public void writeSettings(int slot, WPI_TalonSRX motor) {
		motor.config_kP(slot, getmKP());
		motor.config_kI(slot, getmKI());
		motor.config_kD(slot, getmKD());
		motor.config_kF(slot, getmKFF());
		motor.configMotionCruiseVelocity(getmKCruiseVelocity());
		motor.configMotionAcceleration(getmKCruiseAcceleration());
	}

	public double getmKP() {
		return mKP.getValue();
	}

	public double getmKI() {
		return mKI.getValue();
	}

	public double getmKD() {
		return mKD.getValue();
	}

	public double getmKFF() {
		return mKFF.getValue();
	}

	public int getmKCruiseVelocity() {
		return mKCruiseVelocity.getValue();
	}

	public int getmKCruiseAcceleration() {
		return mKCruiseAcceleration.getValue();
	}
	
	public double getArbFeedForward() {
		return mArbFeedForward.getValue();
	}

}
