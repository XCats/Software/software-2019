package org.xcats.frc.deep_space;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.auto.AutoExecuter;
import org.xcats.frc.deep_space.auto.AutonomousFactory;
import org.xcats.frc.deep_space.auto.StartingPositionManager;
import org.xcats.frc.lib.XLoopable;

import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends TimedRobot {

	private static final Logger LOGGER = LogManager.getLogger(Robot.class);

	private SubsystemManager mSystemManager;
	private ProcessorManager mProcessorManager;
	private IRobotStateManager mStateManager;

	private List<XLoopable> mManagers;

	private AutonomousFactory mAutonomousFactory;
	private AutoExecuter mAutoExecuter;

	private boolean mRunAutonomous;

	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	@Override
	public void robotInit() {

		DigitalInput realRobotJumper = new DigitalInput(9);
		boolean isFinal = realRobotJumper.get();
		realRobotJumper.close();

		SmartDashboard.putBoolean("Is real robot", isFinal);
		
		NetworkTableInstance.getDefault().getTable("RobotData").getEntry(".type").setString("RobotData");

		mSystemManager = new SubsystemManager(isFinal);
		mSystemManager.initialize();

		mProcessorManager = new ProcessorManager(mSystemManager.getDrivetrain().getCurrentState(),
				mSystemManager.getNavx());

		mManagers = Arrays.asList(mSystemManager, mProcessorManager);

		mAutonomousFactory = new AutonomousFactory(mSystemManager, mProcessorManager);
		mAutoExecuter = new AutoExecuter();

		mStateManager = new RobotStateManager(mSystemManager.getDrivetrain(), mSystemManager.getElevator(),
				mSystemManager.getCargoMechanism(), mSystemManager.getHatchMech(), mProcessorManager.getLimelight());

		mSystemManager.getElevator().setStateManager(mStateManager);
		mSystemManager.getDriverStation().setRobotStateManager(mStateManager);

		new StartingPositionManager(mProcessorManager.getPositioner());
	}

	@Override
	public void disabledInit() {
		mSystemManager.stop();
	}

	@Override
	public void autonomousInit() {
		mProcessorManager.getLimelight().turnOnLeds();

		AutoBase selectedMode = mAutonomousFactory.getSelectedMode();
		if (selectedMode != null) {
			mRunAutonomous = true;
			mSystemManager.setCurrentController(mAutoExecuter);
			mAutoExecuter.start(selectedMode);

			LOGGER.log(Level.INFO, "Running auto in sandstorm");
		} else {
			mRunAutonomous = false;
			teleopInit();

			LOGGER.log(Level.INFO, "Running tele in sandstorm");
		}
	}

	@Override
	public void teleopInit() {
		mProcessorManager.getLimelight().turnOnLeds();

		mSystemManager.reInit();

		mSystemManager.setCurrentController(mSystemManager.getDriverStation());
	}

	@Override
	public void disabledPeriodic() {
		mManagers.stream().forEach(m -> m.getPeriodicInput());
	}

	/**
	 * This function is called periodically during autonomous
	 */
	@Override
	public void autonomousPeriodic() {

		if (mSystemManager.getDriverStation().isCancelAutonPressed()) {
			// Switched states so initialize teleop
			if (mRunAutonomous) {
				LOGGER.log(Level.INFO, "Cancelling autonomous....");
				teleopInit();
			}

			mRunAutonomous = false;
		}

		if (mRunAutonomous) {
			mSystemManager.getDriverStation().getPeriodicInput();
			mManagers.stream().forEach(m -> m.getPeriodicInput());
			mManagers.stream().forEach(m -> m.writePeriodicOutput());
		} else {
			teleopPeriodic();
		}
	}

	/**
	 * This function is called periodically during operator control
	 */
	@Override
	public void teleopPeriodic() {
		mManagers.stream().forEach(m -> m.getPeriodicInput());
		mManagers.stream().forEach(m -> m.writePeriodicOutput());
	}

	@Override
	public void robotPeriodic() {

	}

	public SubsystemManager getSubsystemManager() {
		return mSystemManager;
	}

	public ProcessorManager getProcessorManager() {
		return mProcessorManager;
	}

	public IRobotStateManager getStateManager() {
		return mStateManager;
	}
}
