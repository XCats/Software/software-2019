package org.xcats.frc.deep_space.auto.commands;

import org.xcats.frc.deep_space.auto.AutoBase;

public class DoNothing implements AutoBase {

	private final boolean mRunForever;

	public DoNothing(boolean runForever) {
		mRunForever = runForever;
	}

	@Override
	public void init() {

	}

	@Override
	public boolean isCompleted() {
		return !mRunForever;
	}

	@Override
	public void execute() {

	}

	@Override
	public void stop() {

	}

}
