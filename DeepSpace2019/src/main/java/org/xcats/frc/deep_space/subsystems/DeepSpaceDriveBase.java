package org.xcats.frc.deep_space.subsystems;

import org.xcats.frc.lib.devices.sensors.XEncoder;
import org.xcats.frc.lib.subsystems.impl.DriveBase;

import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.SpeedControllerGroup;

/**
 * This class contains code for the 2019 Destination Deep Space Drive base. It extends our basic DriveBase, but adds
 * encoders.
 */
public class DeepSpaceDriveBase extends DriveBase {
	/**
	 * The state class, containing the state of the Drive Base
	 */
	public class State extends DriveBase.State {
		private double mLeftEncoderDistance;
		private double mLeftEncoderVelocity;
		private double mRightEncoderDistance;
		private double mRightEncoderVelocity;

		/**
		 * Zeroes the encoder position of all drive encoders (does nothing to velocity)
		 */
		public void zeroEncoders() {
			DeepSpaceDriveBase.this.zeroEncoders();
		}

		/**
		 * Gets the average of the encoder positions of the left and right encoders
		 * @return the average encoder position
		 */
		public double getAvgEncoderPosition() {
			double sum = this.mLeftEncoderDistance + this.mRightEncoderDistance;
			return sum / 2.0;
		}

		public double getLeftEncoderDistance() {
			return mLeftEncoderDistance;
		}

		public double getLeftEncoderVelocity() {
			return mLeftEncoderVelocity;
		}

		public double getRightEncoderDistance() {
			return mRightEncoderDistance;
		}

		public double getRightEncoderVelocity() {
			return mRightEncoderVelocity;
		}
	}

	// Holds the desired state of the subsystem
	protected State mDesiredState = new State();

	// Holds the actual state of the subsystem
	protected State mCurrentState = new State();

	private final XEncoder mLeftEncoder;
	private final XEncoder mRightEncoder;

	/**
	 * Initialize a new drive base with left and right motors as well as encoders
	 *
	 * @param leftMotor  The left motor(s). Can be an individual speed controller or
	 *                   a {@link SpeedControllerGroup}
	 * @param rightMotor The right motor(s). Can be an individual speed controller or
	 *                   a {@link SpeedControllerGroup}
	 * @param leftEncoder The encoder on the left side of the drivebase
	 * @param rightEncoder the encoder on the right side of the drivebase
	 */
	public DeepSpaceDriveBase(SpeedController leftMotor, SpeedController rightMotor, XEncoder leftEncoder,
			XEncoder rightEncoder) {
		super(leftMotor, rightMotor);
		this.mLeftEncoder = leftEncoder;
		this.mRightEncoder = rightEncoder;
	}

	/**
	 * Runs the {@link DriveBase#getPeriodicInput()} method, reading in values from the controller, and also reads the
	 * cumulative values of both drive encoders into the current state of the class.
	 */
	@Override
	public void getPeriodicInput() {
		super.getPeriodicInput();
		this.getCurrentState().mRightEncoderDistance = this.mRightEncoder.getLinearDistance();
		this.getCurrentState().mLeftEncoderDistance = this.mLeftEncoder.getLinearDistance();

		this.getCurrentState().mRightEncoderVelocity = this.mRightEncoder.getLinearVelocity();
		this.getCurrentState().mLeftEncoderVelocity = this.mLeftEncoder.getLinearVelocity();
	}

	/**
	 * Zeroes both drive encoders
	 */
	public void zeroEncoders() {
		this.mLeftEncoder.zero();
		this.mRightEncoder.zero();
	}

	/**
	 * Gets the average encoder position of the drive base
	 * @return the average encoder position in rotations
	 */
	public double getAvgEncoderPosition() {
		double sum = this.mLeftEncoder.getLinearDistance() + this.mRightEncoder.getLinearDistance();
		return sum / 2.0;
	}

	/**
	 * Gets the desired state of the subsystem
	 * 
	 * @return a {@link State} object holding the desired state of the subsystem
	 */
	@Override
	public State getDesiredState() {
		return this.mDesiredState;
	}

	/**
	 * Gets the current state of the subsystem
	 * 
	 * @return a {@link State} object holding the current state of the subsystem
	 */
	@Override
	public State getCurrentState() {
		return this.mCurrentState;
	}
}
