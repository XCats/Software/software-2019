package org.xcats.frc.deep_space.pure_pursuit;

@SuppressWarnings("PMD.TooManyMethods")
public class Vector2D {
	private double mX;
	private double mY;

	public Vector2D(double x, double y) {
		mX = x;
		mY = y;
	}

	public Vector2D(Vector2D copy) {
		mX = copy.mX;
		mY = copy.mY;
	}

	@Override
	public String toString() {
		return "Vector2D [mX=" + mX + ", mY=" + mY + "]";
	}

	public static Vector2D normalize(Vector2D input) {
		Vector2D output = new Vector2D(input);
		output.divide(output.magnitude());
		return output;
	}

	public static Vector2D add(Vector2D v1, Vector2D v2) {
		Vector2D output = new Vector2D(v1);
		output.add(v2);
		return output;
	}

	public void add(Vector2D other) {
		mX += other.mX;
		mY += other.mY;
	}

	public static Vector2D subtract(Vector2D start, Vector2D end) {
		Vector2D output = new Vector2D(start);
		output.subtract(end);

		return output;
	}

	public void subtract(Vector2D other) {
		mX -= other.mX;
		mY -= other.mY;
	}

	public static Vector2D multiply(Vector2D input, double mult) {
		Vector2D output = new Vector2D(input);
		output.multiply(mult);
		return output;
	}

	public void multiply(double mult) {
		mX *= mult;
		mY *= mult;
	}

	public static double distance(Vector2D pathPoint, Vector2D robotPosition) {
		Vector2D difference = subtract(pathPoint, robotPosition);
		return difference.magnitude();
	}

	public void divide(double divisor) {
		mX /= divisor;
		mY /= divisor;
	}

	public double magnitude() {
		return Math.sqrt(mX * mX + mY * mY);
	}

	public double dot(Vector2D other) {
		return mX * other.mX + mY * other.mY;
	}

	public double getX() {
		return mX;
	}

	public double getY() {
		return mY;
	}

}
