package org.xcats.frc.deep_space;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xcats.frc.deep_space.subsystems.Elevator;
import org.xcats.frc.deep_space.subsystems.FourBarClimber;
import org.xcats.frc.deep_space.subsystems.PullThroughCargo;
import org.xcats.frc.deep_space.subsystems.VelcroPneumaticHatch;
import org.xcats.frc.lib.subsystems.impl.DriveBase;

import edu.wpi.first.wpilibj.Joystick;

public class DriverStation extends BaseRobotController {
	protected static final Logger LOGGER = LogManager.getLogger(DriverStation.class);

	public DriverStation(Joystick leftDriverJS, Joystick rightDriverJS, Joystick operatorJS,
			DriveBase.Controller driveController, VelcroPneumaticHatch.Controller hatchController,
			PullThroughCargo.Controller cargoController,
			Elevator.Controller elevatorController,
			FourBarClimber.Controller fourBarController
	) {
		super(driveController);

		this.mLeftDriverJS = leftDriverJS;
		this.mRightDriverJS = rightDriverJS;
		this.mOperatorController = new OperatorController(operatorJS);
		this.mDriveController = driveController;
		this.mHatchController = hatchController;
		this.mCargoController = cargoController;
		this.mElevatorController = elevatorController;
		this.mFourBarController = fourBarController;
	}

	public boolean isCancelAutonPressed() {
		return mState.mCancelAutonomous;
	}

	private static class State {
		private double mLeftSpeed;
		private double mRightSpeed;
		private boolean mDriveWithLimelight;

		private boolean mCancelAutonomous;
	}

	// Class-specific member variables
	private final State mState = new State();

	// Inputs
	private final Joystick mLeftDriverJS;
	private final Joystick mRightDriverJS;
	private final OperatorController mOperatorController;


	// Subsystems that are controlled
	private final DriveBase.Controller mDriveController;
	private final PullThroughCargo.Controller mCargoController;
	private final Elevator.Controller mElevatorController;
	private final VelcroPneumaticHatch.Controller mHatchController;
	private final FourBarClimber.Controller mFourBarController;

	// Injected in
	private IRobotStateManager mRobotStateManager;

	public void setRobotStateManager(IRobotStateManager manager) {
		mRobotStateManager = manager;
	}

	@Override
	public void getPeriodicInput() {
		this.mState.mLeftSpeed = mLeftDriverJS.getY(); // Invert because forward on the stick is negative
		this.mState.mRightSpeed = mRightDriverJS.getY(); // Invert because forward on the stick is negative
		this.mState.mDriveWithLimelight = mRightDriverJS.getRawButton(9);

		this.mState.mCancelAutonomous = mLeftDriverJS.getTrigger() && mRightDriverJS.getTrigger();

		if (this.mState.mLeftSpeed < 0) {
			this.mState.mLeftSpeed = this.mState.mLeftSpeed + DeepSpaceProperties.DRIVER_JS_ADD.getValue();
		}

		if (this.mState.mRightSpeed < 0) {
			this.mState.mRightSpeed = this.mState.mRightSpeed + DeepSpaceProperties.DRIVER_JS_ADD.getValue();
		}

		this.mOperatorController.getPeriodicInput();

		if (this.mState.mLeftSpeed < DeepSpaceProperties.DRIVER_JS_DEADBAND.getValue()
				&& this.mState.mLeftSpeed > -DeepSpaceProperties.DRIVER_JS_DEADBAND.getValue()) {
			this.mState.mLeftSpeed = 0;
		}
		if (this.mState.mRightSpeed < DeepSpaceProperties.DRIVER_JS_DEADBAND.getValue()
				&& this.mState.mRightSpeed > -DeepSpaceProperties.DRIVER_JS_DEADBAND.getValue()) {
			this.mState.mRightSpeed = 0;
		}
	}

	@Override
	public void writePeriodicOutput() {
		writeDrivetrainOutput();
		writeElevatorOutput();
		writeCargoOutput();
		writeHatchOutput();
		writeFourBarOutput();
	}

	private void writeDrivetrainOutput() {

		if (this.mState.mDriveWithLimelight) {
			LOGGER.info("Driving with limelight in tele");
			mRobotStateManager.driveToTarget();
		} else {
			this.mDriveController.setLeftSpeed(this.mState.mLeftSpeed);
			this.mDriveController.setRightSpeed(this.mState.mRightSpeed);
		}
	}

	private void writeElevatorOutput() {
		if (!mOperatorController.getCurrentState().getRunHatchAcquisitionSequence()) {
			this.mElevatorController.setSpeed(mOperatorController.getCurrentState().getElevatorJS());
			this.mElevatorController.setHeight(mOperatorController.getCurrentState().getElevatorPosition());
		}
	}

	private void writeHatchOutput() {
		if (mOperatorController.getCurrentState().getRunHatchAcquisitionSequence()) {
			if (mOperatorController.getCurrentState().getRunHatchAcquisitionSequenceChanged()) {
				LOGGER.info("Resetting hatch command in tele");
				mRobotStateManager.getAquireHatchSequence().init();
			}

			LOGGER.info("Acquiring hatch command in tele");
			mRobotStateManager.getAquireHatchSequence().execute();
			if (mRobotStateManager.getAquireHatchSequence().isCompleted()) {
				mOperatorController.getCurrentState().setHatchVelcro(false);
			}
		} else {
			this.mHatchController.setEjector(mOperatorController.getCurrentState().getHatchEjector());
			this.mHatchController.setVelcro(mOperatorController.getCurrentState().getHatchVelcro());
		}

	}

	private void writeCargoOutput() {
		this.mCargoController.setRollerState(mOperatorController.getCurrentState().getCargoRollers());
	}

	private void writeFourBarOutput() {
		this.mFourBarController.setSpeed(mOperatorController.getCurrentState().getFourBarJS());
	}

	@Override
	public void initialize() {

	}

	@Override
	public void stop() {

	}

}
