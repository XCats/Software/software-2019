/**
 * Graciously stolen from
 * <a href="https://www.chiefdelphi.com/t/paper-implementation-of-the-adaptive-pure-pursuit-controller/166552">FRC 1712</a>
 * and
 * <a href="https://github.com/Team3256/warriorlib/tree/master/src/main/java/frc/team3256/warriorlib/auto/purepursuit">FRC 3256</a>
 * 
 */
package org.xcats.frc.deep_space.pure_pursuit;
