package org.xcats.frc.deep_space;

import org.xcats.frc.deep_space.auto.commands.combined.AcquireHatchSequence;

public interface IRobotStateManager {

	class RobotStatePair<T> {
		private final T mDesiredState;
		private final T mCurrentState;

		public RobotStatePair(T aDesiredState, T aCurrentState) {
			this.mDesiredState = aDesiredState;
			this.mCurrentState = aCurrentState;
		}

		public T getDesiredState() {
			return mDesiredState;
		}

		public T getCurrentState() {
			return mCurrentState;
		}

	}

	public double getElevatorHighHeight();

	public double getElevatorMediumHeight();

	public double getElevatorCargoHeight();

	public double getElevatorLowHeight();

	public double getHatchOutOfHpHeight();

	public void driveToTarget();

	public AcquireHatchSequence getAquireHatchSequence();

}
