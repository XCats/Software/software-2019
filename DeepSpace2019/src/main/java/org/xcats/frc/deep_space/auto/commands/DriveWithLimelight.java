package org.xcats.frc.deep_space.auto.commands;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.processors.Limelight;
import org.xcats.frc.deep_space.processors.Positioner;
import org.xcats.frc.deep_space.subsystems.DeepSpaceDriveBase;

public class DriveWithLimelight implements AutoBase {
	protected static final Logger LOGGER = LogManager.getLogger(DriveWithLimelight.class);

	private final Limelight mLimelight;
	private final DriveToPoint mDriveToPointCommand;

	public DriveWithLimelight(DeepSpaceDriveBase drivetrain, Positioner positioner, Limelight limelight) {
		mLimelight = limelight;
		mDriveToPointCommand = new DriveToPoint(positioner, drivetrain);
	}

	@Override
	public void init() {
		mDriveToPointCommand.setGoal(Double.MAX_VALUE, Double.MAX_VALUE);
		mDriveToPointCommand.init();
	}

	@Override
	public boolean isCompleted() {
		return mDriveToPointCommand.isCompleted();
	}

	@Override
	public void execute() {
		if (mLimelight.seesTarget()) {
			LOGGER.log(Level.INFO, "Sees target... X: " + mLimelight.getTargetX() + ", Y: " + mLimelight.getTargetY());
			mDriveToPointCommand.setGoal(mLimelight.getTargetX(), mLimelight.getTargetY());
			mDriveToPointCommand.execute();
		}
	}

	@Override
	public void stop() {
		mDriveToPointCommand.stop();
	}

}
