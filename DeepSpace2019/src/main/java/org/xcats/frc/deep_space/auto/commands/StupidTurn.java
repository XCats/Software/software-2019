package org.xcats.frc.deep_space.auto.commands;

import edu.wpi.first.wpilibj.Timer;
import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.subsystems.DeepSpaceDriveBase;

public class StupidTurn implements AutoBase {

	private final DeepSpaceDriveBase mSpaceDriveBase;
	private final Timer mTimer = new Timer();

	private final double mTurnSpeed;
	private final double mTurnTime;

	/**
	 * Turns the robot a given speed for a given time.
	 * 
	 * @param spaceDriveBase The drivetrain
	 * @param speed          The speed to turn. Positive speed means turn right,
	 *                       negative turn left
	 * @param time           The time, in seconds, to drive
	 */
	public StupidTurn(DeepSpaceDriveBase spaceDriveBase, double speed, double time) {
		this.mSpaceDriveBase = spaceDriveBase;

		mTurnSpeed = speed;
		mTurnTime = time;
	}

	@Override
	public void init() {
		mTimer.start();
	}

	@Override
	public boolean isCompleted() {
		return mTimer.get() >= mTurnTime;
	}

	@Override
	public void execute() {
		mSpaceDriveBase.getController().setLeftSpeed(mTurnSpeed);
		mSpaceDriveBase.getController().setRightSpeed(-mTurnSpeed);
	}

	@Override
	public void stop() {
		mSpaceDriveBase.getController().setRightSpeed(0);
		mSpaceDriveBase.getController().setLeftSpeed(0);
	}
}
