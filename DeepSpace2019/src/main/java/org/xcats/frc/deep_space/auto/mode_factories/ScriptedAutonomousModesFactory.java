package org.xcats.frc.deep_space.auto.mode_factories;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xcats.frc.deep_space.ProcessorManager;
import org.xcats.frc.deep_space.SubsystemManager;
import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.auto.commands.SequentialCommand;
import org.xcats.frc.deep_space.auto.scripts.AutoScriptLoader;
import org.xcats.frc.deep_space.auto.scripts.CommandFactory;

public class ScriptedAutonomousModesFactory {
	private static final Logger LOGGER = LogManager.getLogger(ScriptedAutonomousModesFactory.class);

	private final Map<String, Supplier<AutoBase>> mAutonomousModes;
	private final AutoScriptLoader mAutoScriptLoader;

	public ScriptedAutonomousModesFactory(SubsystemManager subsystemManager, ProcessorManager processManager) {

		mAutonomousModes = new LinkedHashMap<>();
		mAutoScriptLoader = new AutoScriptLoader(new CommandFactory(subsystemManager, processManager));

		Path autoDirectory = AutoScriptLoader.getAutoModesPath();
		try {
			Files.walk(autoDirectory)
					.filter(Files::isRegularFile).forEach(this::addFile);
		} catch (IOException ex) {
			LOGGER.log(Level.ERROR, "Could not load autonomous scripts " + autoDirectory, ex);
		}
	}

	private void addFile(Path file) {
		Function<Path, AutoBase> parsingFunction = this::parseAutoFile;
		Supplier<AutoBase> wrappedSupplier = () -> parsingFunction.apply(file);
		String prettyName = AutoScriptLoader.getAutoModesPath().relativize(file).toString()
				.replaceAll("\\\\", ".").replaceAll("/", ".");
		
		mAutonomousModes.put(prettyName, wrappedSupplier);
	}

	public Map<String, Supplier<AutoBase>> getScriptedModes() {
		return mAutonomousModes;
	}

	private AutoBase parseAutoFile(Path filename) {
		List<AutoBase> mode = mAutoScriptLoader.loadFile(filename);
		if (mode == null) {
			return null;
		}
		return new SequentialCommand(mode);
	}
}
