package org.xcats.frc.deep_space.auto.commands;

import java.util.List;

import org.xcats.frc.deep_space.auto.AutoBase;

public class ParallelCommand implements AutoBase {

	private final List<AutoBase> mCommands;
	private final boolean mFinishOnFirst;

	public ParallelCommand(List<AutoBase> commands, boolean finishOnFirst) {
		mCommands = commands;
		mFinishOnFirst = finishOnFirst;
	}

	@Override
	public void init() {
		for (AutoBase command : mCommands) {
			command.init();
		}
	}

	@Override
	public boolean isCompleted() {
		return mFinishOnFirst ? isCompleteOnFirstCompleted() : isCompletedAllComplete();
	}

	private boolean isCompleteOnFirstCompleted() {
		boolean finished = false;
		for (AutoBase command : mCommands) {
			finished |= command.isCompleted();
		}
		return finished;
	}

	private boolean isCompletedAllComplete() {
		boolean finished = true;
		for (AutoBase command : mCommands) {
			finished &= command.isCompleted();
		}
		return finished;
	}

	@Override
	public void execute() {
		for (AutoBase command : mCommands) {
			command.execute();
		}
	}

	@Override
	public void stop() {
		for (AutoBase command : mCommands) {
			command.stop();
		}
	}

}
