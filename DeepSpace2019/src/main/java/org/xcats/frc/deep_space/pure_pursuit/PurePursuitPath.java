package org.xcats.frc.deep_space.pure_pursuit;

import java.util.List;

public class PurePursuitPath {

	private final List<PurePursuitWaypoint> mPath;
	private final double mWheelbase;

	public PurePursuitPath(List<PurePursuitWaypoint> path, double wheelbase, double maxVelocity, double maxAcceleration,
			double turnFactor) {
		mPath = path;
		mWheelbase = wheelbase;

		if (mPath.size() < 2) { // NOPMD
			throw new IllegalArgumentException("Path must have at least two points");
		}

		mPath.get(mPath.size() - 1).setCalculatedValues(0, 0);
		for (int i = mPath.size() - 2; i > 0; i--) {
			PurePursuitWaypoint currentPoint = mPath.get(i);
			PurePursuitWaypoint previousPoint = mPath.get(i - 1);
			PurePursuitWaypoint nextPoint = mPath.get(i + 1);

			double curvature = calculatePathCurvature(currentPoint.asVector(), previousPoint.asVector(),
					nextPoint.asVector());
			double turnSpeed = curvature == 0 || Double.isNaN(curvature) ? maxVelocity : turnFactor / curvature;


			double distance = Vector2D.distance(currentPoint.asVector(), nextPoint.asVector());
			double nextVelocity = nextPoint.getPrecalculatedVelocity();
			double maxReachableVel = Math
					.sqrt(nextVelocity * nextVelocity + (2 * maxAcceleration * distance));
			double velocity = Math.min(Math.min(maxVelocity, maxReachableVel), turnSpeed);

			currentPoint.setCalculatedValues(velocity, curvature);
		}
	}

	private double calculatePathCurvature(Vector2D currentPoint, Vector2D previousPoint, Vector2D nextPoint) {
		double x1 = currentPoint.getX();
		double y1 = currentPoint.getY();
		double x1s = x1 * x1;
		double y1s = y1 * y1;

		double x2 = previousPoint.getX();
		double y2 = previousPoint.getY();
		double x2s = x2 * x2;
		double y2s = y2 * y2;

		double x3 = nextPoint.getX();
		double y3 = nextPoint.getY();
		double x3s = x3 * x3;
		double y3s = y3 * y3;

		double k1 = .5 * (x1s + y1s - x2s - y2s) / (x1 - x2 + 1e-4);
		double k2 = (y1 - y2) / (x1 - x2 + 1e-4);
		double b = .5 * (x2s - 2 * x2 * k1 + y2s - x3s + 2 * x3 * k1 - y3s) / (x3 * k2 - y3 + y2 - x2 * k2);
		double a = k1 - k2 * b;
		double radius = Math.sqrt((x1 - a) * (x1 - a) + (y1 - b) * (y1 - b));
		double curvature = 1 / radius;

		return curvature;
	}

	public double calculateCurvatureLookAheadArc(Vector2D robotPosition, double headingDegrees, Vector2D lookahead,
			double lookaheadDistance) {
		double heading = Math.toRadians(headingDegrees);

		Vector2D difference = Vector2D.subtract(lookahead, robotPosition);

		double a = -Math.tan(heading);
		double b = 1;
		double c = (Math.tan(heading) * robotPosition.getY()) - robotPosition.getX();
		double x = Math.abs(a * lookahead.getY() + b * lookahead.getX() + c) / Math.sqrt(a * a + b * b);
		double cross = (Math.sin(heading) * (difference.getY())) - (Math.cos(heading) * (difference.getX()));
		double curvature = (2 * x) / (lookaheadDistance * lookaheadDistance);

		return curvature * -Math.signum(cross);
	}

	public int getClosestPointIndex(int lastClosestPoint, Vector2D robotPosition) {
		double shortestDistance = Double.MAX_VALUE;
		int closestPoint = 0;

		for (int i = lastClosestPoint; i < mPath.size(); i++) {
			Vector2D pathPoint = mPath.get(i).asVector();

			double distance = Vector2D.distance(pathPoint, robotPosition);

			if (distance < shortestDistance) {
				closestPoint = i;
				shortestDistance = distance;
			}
		}

		return closestPoint;
	}

	public Vector2D getLookaheadPoint(int closestPointIndex, Vector2D currentPosition, double lookaheadDistance) {

		for (int i = closestPointIndex; i < mPath.size() - 1; i++) {

			Vector2D startPoint = mPath.get(i).asVector();
			Vector2D endPoint = mPath.get(i + 1).asVector();

			Double intersection = calcIntersectionTVal(startPoint, endPoint, currentPosition, lookaheadDistance);
			if (intersection != null) {
				Vector2D intersectVector = Vector2D.subtract(endPoint, startPoint);
				Vector2D vectorSegment = Vector2D.multiply(intersectVector, intersection);
				Vector2D point = Vector2D.add(startPoint, vectorSegment);
				return point;
			}
		}

		return mPath.get(mPath.size() - 1).asVector();
	}

	private Double calcIntersectionTVal(Vector2D startPoint, Vector2D endPoint, Vector2D currentPosition,
			double lookaheadDistance) {

		Vector2D d = Vector2D.subtract(endPoint, startPoint);
		Vector2D f = Vector2D.subtract(startPoint, currentPosition);

		double a = d.dot(d);
		double b = 2 * f.dot(d);
		double c = f.dot(f) - Math.pow(lookaheadDistance, 2);
		double discriminant = Math.pow(b, 2) - (4 * a * c);

		if (discriminant < 0) {
			return null;
		}

		discriminant = Math.sqrt(discriminant);
		double t1 = (-b - discriminant) / (2 * a);
		double t2 = (-b + discriminant) / (2 * a);

		if (t1 >= 0 && t1 <= 1) {
			return t1;
		}
		if (t2 >= 0 && t2 <= 1) {
			return t2;
		}

		return null;
	}

	public double getLeftWheelVelocity(double targetVelocity, double curvature) {
		return targetVelocity * ((2 + (mWheelbase * curvature))) / 2;
	}

	public double getRightWheelVelocity(double targetVelocity, double curvature) {
		return targetVelocity * ((2 - (mWheelbase * curvature))) / 2;
	}

	public int getPathSize() {
		return mPath.size();
	}

	public PurePursuitWaypoint getWaypoint(int pointIndex) {
		return mPath.get(pointIndex);
	}

	public PurePursuitWaypoint getLastPoint() {
		return mPath.get(mPath.size() - 1);
	}
}
