package org.xcats.frc.deep_space.auto.commands;

import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xcats.frc.deep_space.DeepSpaceProperties;
import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.processors.Positioner;
import org.xcats.frc.deep_space.pure_pursuit.PurePursuitNetworkTable;
import org.xcats.frc.deep_space.pure_pursuit.PurePursuitPath;
import org.xcats.frc.deep_space.pure_pursuit.PurePursuitPathGenerator;
import org.xcats.frc.deep_space.pure_pursuit.PurePursuitWaypoint;
import org.xcats.frc.deep_space.pure_pursuit.Vector2D;
import org.xcats.frc.deep_space.subsystems.DeepSpaceDriveBase;

import edu.wpi.first.wpilibj.TimedRobot;

public class PurePursuitCommand implements AutoBase {
	protected static final Logger sLOGGER = LogManager.getLogger(PurePursuitCommand.class);
	private static final PurePursuitNetworkTable NETWORK_TABLE = new PurePursuitNetworkTable();

	private final DeepSpaceDriveBase mDrivetrain;
	private final Positioner mPositioner;
	private final List<PurePursuitWaypoint> mWaypoints;
	private final PurePursuitPathGenerator mGenerator;
	private final PurePursuitPath mPath;
	private final double mLookaheadDistance;
	private final boolean mIsBackwards;
	private final double mMaxAcceleration;

	private int mIterationCtr;
	private int mLastClosestPoint;
	private double mDistanceToLastPoint;
	private double mLastPrecalculatedVelocity;
	private boolean mFinished;

	@SuppressWarnings("PMD.ExcessiveParameterList")
	public PurePursuitCommand(DeepSpaceDriveBase drivetrain, Positioner positioner, List<PurePursuitWaypoint> waypoints,
			double maxVelocity, double maxAcceleration, double turnFactor,
			double lookaheadDistance, double spacing, double weightSmoothing, boolean isBackwards) {

		mDrivetrain = drivetrain;
		mPositioner = positioner;
		mWaypoints = waypoints;
		mLookaheadDistance = lookaheadDistance;
		mIsBackwards = isBackwards;
		mMaxAcceleration = maxAcceleration;
		mGenerator = new PurePursuitPathGenerator(waypoints, spacing, weightSmoothing, .001);
		mPath = new PurePursuitPath(mGenerator.getSmoothed(), 23.5, maxVelocity, maxAcceleration, turnFactor);
	}

	@Override
	public void init() {
		NETWORK_TABLE.sendConfigToNetworkTable(mWaypoints, mGenerator.getUpsampled(), mGenerator.getSmoothed());
		mLastClosestPoint = 1;
		mIterationCtr = 0;
	}

	@Override
	public void execute() {

		Vector2D robotPosition = new Vector2D(mPositioner.getX(), mPositioner.getY());
		double mHeading = mPositioner.getAngle();

		int closestPoint = mPath.getClosestPointIndex(mLastClosestPoint, robotPosition);
		Vector2D lookaheadPoint = mPath.getLookaheadPoint(closestPoint, robotPosition, mLookaheadDistance);
		Vector2D error = Vector2D.subtract(mPath.getLastPoint().asVector(), robotPosition);

		mDistanceToLastPoint = error.magnitude();
		mFinished = mDistanceToLastPoint < 10;
		if (lookaheadPoint == null) {
			sLOGGER.log(Level.ERROR, "Could not find next point... " + mLastClosestPoint + "(" + mPath.getPathSize()
					+ ") Robot Position "
					+ robotPosition + ", heading " + mHeading + " " + lookaheadPoint);
			return;
		}
		mLastClosestPoint = closestPoint;
		NETWORK_TABLE.sendLookahead(robotPosition, lookaheadPoint);

		double curvature = mPath.calculateCurvatureLookAheadArc(robotPosition, mHeading, lookaheadPoint,
				mLookaheadDistance);

		double kFF = DeepSpaceProperties.PURE_PURSUIT_KVFF.getValue();
		double kP = DeepSpaceProperties.PURE_PURSUIT_KVP.getValue();

		PurePursuitWaypoint closestWaypoint = mPath.getWaypoint(closestPoint);

		double waypointVelocity = closestWaypoint.getPrecalculatedVelocity();

		double velocityDiff = Math.abs(mLastPrecalculatedVelocity - waypointVelocity);
		double limitedWaypointVelocity = waypointVelocity;
		if (velocityDiff > mMaxAcceleration) {
			limitedWaypointVelocity = mLastPrecalculatedVelocity + mMaxAcceleration * TimedRobot.kDefaultPeriod;
		}
		mLastPrecalculatedVelocity = limitedWaypointVelocity;

		double leftTargetVelocity = mPath.getLeftWheelVelocity(limitedWaypointVelocity, curvature);
		double rightTargetVelocity = mPath.getRightWheelVelocity(limitedWaypointVelocity, curvature);

		if (mIsBackwards) {
			leftTargetVelocity *= -1;
			rightTargetVelocity *= -1;
		}

		double leftVel = mDrivetrain.getCurrentState().getLeftEncoderVelocity();
		double rightVel = mDrivetrain.getCurrentState().getRightEncoderVelocity();

		double leftVelError = leftTargetVelocity - leftVel;
		double rightVelError = rightTargetVelocity - rightVel;

		double leftPower = leftTargetVelocity * kFF + leftVelError * kP;
		double rightPower = rightTargetVelocity * kFF + rightVelError * kP;

		NETWORK_TABLE.sendCurrentPointInformation(mIterationCtr, robotPosition.getX(), robotPosition.getY(),
				waypointVelocity, limitedWaypointVelocity, leftVel, leftTargetVelocity, rightVel, rightTargetVelocity);

		sLOGGER.log(Level.INFO,
				"CLosest point " + closestPoint + " -> "
						+ "Robot Position " + robotPosition + ", heading " + mHeading + " " + lookaheadPoint + ", " + curvature
						+ "\n" + "  Left Goal Vel: " + leftTargetVelocity + ", Right Goal Vel: " + rightTargetVelocity
						+ "\n" + "  Left Vel Error: " + leftVelError + ", Right Vel Error: " + rightVelError
						+ "\n" + "  Left Power: " + leftPower + ", Right Power: " + rightPower);

		mDrivetrain.getController().setLeftSpeed(leftPower);
		mDrivetrain.getController().setRightSpeed(rightPower);

		++mIterationCtr;
	}


	@Override
	public boolean isCompleted() {
		return mFinished;
	}

	public double getDistanceError() {
		return mDistanceToLastPoint;
	}

	@Override
	public void stop() {
		mDrivetrain.getController().setLeftSpeed(0);
		mDrivetrain.getController().setRightSpeed(0);

//		NETWORK_TABLE.clearTable();
	}

}
