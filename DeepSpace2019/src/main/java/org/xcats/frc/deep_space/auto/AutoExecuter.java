package org.xcats.frc.deep_space.auto;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xcats.frc.lib.XLoopable;

public class AutoExecuter implements XLoopable {
	private static final Logger LOGGER = LogManager.getLogger(AutoExecuter.class);

	private AutoBase mAutoMode;
	private boolean mFinished;

	public void start(AutoBase autoMode) {
		LOGGER.log(Level.INFO, "Starting auton execution");

		mAutoMode = autoMode;
		mAutoMode.init();
		mFinished = false;
	}

	@Override
	public void getPeriodicInput() {
	}

	@Override
	public void writePeriodicOutput() {
		if (mAutoMode.isCompleted()) {
			mAutoMode.stop();
			if (!mFinished) {
				LOGGER.log(Level.INFO, "Autonomous Finished");
			}
			mFinished = true;
		} else {
			mAutoMode.execute();
		}
	}

}
