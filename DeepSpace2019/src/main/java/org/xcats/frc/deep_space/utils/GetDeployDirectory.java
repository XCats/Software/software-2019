package org.xcats.frc.deep_space.utils;

import java.io.File;

import edu.wpi.first.wpilibj.RobotBase;

public final class GetDeployDirectory {

	private GetDeployDirectory() {

	}

	public static final File getDeployDirectory() {
		File deployDirectory;
		if (RobotBase.isReal()) {
			deployDirectory = new File("/home/lvuser/deploy");
		} else {
			deployDirectory = new File(new File(System.getProperty("user.dir")).getAbsoluteFile(), "src/main/deploy");
		}
		return deployDirectory;
	}
}
