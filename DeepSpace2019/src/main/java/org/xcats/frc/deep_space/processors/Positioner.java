package org.xcats.frc.deep_space.processors;

import org.xcats.frc.deep_space.subsystems.DeepSpaceDriveBase;
import org.xcats.frc.deep_space.utils.CoordinateGuiNetworkTableContainer;
import org.xcats.frc.lib.devices.sensors.XInu;
import org.xcats.frc.lib.logging.XLogString;
import org.xcats.frc.lib.processors.XProcessor;

public class Positioner implements XProcessor {

	private final CoordinateGuiNetworkTableContainer mCoordinateGuiTable;
	private final XInu mImu;
	private final DeepSpaceDriveBase.State mDriveState;

	private double mX;
	private double mY;
	private double mAngle;
	private double mPreviousDistance;

	public Positioner(CoordinateGuiNetworkTableContainer coordinateGuiTable, DeepSpaceDriveBase.State drivetrain,
			XInu inu) {
		mCoordinateGuiTable = coordinateGuiTable;
		mDriveState = drivetrain;
		mImu = inu;
	}

	@Override
	public void getPeriodicInput() {
		double currentDistance = mDriveState.getAvgEncoderPosition();
		double deltaDistance = currentDistance - mPreviousDistance;
		mAngle = mImu.getYaw();
		while (mAngle > 180) {
			mAngle -= 360;
		}
		while (mAngle < -180) {
			mAngle += 360;
		}

		double dX = Math.sin(Math.toRadians(mAngle)) * deltaDistance;
		double dY = Math.cos(Math.toRadians(mAngle)) * deltaDistance;
		mX += dX;
		mY += dY;

		mPreviousDistance = currentDistance;
	}

	public void setPosition(double X, double Y, double Angle) {
		mX = X;
		mY = Y;
		mImu.setYaw(Angle);
	}

	public double getX() {
		return mX;
	}

	public double getY() {
		return mY;
	}

	public double getAngle() {
		return mAngle;
	}

	@Override
	public XLogString getLogString() {
		return null;
	}


	@Override
	public void writePeriodicOutput() {
		mCoordinateGuiTable.setRobotPosition(getX(), getY(), getAngle());
		mImu.pushNetworkTables();
	}

}
