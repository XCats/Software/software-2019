package org.xcats.frc.deep_space.subsystems;

import org.xcats.frc.deep_space.DeepSpaceProperties;
import org.xcats.frc.lib.XSubsystem;
import org.xcats.frc.lib.devices.actuation.pneumatics.impl.XPneumaticCylinder;
import org.xcats.frc.lib.logging.XLogString;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;

public class VelcroPneumaticHatch implements XSubsystem {

	public class Controller {
		private boolean mEjector;
		private boolean mVelcro;

		public void setEjector(boolean ejectorChange) {
			this.mEjector = ejectorChange;
		}

		public void setVelcro(boolean velcroChange) {
			this.mVelcro = velcroChange;
		}
	}

	public class State {
		private boolean mVelcroOut;
		private boolean mEjectorOut;

		/**
		 * Gets whether or not the Hatch handler is deployed
		 *
		 * @return the state of the Hatch Handler. If it is deployed it means we're in acquisition mode. If it is not it means we are in stored mode / hatch placement mode
		 */
		public boolean isVelcroOut() {
			return this.mVelcroOut;
		}

		/**
		 * Gets whether or not the hatch mech is activated
		 *
		 * @return the state of the Hatch mech. If true, then the hatch mechanism is set up to acquire and place hatches. If it is false the hatch mechanism is stored and in the bounding box.
		 */
		public boolean isEjectorOut() {
			return this.mEjectorOut;
		}
	}

	private final State mDesiredState = new State();
	private final State mCurrentState = new State();
	private final Controller mController = new Controller();

	private final XPneumaticCylinder mHatchEjector;
	private final XPneumaticCylinder mHatchVelcro;

	private final NetworkTable mNetworkTable;

	public VelcroPneumaticHatch(XPneumaticCylinder hatchEjector, XPneumaticCylinder hatchVelcro) {
		this.mHatchEjector = hatchEjector;
		this.mHatchVelcro = hatchVelcro;

		mNetworkTable = NetworkTableInstance.getDefault().getTable("RobotData").getSubTable("Hatch Aquisition");
	}

	public Controller getController() {
		return this.mController;
	}

	@Override
	public void initialize() {
		mHatchVelcro.initialize();
		mHatchEjector.initialize();
	}

	@Override
	public void stop() {

	}

	@Override
	public XLogString getLogString() {
		return null;
	}

	@Override
	public void getPeriodicInput() {
		this.mDesiredState.mEjectorOut = this.mController.mEjector;
		this.mDesiredState.mVelcroOut = this.mController.mVelcro;
	}

	@Override
	public void writePeriodicOutput() {
		this.mHatchVelcro.setExtended(this.mDesiredState.mVelcroOut);
		this.mHatchEjector.setExtended(this.mDesiredState.mEjectorOut);

		this.mCurrentState.mEjectorOut = this.mHatchEjector.getState();
		this.mCurrentState.mVelcroOut = this.mHatchVelcro.getState();

		if (DeepSpaceProperties.SMART_DASHBOARD_DEBUG.getValue()) {
			putNetworkTables();
		}
	}

	private void putNetworkTables() {
		mNetworkTable.getEntry("Velcro").setBoolean(mCurrentState.mVelcroOut);
		mNetworkTable.getEntry("Deploy").setBoolean(mCurrentState.mEjectorOut);

	}

	public State getCurrentState() {
		return this.mCurrentState;
	}

	public State getDesiredState() {
		return this.mDesiredState;
	}

}
