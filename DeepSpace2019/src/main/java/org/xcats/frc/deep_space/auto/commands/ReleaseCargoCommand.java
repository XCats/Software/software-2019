package org.xcats.frc.deep_space.auto.commands;

import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.subsystems.PullThroughCargo;

public class ReleaseCargoCommand implements AutoBase {
	private final PullThroughCargo mAquisition;

	public ReleaseCargoCommand(PullThroughCargo aquisition) {
		mAquisition = aquisition;
	}

	@Override
	public void init() {

	}

	@Override
	public boolean isCompleted() {
		return !mAquisition.getCurrentState().getHasBall();
	}

	@Override
	public void execute() {
		mAquisition.getController().setRollerState(PullThroughCargo.RollerState.OUT);
	}

	@Override
	public void stop() {
		mAquisition.getController().setRollerState(PullThroughCargo.RollerState.OFF);
	}
}
