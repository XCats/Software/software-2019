package org.xcats.frc.deep_space.auto.commands.combined;

import java.util.Arrays;

import org.xcats.frc.deep_space.auto.commands.ElevatorGoToHeight;
import org.xcats.frc.deep_space.auto.commands.SequentialCommand;
import org.xcats.frc.deep_space.auto.commands.SetHatchVelcro;
import org.xcats.frc.deep_space.subsystems.Elevator;
import org.xcats.frc.deep_space.subsystems.Elevator.Height;
import org.xcats.frc.deep_space.subsystems.VelcroPneumaticHatch;

public class AcquireHatchSequence extends SequentialCommand {

	public AcquireHatchSequence(Elevator elevator, VelcroPneumaticHatch hatchquisition) {
		super(Arrays.asList(
				new ElevatorGoToHeight(Height.HATCH_OUT_OF_HP, elevator),
				new SetHatchVelcro(hatchquisition, false)));
	}
}
