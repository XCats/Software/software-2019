package org.xcats.frc.deep_space;

import org.xcats.frc.deep_space.auto.commands.DriveWithLimelightStupid;
import org.xcats.frc.deep_space.auto.commands.DriveWithLimelightStupid.TrackingVersion;
import org.xcats.frc.deep_space.auto.commands.combined.AcquireHatchSequence;
import org.xcats.frc.deep_space.processors.Limelight;
import org.xcats.frc.deep_space.subsystems.DeepSpaceDriveBase;
import org.xcats.frc.deep_space.subsystems.Elevator;
import org.xcats.frc.deep_space.subsystems.PullThroughCargo;
import org.xcats.frc.deep_space.subsystems.VelcroPneumaticHatch;

public class RobotStateManager implements IRobotStateManager {

	private final RobotStatePair<DeepSpaceDriveBase.State> mDriveState;
	private final RobotStatePair<Elevator.State> mElevatorState;
	private final RobotStatePair<PullThroughCargo.State> mCargoState;

	// Commands to run in tele
	private final DriveWithLimelightStupid mDriveWithLimelight;
	private final AcquireHatchSequence mAcquireHatchSequence;

	public RobotStateManager(DeepSpaceDriveBase aDriveBase, Elevator aElevator, PullThroughCargo aCargo,
			VelcroPneumaticHatch hatchquisition, Limelight limelight) {
		mDriveState = new RobotStatePair<>(aDriveBase.getDesiredState(), aDriveBase.getCurrentState());
		mElevatorState = new RobotStatePair<>(aElevator.getDesiredState(), aElevator.getCurrentState());
		mCargoState = new RobotStatePair<>(aCargo.getDesiredState(), aCargo.getCurrentState());

		mDriveWithLimelight = new DriveWithLimelightStupid(aDriveBase, limelight, TrackingVersion.Distance);
		mAcquireHatchSequence = new AcquireHatchSequence(aElevator, hatchquisition);
	}

	@Override
	public double getElevatorHighHeight() {

		if (mCargoState.getCurrentState().getHasBall()) {
			return DeepSpaceProperties.CARGO_ROCKET_SHIP_HIGH_PORT;
		} else {
			return DeepSpaceProperties.HATCH_ROCKET_SHIP_HIGH_PORT;
		}

	}

	@Override
	public double getElevatorMediumHeight() {

		if (mCargoState.getCurrentState().getHasBall()) {
			return DeepSpaceProperties.CARGO_ROCKET_SHIP_MED_PORT;
		} else {
			return DeepSpaceProperties.HATCH_ROCKET_SHIP_MED_PORT;
		}

	}

	@Override
	public double getElevatorCargoHeight() {
		return DeepSpaceProperties.CARGO_CARGO_SHIP;
	}

	@Override
	public double getHatchOutOfHpHeight() {
		return DeepSpaceProperties.HATCH_OUT_OF_HP_STATION;
	}

	@Override
	public double getElevatorLowHeight() {

		if (mCargoState.getCurrentState().getHasBall()) {
			return DeepSpaceProperties.CARGO_ROCKET_SHIP_LOW_PORT;
		} else {
			return DeepSpaceProperties.HATCH_ROCKET_SHIP_LOW_PORT;
		}

	}

	public RobotStatePair<DeepSpaceDriveBase.State> getDriveState() {
		return mDriveState;
	}

	public RobotStatePair<Elevator.State> getElevatorState() {
		return mElevatorState;
	}

	@Override
	public void driveToTarget() {
		mDriveWithLimelight.execute();
	}

	@Override
	public AcquireHatchSequence getAquireHatchSequence() {
		return mAcquireHatchSequence;
	}

}
