package org.xcats.frc.deep_space.auto;

/**
 * This is the base for any auto command that is created.
 *
 */

public interface AutoBase {
	/**
	 * In init you perform initialization commands needed ie initialize , will be
	 * called before execute is ever called
	 */
	void init();

	/**
	 * @return true when the auto command is completed
	 */
	boolean isCompleted();

	/**
	 * this is where any actions that are performed on the robot during autonomous
	 * should go this is the loop that will be called in every autonomous periodic
	 * loop
	 */
	void execute();

	/**
	 * this is called after is completed is true
	 */
	void stop();

}
