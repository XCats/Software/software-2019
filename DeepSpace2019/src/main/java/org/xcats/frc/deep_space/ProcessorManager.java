package org.xcats.frc.deep_space;

import java.util.Arrays;

import org.xcats.frc.deep_space.processors.DeepSpaceBlinkinDriver;
import org.xcats.frc.deep_space.processors.Limelight;
import org.xcats.frc.deep_space.processors.Positioner;
import org.xcats.frc.deep_space.subsystems.DeepSpaceDriveBase;
import org.xcats.frc.deep_space.utils.CoordinateGuiNetworkTableContainer;
import org.xcats.frc.lib.XProcessorManager;
import org.xcats.frc.lib.devices.sensors.XInu;

import edu.wpi.first.wpilibj.Spark;

public class ProcessorManager extends XProcessorManager {
	private final Positioner mPositioner;
	private final Limelight mLimelight;
	private final DeepSpaceBlinkinDriver mBlinkin;
	private final XInu mInu;

	public ProcessorManager(DeepSpaceDriveBase.State driveState, XInu inu) {
		CoordinateGuiNetworkTableContainer coordinateGuiTable = new CoordinateGuiNetworkTableContainer();

		this.mInu = inu;
		this.mPositioner = new Positioner(coordinateGuiTable, driveState, this.mInu);
		this.mLimelight = new Limelight(coordinateGuiTable, mPositioner);

		Spark blinkinSpark = new Spark(PortMappings.BLINKEN_PWM);
		this.mBlinkin = new DeepSpaceBlinkinDriver(blinkinSpark);

		super.mProcessors = Arrays.asList(mPositioner, mBlinkin, mLimelight);
	}

	public Positioner getPositioner() {
		return this.mPositioner;
	}

	public DeepSpaceBlinkinDriver getBlinken() {
		return this.mBlinkin;
	}

	public Limelight getLimelight() {
		return mLimelight;
	}

	public XInu getNavx() {
		return this.mInu;
	}
}
