package org.xcats.frc.deep_space.auto.scripts;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xcats.frc.deep_space.DeepSpaceProperties;
import org.xcats.frc.deep_space.ProcessorManager;
import org.xcats.frc.deep_space.SubsystemManager;
import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.auto.StartingPositionManager;
import org.xcats.frc.deep_space.auto.commands.AquireCargoCommand;
import org.xcats.frc.deep_space.auto.commands.DriveToPoint;
import org.xcats.frc.deep_space.auto.commands.DriveWithLimelight;
import org.xcats.frc.deep_space.auto.commands.DriveWithLimelightStupid;
import org.xcats.frc.deep_space.auto.commands.ElevatorGoToHeight;
import org.xcats.frc.deep_space.auto.commands.ParallelCommand;
import org.xcats.frc.deep_space.auto.commands.PurePursuitCommand;
import org.xcats.frc.deep_space.auto.commands.PurePursuitWithCameraOverride;
import org.xcats.frc.deep_space.auto.commands.ReleaseCargoCommand;
import org.xcats.frc.deep_space.auto.commands.SequentialCommand;
import org.xcats.frc.deep_space.auto.commands.SetHatchEjector;
import org.xcats.frc.deep_space.auto.commands.SetHatchVelcro;
import org.xcats.frc.deep_space.auto.commands.SetPosition;
import org.xcats.frc.deep_space.auto.commands.StupidStraight;
import org.xcats.frc.deep_space.auto.commands.StupidTurn;
import org.xcats.frc.deep_space.auto.commands.TrajectoryPathCommand;
import org.xcats.frc.deep_space.auto.commands.TurnToAngle;
import org.xcats.frc.deep_space.auto.commands.WaitCommand;
import org.xcats.frc.deep_space.auto.commands.combined.AcquireHatchSequence;
import org.xcats.frc.deep_space.auto.commands.combined.PrepareForHatch;
import org.xcats.frc.deep_space.pure_pursuit.PurePursuitWaypoint;
import org.xcats.frc.deep_space.subsystems.Elevator;

@SuppressWarnings({ "PMD.TooManyMethods", "PMD.ExcessiveImports", "PMD.AvoidDuplicateLiterals" })
public class CommandFactory {

	private static final Logger LOGGER = LogManager.getLogger(CommandFactory.class);

	private final SubsystemManager mSubsystemManager;
	private final ProcessorManager mProcessManager;

	public CommandFactory(SubsystemManager subsystemManager, ProcessorManager processManager) {
		mSubsystemManager = subsystemManager;
		mProcessManager = processManager;
	}

	@SuppressWarnings("unchecked")
	public List<AutoBase> parseCommandList(List<Object> yamlList) {

		List<AutoBase> parsedCommands = new ArrayList<>();

		boolean parseSuccesful = true;
		for (Object commandYaml : yamlList) {
			if (commandYaml instanceof List) {
				parsedCommands.add(new SequentialCommand(parseCommandList((List<Object>) commandYaml))); // NOPMD
			} else if (commandYaml instanceof Map) {
				try {
					AutoBase command = parseCommand((Map<String, Object>) commandYaml);
					if (command != null) {
						parsedCommands.add(command);
					} else {
						parseSuccesful = false;
					}
				} catch (Exception ex) { // NOPMD
					LOGGER.log(Level.ERROR, "Could not parse command " + commandYaml, ex);
					parseSuccesful = false;
				}
			}
		}

		if (!parseSuccesful) {
			return null;
		}

		return parsedCommands;

	}

	@SuppressWarnings({ "PMD.CyclomaticComplexity", "PMD.NcssCount" })
	public AutoBase parseCommand(Map<String, Object> yamlObject) {
		AutoBase output = null;

		String commandType = (String) yamlObject.get("type");

		switch (commandType) {
			case "AcquireCargo":
				output = parseAquireCargo();
				break;
			case "DriveToPoint":
				output = parseDriveToPoint(yamlObject);
				break;
			case "ElevatorHeight":
				output = parseElevatorGoToheight(yamlObject);
				break;
			case "StupidTurn":
				output = parseDriveStupidTurn(yamlObject);
				break;
			case "StupidDriveStraight":
				output = parseDriveStupidStraight(yamlObject);
				break;
			case "PurePursuit":
				output = parsePurePursuit(yamlObject);
				break;
			case "HatchEjector":
				output = parseHatchEjector(yamlObject);
				break;
			case "HatchVelcro":
				output = parseHatchVelcro(yamlObject);
				break;
			case "ReleaseCargo":
				output = parseReleaseCargo();
				break;
			case "SetPosition":
				output = parseSetPosition(yamlObject);
				break;
			case "Trajectory":
				output = parseTrajectory(yamlObject);
				break;
			case "TurnToAngle":
				output = parseTurnToAngle(yamlObject);
				break;
			case "Parallel":
				output = parseParallelCommand(yamlObject);
				break;
			case "AcquireHatchSequence":
				output = parseAcquireHatchSequence();
				break;
			case "PrepareForHatch":
				output = parsePrepareForHatch();
				break;
			case "DriveWithLimelight":
				output = parseDriveWithLimelight();
				break;
			case "DriveWithLimelightStupid":
				output = parseDriveWithLimelightStupid(yamlObject);
				break;
			case "PurePursuitWithCameraOverride":
				output = parsePurePursuitWithCameraOverride(yamlObject);
				break;
			case "Wait":
				output = parseWaitCommand(yamlObject);
				break;
			default:
				LOGGER.log(Level.ERROR, "Unknown command '" + commandType + "'");
				break;
		}

		return output;
	}

	private double parseDouble(Map<String, Object> yamlObject, String name) {
		return ((Number) yamlObject.get(name)).doubleValue();
	}

	private double parseDouble(Map<String, Object> yamlObject, String name, double defaultValue) {
		if (yamlObject.containsKey(defaultValue)) {
			return ((Number) yamlObject.get(name)).doubleValue();
		}
		return defaultValue;
	}

	private AutoBase parseAquireCargo() {
		return new AquireCargoCommand(mSubsystemManager.getCargoMechanism());
	}

	private AutoBase parseReleaseCargo() {
		return new ReleaseCargoCommand(mSubsystemManager.getCargoMechanism());
	}

	private AutoBase parseDriveToPoint(Map<String, Object> yamlObject) {
		double x = parseDouble(yamlObject, "x");
		double y = parseDouble(yamlObject, "y");

		return new DriveToPoint(mProcessManager.getPositioner(), mSubsystemManager.getDrivetrain(), x, y);
	}

	private AutoBase parseElevatorGoToheight(Map<String, Object> yamlObject) {
		Elevator.Height position = Elevator.Height.valueOf((String) yamlObject.get("height"));

		return new ElevatorGoToHeight(position, mSubsystemManager.getElevator());
	}

	private AutoBase parseDriveStupidStraight(Map<String, Object> yamlObject) {
		double time = parseDouble(yamlObject, "time");
		double speed = parseDouble(yamlObject, "speed");

		return new StupidStraight(mSubsystemManager.getDrivetrain(), time, speed);
	}

	private AutoBase parseDriveStupidTurn(Map<String, Object> yamlObject) {
		double time = parseDouble(yamlObject, "time");
		double speed = parseDouble(yamlObject, "speed");

		return new StupidTurn(mSubsystemManager.getDrivetrain(), time, speed);
	}

	private AutoBase parseTrajectory(Map<String, Object> yamlObject) {
		String pathName = (String) yamlObject.get("path_name");

		return TrajectoryPathCommand.getTrajectoryCommand(mSubsystemManager.getDrivetrain(),
				mProcessManager.getPositioner(), pathName);
	}

	@SuppressWarnings("unchecked")
	private PurePursuitCommand parsePurePursuit(Map<String, Object> yamlObject) {

		double pathSeparation = parseDouble(yamlObject, "path_separation");
		double smoothingWieght = parseDouble(yamlObject, "smoothing_wieght");

		double lookaheadDistance = parseDouble(yamlObject, "lookahead_distance");
		double maxVelocity = parseDouble(yamlObject, "max_velocity", DeepSpaceProperties.PURE_PURSUIT_DEFAULT_VELOCITY.getValue());
		double maxAcceleration = parseDouble(yamlObject, "max_acceleration", DeepSpaceProperties.PURE_PURSUIT_DEFAULT_ACCELERATION.getValue());
		double turnFactor = parseDouble(yamlObject, "turn_factor");

		boolean backwards;
		if (yamlObject.containsKey("is_backwards")) {
			backwards = (Boolean) yamlObject.get("is_backwards");
		} else {
			backwards = false;
		}

		List<PurePursuitWaypoint> waypoints = new ArrayList<>();
		for (List<Number> waypointYaml : (List<List<Number>>) yamlObject.get("waypoints")) { 
			waypoints.add(new PurePursuitWaypoint(waypointYaml.get(0).doubleValue(), waypointYaml.get(1).doubleValue())); // NOPMD
		}

		return new PurePursuitCommand(mSubsystemManager.getDrivetrain(), mProcessManager.getPositioner(), waypoints,
				maxVelocity, maxAcceleration, turnFactor, lookaheadDistance, pathSeparation, smoothingWieght,
				backwards);

	}

	private AutoBase parseHatchEjector(Map<String, Object> yamlObject) {
		String state = yamlObject.get("state").toString();
		return new SetHatchEjector(mSubsystemManager.getHatchMech(), "OUT".equalsIgnoreCase(state));
	}

	private AutoBase parseHatchVelcro(Map<String, Object> yamlObject) {
		String state = yamlObject.get("state").toString();
		return new SetHatchVelcro(mSubsystemManager.getHatchMech(), "OUT".equalsIgnoreCase(state));
	}

	private AutoBase parseTurnToAngle(Map<String, Object> yamlObject) {
		double angle = parseDouble(yamlObject, "angle");

		return new TurnToAngle(mSubsystemManager.getDrivetrain(), mProcessManager.getPositioner(), angle);
	}

	private AutoBase parseSetPosition(Map<String, Object> yamlObject) {
		if (yamlObject.containsKey("position")) {
			StartingPositionManager.StartingPosition position = StartingPositionManager.StartingPosition
					.valueOf((String) yamlObject.get("position"));

			return new SetPosition(mProcessManager.getPositioner(), position);
		} else {
			double x = parseDouble(yamlObject, "x");
			double y = parseDouble(yamlObject, "y");
			double angle = parseDouble(yamlObject, "angle");
			return new SetPosition(mProcessManager.getPositioner(), x, y, angle);
		}

	}

	private AutoBase parseParallelCommand(Map<String, Object> yamlObject) {

		@SuppressWarnings("unchecked")
		List<AutoBase> childrenCommands = parseCommandList((List<Object>) yamlObject.get("commands"));
		boolean finishOnFirst = false;

		return new ParallelCommand(childrenCommands, finishOnFirst);

	}

	private AutoBase parseAcquireHatchSequence() {
		return new AcquireHatchSequence(mSubsystemManager.getElevator(), mSubsystemManager.getHatchMech());
	}

	private AutoBase parsePrepareForHatch() {
		return new PrepareForHatch(mSubsystemManager.getElevator(), mSubsystemManager.getHatchMech());
	}

	private AutoBase parseDriveWithLimelight() {
		return new DriveWithLimelight(mSubsystemManager.getDrivetrain(), mProcessManager.getPositioner(),
				mProcessManager.getLimelight());
	}

	private DriveWithLimelightStupid parseDriveWithLimelightStupid(Map<String, Object> yamlObject) {
		DriveWithLimelightStupid.TrackingVersion trackingVersion = DriveWithLimelightStupid.TrackingVersion
				.valueOf((String) yamlObject.get("tracking_version"));

		return new DriveWithLimelightStupid(mSubsystemManager.getDrivetrain(),
				mProcessManager.getLimelight(), trackingVersion);
	}

	@SuppressWarnings("unchecked")
	private AutoBase parsePurePursuitWithCameraOverride(Map<String, Object> yamlObject) {

		PurePursuitCommand purePursuit = parsePurePursuit((Map<String, Object>) yamlObject.get("pure_pursuit"));
		DriveWithLimelightStupid driveWithLimelight = parseDriveWithLimelightStupid((Map<String, Object>) yamlObject.get("limelight"));

		double overrideDistance = parseDouble(yamlObject, "override_distance");

		return new PurePursuitWithCameraOverride(purePursuit, driveWithLimelight,
				mProcessManager.getLimelight(), overrideDistance);
	}

	private AutoBase parseWaitCommand(Map<String, Object> yamlObject) {
		double time = parseDouble(yamlObject, "time");
		return new WaitCommand(time);
	}
}
