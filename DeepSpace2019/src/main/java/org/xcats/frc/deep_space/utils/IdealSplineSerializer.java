package org.xcats.frc.deep_space.utils;

import jaci.pathfinder.Trajectory.Segment;

/**
 * Serializes a 2D cubic spline into a string, to be used by the SmartDashboard.
 * 
 * @author PJ
 *
 */
public final class IdealSplineSerializer {

	/**
	 * Constructor, private because the static functions should be used.
	 */
	private IdealSplineSerializer() {

	}

	static String serializePath(boolean isPathfinder, Segment[] aAverage, Segment[] aRight, Segment[] aLeft) { // NOPMD
		StringBuilder output = new StringBuilder();

		for (int i = 0; i < aAverage.length; ++i) {
			output.append(serializePathPoint(isPathfinder, aAverage[i], aRight[i], aLeft[i]));
		}

		return output.toString();
	}

	/**
	 * Serializes a single spline point.
	 * 
	 * @param aPoint The piont to serialize
	 * 
	 * @return The serialized point
	 */
	@SuppressWarnings("PMD.DataflowAnomalyAnalysis")
	static String serializePathPoint(boolean isPathfinder, Segment aAverage, Segment aLeft, Segment aRight) {

		double leftPosition = aLeft.position;
		double leftVelocity = aLeft.velocity;
		double rightPosition = aRight.position;
		double rightVelocity = aRight.velocity;
		double x = aAverage.x;
		double y = aAverage.y;
		double heading = aAverage.heading;
		

		if (isPathfinder) {
			x = aAverage.y - 162;
			y = aAverage.x - 324;
			heading = Math.toDegrees(heading);
		}

		while (heading > 360) {
			heading -= 360;
		}
		while (heading < 0) {
			heading += 360;
		}

		return leftPosition + ", "
				+ leftVelocity + ", "
				+ rightPosition + ", "
				+ rightVelocity + ", "
				+ heading + ","
				+ x + ","
				+ y + ",";
	}
}
