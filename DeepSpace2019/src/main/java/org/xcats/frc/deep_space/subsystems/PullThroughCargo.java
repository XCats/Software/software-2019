package org.xcats.frc.deep_space.subsystems;

import org.xcats.frc.deep_space.DeepSpaceProperties;
import org.xcats.frc.lib.XSubsystem;
import org.xcats.frc.lib.logging.XLogString;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.SpeedController;

public class PullThroughCargo implements XSubsystem {

	public enum RollerState {
		IN, OUT, OFF
	}

	public class Controller {
		private RollerState mRollerState = RollerState.OFF;

		public void setRollerState(RollerState newState) {
			mRollerState = newState;
		}
	}

	public class State {
		private double mRollerSpeed;

		private RollerState mRollerState = RollerState.OFF;

		public RollerState getRollerState() {
			return mRollerState;
		}

		public boolean getHasBall() {
			return false;
		}

		public double getRollerSpeed() {
			return mRollerSpeed;
		}
	}

	private final Controller mController = new Controller();
	private final State mDesiredState = new State();
	private final State mCurrentState = new State();
	private final SpeedController mRollieMotor;

	private final NetworkTable mNetworkTable;

	public PullThroughCargo(SpeedController rollieMotor) {
		mRollieMotor = rollieMotor;

		mNetworkTable = NetworkTableInstance.getDefault().getTable("RobotData").getSubTable("Cargo Aquisition");
	}

	public State getCurrentState() {
		return mCurrentState;
	}

	public State getDesiredState() {
		return mDesiredState;
	}

	public Controller getController() {
		return mController;
	}

	@Override
	public void initialize() {

	}

	@Override
	public void stop() {

	}

	@Override
	public XLogString getLogString() {
		return null;
	}

	@Override
	public void getPeriodicInput() {
		mDesiredState.mRollerState = mController.mRollerState;
	}

	@Override
	public void writePeriodicOutput() {
		switch (mDesiredState.getRollerState()) {
			case IN:
				mRollieMotor.set(DeepSpaceProperties.CARGO_ACQUISITION_SPEED.getValue());
				break;
			case OUT:
				mRollieMotor.set(-DeepSpaceProperties.CARGO_ACQUISITION_SPEED.getValue());
				break;
			default:
				mRollieMotor.set(0);
				break;
		}

		mCurrentState.mRollerSpeed = mRollieMotor.get();

		if (DeepSpaceProperties.SMART_DASHBOARD_DEBUG.getValue()) {
			putNetworkTableInfo();
		}
	}

	private void putNetworkTableInfo() {
		mNetworkTable.getEntry("Has Ball").setBoolean(mCurrentState.getHasBall());
		mNetworkTable.getEntry("Roller Speed").setDouble(mCurrentState.getRollerSpeed());
	}
}
