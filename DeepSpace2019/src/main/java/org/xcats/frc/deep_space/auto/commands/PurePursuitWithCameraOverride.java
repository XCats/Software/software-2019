package org.xcats.frc.deep_space.auto.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.processors.Limelight;

public class PurePursuitWithCameraOverride implements AutoBase {
	protected static final Logger LOGGER = LogManager.getLogger(PurePursuitWithCameraOverride.class);

	private final PurePursuitCommand mPurePursuitCommand;
	private final DriveWithLimelightStupid mDriveWithLimelightCommand;
	private final Limelight mLimelight;
	private final double mOverrideDistance;

	private boolean mUsingLimelight;

	public PurePursuitWithCameraOverride(PurePursuitCommand purePursuitCommand,
			DriveWithLimelightStupid driveWithLimelightCommand, Limelight limelight, double overrideDistance) {
		mPurePursuitCommand = purePursuitCommand;
		mDriveWithLimelightCommand = driveWithLimelightCommand;
		mLimelight = limelight;
		mOverrideDistance = overrideDistance;
	}

	@Override
	public void init() {
		mUsingLimelight = false;
		mPurePursuitCommand.init();
	}

	@Override
	public boolean isCompleted() {
		if (mUsingLimelight) {
			LOGGER.info("Checking if limelight is done" + mDriveWithLimelightCommand.isCompleted());
			return mDriveWithLimelightCommand.isCompleted();
		} else {
			return mPurePursuitCommand.isCompleted();
		}
	}

	@Override
	public void execute() {
		if (mUsingLimelight) {
			mDriveWithLimelightCommand.execute();
			LOGGER.info("Doing limelight");
		} else {
			if (mLimelight.seesTarget() && mPurePursuitCommand.getDistanceError() < mOverrideDistance) {
				LOGGER.info("Switching to limelight... " + mPurePursuitCommand.getDistanceError());
				mUsingLimelight = true;
				mDriveWithLimelightCommand.init();
			} else {
				LOGGER.info("Doing pure pursuit. Error: " + mPurePursuitCommand.getDistanceError());
				mPurePursuitCommand.execute();
			}
		}
	}

	@Override
	public void stop() {
		if (mUsingLimelight) {
			mDriveWithLimelightCommand.stop();
		} else {
			mPurePursuitCommand.stop();
		}
	}

}
