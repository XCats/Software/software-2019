package org.xcats.frc.deep_space.auto.commands;

import org.xcats.frc.deep_space.DeepSpaceProperties;
import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.processors.Positioner;
import org.xcats.frc.deep_space.subsystems.DeepSpaceDriveBase;

public class TurnToAngle implements AutoBase {

	private final DeepSpaceDriveBase mDrivetrain;
	private final Positioner mPositioner;
	private final double mAngle;
	private final double mDeadband;
	private double mLastError;
	private double mError;

	public TurnToAngle(DeepSpaceDriveBase drivetrain, Positioner positioner, double angle) {
		this(drivetrain, positioner, angle, 10);
	}

	public TurnToAngle(DeepSpaceDriveBase drivetrain, Positioner positioner, double angle, double deadband) {
		mDrivetrain = drivetrain;
		mPositioner = positioner;
		mAngle = angle;
		mDeadband = deadband;
	}

	@Override
	public void init() {
	}

	@Override
	public boolean isCompleted() {
		return Math.abs(mError) <= mDeadband;
	}

	@Override
	public void execute() {
		mError = mAngle - mPositioner.getAngle();
		while (mError > 180) {
			mError -= 360;
		}
		while (mError < -180) {
			mError += 360;
		}

		double speed = mError * DeepSpaceProperties.TURN_TO_ANGLE_KP.getValue()
				+ mLastError * DeepSpaceProperties.TURN_TO_ANGLE_KD.getValue();
		mDrivetrain.getController().setLeftSpeed(speed);
		mDrivetrain.getController().setRightSpeed(-speed);

		mLastError = mError;
	}

	@Override
	public void stop() {
		mDrivetrain.getController().setRightSpeed(0);
		mDrivetrain.getController().setLeftSpeed(0);
	}
}
