package org.xcats.frc.deep_space;

import org.xcats.frc.deep_space.subsystems.Elevator;
import org.xcats.frc.deep_space.subsystems.PullThroughCargo;
import org.xcats.frc.lib.XLoggable;
import org.xcats.frc.lib.XLoopable;
import org.xcats.frc.lib.logging.XLogString;

import edu.wpi.first.wpilibj.Joystick;


// For Karenna - Right IN Left OUT
public class OperatorController implements XLoopable, XLoggable {

	public OperatorController(Joystick operatorController) {
		this.mOperatorController = operatorController;
	}

	@SuppressWarnings("PMD.TooManyFields") // TODO, maybe break this up into hatch/cargo/fourbar/elevator structs
	public class State {
		//Joystick
		private double mElevatorJS;
		private double mFourBarJS;

		//Buttons
		private Elevator.Height mElevatorPosition = Elevator.Height.OFF;
		private PullThroughCargo.RollerState mCargoRollers;

		//Hatch Mech
		private boolean mHatchVelcro;
		private boolean mHatchEjector;

		// Command runners
		private boolean mRunAcquireHatchSequence;
		private boolean mRunAcquireHatchSequenceChanged;


		// getters
		public PullThroughCargo.RollerState getCargoRollers() {
			return mCargoRollers;
		}

		public boolean getHatchVelcro() {
			return mHatchVelcro;
		}

		public boolean getHatchEjector() {
			return mHatchEjector;
		}

		public double getElevatorJS() {
			return mElevatorJS;
		}

		public double getFourBarJS() {
			return mFourBarJS;
		}

		public Elevator.Height getElevatorPosition() {
			return mElevatorPosition;
		}

		public void setHatchVelcro(boolean out) {
			mHatchVelcro = out;
		}

		public boolean getRunHatchAcquisitionSequence() {
			return mRunAcquireHatchSequence;
		}

		public boolean getRunHatchAcquisitionSequenceChanged() {
			return mRunAcquireHatchSequenceChanged;
		}
	}

	private final Joystick mOperatorController;
	private final State mCurrentState = new State();

	public State getCurrentState() {
		return this.mCurrentState;
	}

	@Override
	public XLogString getLogString() {
		return null;
	}

	@Override
	public void getPeriodicInput() {
		readCommands();
		readElevator();
		readHatch();
		readCargo();
		readFourBar();
	}

	private void readCommands() {

		boolean acquirePressed = this.mOperatorController.getPOV() == XboxButtonMap.D_PAD_UP;

		// If the state has changed, reset the command
		this.mCurrentState.mRunAcquireHatchSequenceChanged = acquirePressed != this.mCurrentState.mRunAcquireHatchSequence;

		this.mCurrentState.mRunAcquireHatchSequence = acquirePressed;
	}

	private void readElevator() {

		// elevator
		if (Math.abs(mOperatorController.getRawAxis(
				XboxButtonMap.RIGHT_Y_AXIS)) < DeepSpaceProperties.XBOX_JS_DEADBAND.getValue()) {
			this.mCurrentState.mElevatorJS = 0;
		} else {
			this.mCurrentState.mElevatorJS = mOperatorController.getRawAxis(XboxButtonMap.RIGHT_Y_AXIS) * .8;
		}

		boolean highPressed = this.mOperatorController.getRawButton(XboxButtonMap.Y_BUTTON);
		boolean midPressed = this.mOperatorController.getRawButton(XboxButtonMap.X_BUTTON);
		boolean lowPressed = this.mOperatorController.getRawButton(XboxButtonMap.A_BUTTON);
		boolean cargoPressed = this.mOperatorController.getRawButton(XboxButtonMap.B_BUTTON);
		boolean hatchFromHpPressed = this.mOperatorController.getPOV() == XboxButtonMap.D_PAD_RIGHT;

		if (hatchFromHpPressed) {
			mCurrentState.mElevatorPosition = Elevator.Height.HATCH_OUT_OF_HP;
		} else if (highPressed) {
			mCurrentState.mElevatorPosition = Elevator.Height.HIGH;
		} else if (midPressed) {
			mCurrentState.mElevatorPosition = Elevator.Height.MED;
		} else if (lowPressed) {
			mCurrentState.mElevatorPosition = Elevator.Height.LOW;
		} else if (cargoPressed) {
			mCurrentState.mElevatorPosition = Elevator.Height.CARGO;
		} else {
			mCurrentState.mElevatorPosition = Elevator.Height.OFF;
		}
	}

	private void readHatch() {
		if (mOperatorController.getRawButtonPressed(XboxButtonMap.LB_BUTTON)) {
			this.mCurrentState.mHatchEjector = !this.mCurrentState.mHatchEjector;
		}

		if (mOperatorController.getRawButtonPressed(XboxButtonMap.RB_BUTTON)) {
			this.mCurrentState.mHatchVelcro = !this.mCurrentState.mHatchVelcro;
		}
	}

	private void readCargo() {
		if (mOperatorController.getRawAxis(XboxButtonMap.LEFT_TRIGGER) > 0) {
			this.mCurrentState.mCargoRollers = PullThroughCargo.RollerState.OUT;
		} else if (mOperatorController.getRawAxis(XboxButtonMap.RIGHT_TRIGGER) > 0) {
			this.mCurrentState.mCargoRollers = PullThroughCargo.RollerState.IN;
		} else {
			this.mCurrentState.mCargoRollers = PullThroughCargo.RollerState.OFF;
		}
	}

	private void readFourBar() {
		if (Math.abs(mOperatorController.getRawAxis(XboxButtonMap.LEFT_Y_AXIS)) < DeepSpaceProperties.XBOX_JS_DEADBAND.getValue()) {
			this.mCurrentState.mFourBarJS = 0;
		} else {
			this.mCurrentState.mFourBarJS = mOperatorController.getRawAxis(XboxButtonMap.LEFT_Y_AXIS);
		}
	}

	@Override
	public void writePeriodicOutput() {

	}
}
