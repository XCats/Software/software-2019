package org.xcats.frc.deep_space.auto.commands;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.wpi.first.wpilibj.RobotBase;
import org.xcats.frc.deep_space.DeepSpaceProperties;
import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.processors.Positioner;
import org.xcats.frc.deep_space.subsystems.DeepSpaceDriveBase;
import org.xcats.frc.deep_space.utils.CoordinateGuiNetworkTableContainer;
import jaci.pathfinder.Pathfinder;
import jaci.pathfinder.Trajectory;
import jaci.pathfinder.followers.DistanceFollower;

public class TrajectoryPathCommand implements AutoBase {
	protected static final Logger sLOGGER = LogManager.getLogger(TrajectoryPathCommand.class);

	private final CoordinateGuiNetworkTableContainer mNTC;
	private final DeepSpaceDriveBase.State mDrivetrainState;
	private final DeepSpaceDriveBase.Controller mDrivetrainController;
	private final Positioner mPositioner;
	private final Trajectory mAverageTrajectory;
	private final Trajectory mLeftTrajectory;
	private final Trajectory mRightTrajectory;
	private final DistanceFollower mFollowerLeft;
	private final DistanceFollower mFollowerRight;
	private double mStartingLeftDistance;
	private double mStartingRightDistance;

	private int mSegmentCtr;

	/**
	 * Creates the trajectory path command.
	 * 
	 * @param aDrivetrain computer's drivetain
	 * @param aPositioner computer's positioner
	 * @param aPath       Trajectory path
	 */
	public TrajectoryPathCommand(DeepSpaceDriveBase.State aDrivetrainState,
			DeepSpaceDriveBase.Controller aDrivetrainController, Positioner aPositioner,
			Trajectory averageTrajectory, Trajectory leftTrajectory, Trajectory rightTrajectory) {
		mAverageTrajectory = averageTrajectory;
		mLeftTrajectory = leftTrajectory;
		mRightTrajectory = rightTrajectory;

		mDrivetrainState = aDrivetrainState;
		mDrivetrainController = aDrivetrainController;
		mPositioner = aPositioner;
		mNTC = new CoordinateGuiNetworkTableContainer();

		mFollowerLeft = new DistanceFollower(mLeftTrajectory);
		mFollowerRight = new DistanceFollower(mRightTrajectory);

		if (mNTC.isIdealTrajectorySet()) {
			sendIdealPath();
		}
	}

	@Override
	public void init() {
		sendIdealPath();

		mSegmentCtr = 0;

		double kP = DeepSpaceProperties.DRIVE_PATH_KP.getValue();
		double kI = 0;
		double kD = 0;
		double kVelocity = DeepSpaceProperties.DRIVE_PATH_KV.getValue();
		double kAccel = DeepSpaceProperties.DRIVE_PATH_KA.getValue();

		mFollowerLeft.configurePIDVA(kP, kI, kD, kVelocity, kAccel);
		mFollowerRight.configurePIDVA(kP, kI, kD, kVelocity, kAccel);

		mFollowerLeft.reset();
		mFollowerRight.reset();

		mStartingLeftDistance = mDrivetrainState.getLeftEncoderDistance();
		mStartingRightDistance = mDrivetrainState.getRightEncoderDistance();

		sLOGGER.log(Level.INFO, "Starting to drive path...  " + mStartingLeftDistance + ", " + mStartingRightDistance);
	}

	@Override
	public void execute() {
		double distanceL = mDrivetrainState.getLeftEncoderDistance() - mStartingLeftDistance;
		double distanceR = mDrivetrainState.getRightEncoderDistance() - mStartingRightDistance;

		double goalHeading = Math.toDegrees(mFollowerLeft.getHeading());
		double observedHeading = mPositioner.getAngle();
		double headingDifference = Pathfinder.boundHalfDegrees(goalHeading - observedHeading);

		double turn = DeepSpaceProperties.SPLINE_TURN_FACTOR.getValue() * headingDifference;
		double speedLeft = mFollowerLeft.calculate(distanceL);
		double speedRight = mFollowerRight.calculate(distanceR);

		sLOGGER.log(Level.INFO, "heading (" + observedHeading + ", " + goalHeading + ", " + headingDifference + " -- " + turn + ")\n"
				+ "Left (" + distanceL + ", " + mDrivetrainState.getLeftEncoderVelocity() + ", " + speedLeft + ")\n"
				+ "Right (" + distanceR + ", " + mDrivetrainState.getRightEncoderVelocity() + speedRight + ")");

		mDrivetrainController.setLeftSpeed(speedLeft + turn);
		mDrivetrainController.setRightSpeed(speedRight - turn);

		mNTC.setMeasuredTrajectory(false, mSegmentCtr++, distanceL, mDrivetrainState.getLeftEncoderVelocity(),
				distanceR, mDrivetrainState.getRightEncoderVelocity(), mPositioner.getX(),
				mPositioner.getY(), observedHeading);
	}

	@Override
	public boolean isCompleted() {
		boolean finished = mFollowerLeft.isFinished();
		if (finished) {
			sLOGGER.log(Level.WARN, "Trajectory Finished");
			mDrivetrainController.setLeftSpeed(0);
			mDrivetrainController.setRightSpeed(0);
		}
		return finished;
	}

	private void sendIdealPath() {
		mNTC.setIdealTrajectory(true, mAverageTrajectory, mLeftTrajectory, mRightTrajectory);
	}

	@Override
	public void stop() {
		mDrivetrainController.setLeftSpeed(0);
		mDrivetrainController.setRightSpeed(0);
	}

	private static File getTrajectoryFile(String name) {

		// TODO this might get fixed in an update to wpilib...
		File baseFile;
		if (RobotBase.isReal()) {
			baseFile = new File("/home/lvuser/deploy");
		} else {
			baseFile = new File(new File(System.getProperty("user.dir")).getAbsoluteFile(), "src/main/deploy");
		}
		return new File(baseFile, "pathweaver/output/" + name + ".pf1.csv");
	}

	public static AutoBase getTrajectoryCommand(DeepSpaceDriveBase drivetrain, Positioner positioner, String pathName) {

		// Flipped on purpose
		final String leftTrajectorySuffix = ".right";
		final String rightTrajectorySuffix = ".left";

		File averageFile = getTrajectoryFile(pathName);
		File leftFile = getTrajectoryFile(pathName + leftTrajectorySuffix);
		File rightFile = getTrajectoryFile(pathName + rightTrajectorySuffix);

		try {
			Trajectory averageTrajectory = Pathfinder.readFromCSV(averageFile);
			Trajectory leftTrajectory = Pathfinder.readFromCSV(leftFile);
			Trajectory rightTrajectory = Pathfinder.readFromCSV(rightFile);

			return new TrajectoryPathCommand(drivetrain.getCurrentState(), drivetrain.getController(), positioner,
					averageTrajectory, leftTrajectory, rightTrajectory);
		} catch (IOException ex) {
			sLOGGER.log(Level.ERROR, "Could not load trajectory " + pathName, ex);
		}

		return new DoNothing(false);
	}
}
