package org.xcats.frc.deep_space.subsystems;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import org.xcats.frc.deep_space.processors.Positioner;
import org.xcats.frc.deep_space.test_utilities.BaseTestFixture;

public class PositionerTest extends BaseTestFixture {

	private static final double EPSILON = 0.01;

	@Test
	public void testSetPosition() {
		Positioner positioner = mRobot.getProcessorManager().getPositioner();

		assertEquals(0, positioner.getX(), EPSILON);
		assertEquals(0, positioner.getY(), EPSILON);
		assertEquals(0, positioner.getAngle(), EPSILON);

		simulateForTime(.5, () -> {
			positioner.setPosition(5, 12, 45);
			positioner.getPeriodicInput();
		});

		assertEquals(5, positioner.getX(), EPSILON);
		assertEquals(12, positioner.getY(), EPSILON);
		assertEquals(45, positioner.getAngle(), EPSILON);

		simulateForTime(.5, () -> {
			positioner.setPosition(5, 12, -17);
			positioner.getPeriodicInput();
		});

		assertEquals(5, positioner.getX(), EPSILON);
		assertEquals(12, positioner.getY(), EPSILON);
		assertEquals(-17, positioner.getAngle(), EPSILON);
	}
}
