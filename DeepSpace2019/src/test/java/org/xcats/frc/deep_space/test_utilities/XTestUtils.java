package org.xcats.frc.deep_space.test_utilities;

import org.xcats.frc.lib.XLoopable;

public final class XTestUtils {
	private XTestUtils() {}

	public static void runLoop(XLoopable loopable) {
		loopable.getPeriodicInput();
		loopable.writePeriodicOutput();
	}
}
