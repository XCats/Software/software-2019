package org.xcats.frc.deep_space.test_utilities;

import com.snobot.simulator.DefaultDataAccessorFactory;
import com.snobot.simulator.wrapper_accessors.DataAccessorFactory;
import com.snobot.simulator.wrapper_accessors.SimulatorDataAccessor.SnobotLogLevel;

import org.xcats.frc.deep_space.Robot;
import org.xcats.frc.deep_space.XcatsSimulator;

public class BaseTestFixture {
	private static boolean INITIALIZED;

	protected Robot mRobot;
	protected XcatsSimulator mSimulator;

	protected BaseTestFixture() {
		setupSimulator();

		mSimulator = new XcatsSimulator();
		mSimulator.loadConfig("simulator_config/simulator_config.yml");

		mRobot = new Robot();
		mRobot.robotInit();

		mSimulator.setRobot(mRobot);
	}

	private void setupSimulator() {
		if (!INITIALIZED) {
			DefaultDataAccessorFactory.initalize();
			DataAccessorFactory.getInstance().getSimulatorDataAccessor().setLogLevel(SnobotLogLevel.DEBUG);

			INITIALIZED = true;
		}

		DataAccessorFactory.getInstance().getSimulatorDataAccessor().reset();
	}

	protected void simulateForTime(double aSeconds) {
		simulateForTime(aSeconds, () -> {
		});
	}

	protected void simulateForTime(double aSeconds, Runnable aTask) {
		simulateForTime(aSeconds, .02, aTask);
	}

	protected void simulateForTime(double aSeconds, double aUpdatePeriod, Runnable aTask) {
		double updateFrequency = 1 / aUpdatePeriod;

		for (int i = 0; i < updateFrequency * aSeconds; ++i) {
			aTask.run();
			mSimulator.update();
			mRobot.robotPeriodic();
			DataAccessorFactory.getInstance().getSimulatorDataAccessor().updateSimulatorComponents(aUpdatePeriod);
		}
	}
}
