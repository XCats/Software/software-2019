package org.xcats.frc.deep_space.subsystems;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import org.xcats.frc.deep_space.test_utilities.BaseTestFixture;

public class DeepSpaceDriveBaseTest extends BaseTestFixture {

	@Test
	public void testDriveForwards() {
		DeepSpaceDriveBase drivetrain = mRobot.getSubsystemManager().getDrivetrain();

		simulateForTime(.5, () -> {
			drivetrain.getController().setLeftSpeed(1);
			drivetrain.getController().setRightSpeed(1);
			mRobot.teleopPeriodic();

			assertEquals(drivetrain.getCurrentState().getLeftSpeed(), 1.0, 0.0001);
			assertEquals(drivetrain.getCurrentState().getRightSpeed(), 1.0, 0.0001);
		});
	}

}
