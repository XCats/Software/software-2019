package org.xcats.frc.deep_space.auto.commands;

import org.junit.Assert;
import org.junit.Test;

import org.xcats.frc.deep_space.processors.Positioner;
import org.xcats.frc.deep_space.test_utilities.BaseTestFixture;

public class DriveToPositionTest extends BaseTestFixture {

	private static final double EPSILON = 0.01;

	@Test
	public void testCenterTo10_10() {
		Positioner positioner = mRobot.getProcessorManager().getPositioner();
		positioner.setPosition(0, 0, 0);

		DriveToPoint driver = new DriveToPoint(mRobot.getProcessorManager().getPositioner(),
				mRobot.getSubsystemManager().getDrivetrain());
		driver.setGoal(10, 10);
		driver.execute();

		Assert.assertEquals(Math.sqrt(200), driver.getDistanceError(), EPSILON);
		Assert.assertEquals(45, driver.getAngleError(), EPSILON);
	}

	@Test
	public void testCenterToNeg10_Neg10() {
		Positioner positioner = mRobot.getProcessorManager().getPositioner();
		positioner.setPosition(0, 0, 0);

		DriveToPoint driver = new DriveToPoint(mRobot.getProcessorManager().getPositioner(),
				mRobot.getSubsystemManager().getDrivetrain());
		driver.setGoal(-10, -10);
		driver.execute();

		Assert.assertEquals(Math.sqrt(200), driver.getDistanceError(), EPSILON);
		Assert.assertEquals(-135, driver.getAngleError(), EPSILON);
	}

	@Test
	public void test10_0_90_to_10_10() {
		Positioner positioner = mRobot.getProcessorManager().getPositioner();
		simulateForTime(.5, () -> {
			positioner.setPosition(10, 0, 90);
			mRobot.teleopPeriodic();
		});

		DriveToPoint driver = new DriveToPoint(mRobot.getProcessorManager().getPositioner(),
				mRobot.getSubsystemManager().getDrivetrain());
		driver.setGoal(10, 10);
		driver.execute();

		Assert.assertEquals(Math.sqrt(100), driver.getDistanceError(), EPSILON);
		Assert.assertEquals(-90, driver.getAngleError(), EPSILON);
	}

	@Test
	public void test5_5_30_to_10_10() {
		Positioner positioner = mRobot.getProcessorManager().getPositioner();
		simulateForTime(.5, () -> {
			positioner.setPosition(5, 5, 30);
			mRobot.teleopPeriodic();
		});

		DriveToPoint driver = new DriveToPoint(mRobot.getProcessorManager().getPositioner(),
				mRobot.getSubsystemManager().getDrivetrain());
		driver.setGoal(10, 10);
		driver.execute();

		Assert.assertEquals(Math.sqrt(50), driver.getDistanceError(), EPSILON);
		Assert.assertEquals(15, driver.getAngleError(), EPSILON);
	}

	@Test
	public void test3_4_45_to_neg87_neg43() {
		Positioner positioner = mRobot.getProcessorManager().getPositioner();
		simulateForTime(.5, () -> {
			positioner.setPosition(3, 4, 45);
			mRobot.teleopPeriodic();
		});

		DriveToPoint driver = new DriveToPoint(mRobot.getProcessorManager().getPositioner(),
				mRobot.getSubsystemManager().getDrivetrain());
		driver.setGoal(-87, -43);
		driver.execute();

		Assert.assertEquals(101.533, driver.getDistanceError(), EPSILON);
		Assert.assertEquals(-162.5745639596944, driver.getAngleError(), EPSILON);
	}
}
