package org.xcats.frc.deep_space.auto.scripts;

import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Supplier;

import org.junit.Assert;
import org.junit.Test;

import org.xcats.frc.deep_space.auto.AutoBase;
import org.xcats.frc.deep_space.auto.mode_factories.ScriptedAutonomousModesFactory;
import org.xcats.frc.deep_space.test_utilities.BaseTestFixture;

public class AutonomsModeFactoryTest extends BaseTestFixture {

	@Test
	public void loadAllFiles() {
		ScriptedAutonomousModesFactory factory = new ScriptedAutonomousModesFactory(mRobot.getSubsystemManager(),
				mRobot.getProcessorManager());
		Map<String, Supplier<AutoBase>> modes = factory.getScriptedModes();

		for (Entry<String, Supplier<AutoBase>> pair : modes.entrySet()) {
			AutoBase mode = pair.getValue().get();
			Assert.assertNotNull("Failed on " + pair.getKey(), mode);
		}
	}

}
