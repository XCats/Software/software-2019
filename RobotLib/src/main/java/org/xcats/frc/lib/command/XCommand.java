package org.xcats.frc.lib.command;

import org.xcats.frc.lib.XLoggable;
import org.xcats.frc.lib.XLoopable;

public interface XCommand extends XLoggable, XLoopable {
	void start();

	void cancel();

	boolean isCompleted();
}
