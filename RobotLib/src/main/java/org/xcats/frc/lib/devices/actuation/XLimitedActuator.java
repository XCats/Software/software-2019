package org.xcats.frc.lib.devices.actuation;

import org.xcats.frc.lib.devices.XLimitedDevice;

/**
 * An interface that all limited actuators (Linear, pneumatic, etc) should
 * implement
 */
public interface XLimitedActuator extends XActuator, XLimitedDevice {
}
