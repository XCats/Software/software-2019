package org.xcats.frc.lib.devices.sensors;

public interface XInu {

	enum GyroDirection {
		Yaw, Pitch, Roll
	}

	void setYaw(double yaw);

	double getYaw();

	double getRawYaw();

	double getRawPitch();

	double getRawRoll();

	void pushNetworkTables();

}
