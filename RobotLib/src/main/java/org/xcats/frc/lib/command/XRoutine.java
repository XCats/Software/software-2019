package org.xcats.frc.lib.command;

import java.util.List;

public interface XRoutine extends XCommand {
	List<XCommand> getCommands();
}
