package org.xcats.frc.lib.devices.sensors;

import org.xcats.frc.lib.devices.XDevice;

public interface XSensor<T> extends XDevice {
	void zero();

	T getCurrentValue(boolean raw);

	T getCurrentValue();
}
