package org.xcats.frc.lib;

import java.util.List;

public abstract class XSystemManager implements XLoopable, XInitializable {
	protected List<XSubsystem> mSubsystems;
	protected XLoopable mCurrentController;

	@Override
	public abstract void initialize();

	public void reInit() {
		mSubsystems.stream().forEach(s -> s.initialize());
	}

	@Override
	public void stop() {
		mSubsystems.stream().forEach(x -> x.stop());
	}

	@Override
	public void getPeriodicInput() {
		mCurrentController.getPeriodicInput();
		mSubsystems.stream().forEach(x -> x.getPeriodicInput());
	}

	@Override
	public void writePeriodicOutput() {
		mCurrentController.writePeriodicOutput();
		mSubsystems.stream().forEach(x -> x.writePeriodicOutput());
	}

	public void setCurrentController(XLoopable newController) {
		mCurrentController = newController;
	}
}
