package org.xcats.frc.lib.devices.sensors.impl;

import org.xcats.frc.lib.devices.sensors.XPotentiometer;

public abstract class XBasePotentiometer implements XPotentiometer {

	protected final String mName;

	protected final double mMinPotValue;
	protected final double mMinPotAngle;
	protected final double mMaxPotValue;
	protected final double mMaxPotAngle;

	protected final double mValueThrow;
	protected final double mAngleThrow;

	public XBasePotentiometer(String name, double minPotValue, double minPotAngle, double maxPotValue,
			double maxPotAngle) {
		mName = name;
		mMinPotValue = minPotValue;
		mMinPotAngle = minPotAngle;
		mMaxPotValue = maxPotValue;
		mMaxPotAngle = maxPotAngle;

		mValueThrow = mMaxPotValue - mMinPotValue;
		mAngleThrow = mMaxPotAngle - mMinPotAngle;
	}

	@Override
	public double getAngle() {
		return mMinPotAngle + (getRawValue() - mMinPotValue) / mValueThrow * mAngleThrow;
	}

	@Override
	public double getTicksFromAngle(double angle) {
		return (angle - mMinPotAngle) * (mValueThrow / mAngleThrow) + mMinPotValue;
	}

}
