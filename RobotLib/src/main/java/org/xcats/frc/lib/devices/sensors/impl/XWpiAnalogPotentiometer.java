package org.xcats.frc.lib.devices.sensors.impl;

import edu.wpi.first.wpilibj.AnalogPotentiometer;

public class XWpiAnalogPotentiometer extends XBasePotentiometer {

	private final AnalogPotentiometer mPotentiometer;

	public XWpiAnalogPotentiometer(String name, int port, double minPotValue, double minPotAngle, double maxPotValue,
			double maxPotAngle) {
		super(name, minPotValue, minPotAngle, maxPotValue, maxPotAngle);
		mPotentiometer = new AnalogPotentiometer(port);
	}

	@Override
	public double getRawValue() {
		return this.mPotentiometer.get();
	}
}
