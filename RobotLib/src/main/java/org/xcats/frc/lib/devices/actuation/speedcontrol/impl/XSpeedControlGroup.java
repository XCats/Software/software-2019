package org.xcats.frc.lib.devices.actuation.speedcontrol.impl;

import java.util.Arrays;
import java.util.List;

import org.xcats.frc.lib.devices.actuation.speedcontrol.XSpeedController;
import org.xcats.frc.lib.devices.actuation.speedcontrol.XSpeedControllerStatus;
import org.xcats.frc.lib.logging.XLogString;

@Deprecated
public class XSpeedControlGroup implements XSpeedController {

	private final String mName;
	private final XSpeedController mMaster;
	private final List<XSpeedController> mFollowers;

	private double mSetPoint;
	private boolean mInverted;
	private double mInvertCoefficient;

	public XSpeedControlGroup(String name, XSpeedController master, XSpeedController... followers) {
		this.mName = name;
		this.mMaster = master;
		this.mFollowers = Arrays.asList(followers);
	}

	@Override
	public String getName() {
		return this.mName;
	}

	@Override
	public void alterSetPoint(double newSetPoint) {
		this.mSetPoint = newSetPoint;
		mMaster.alterSetPoint(this.mSetPoint);
		mFollowers.forEach(s -> s.alterSetPoint(this.mSetPoint));
	}

	@Override
	public double getSetPoint() {
		return this.mSetPoint;
	}

	@Override
	public void setInverted() {
		this.mInverted = !this.mInverted;
		this.mInvertCoefficient = -1 * this.mInvertCoefficient;
		mMaster.setInverted();
		mFollowers.forEach(s -> s.setInverted());
	}

	@Override
	public boolean isInverted() {
		return this.mInverted;
	}

	public List<XSpeedController> getFollowers() {
		return this.mFollowers;
	}

	public XSpeedController getMaster() {
		return this.mMaster;
	}

	@Override
	public void stop() {
		this.alterSetPoint(0);
	}

	@Override
	public XSpeedControllerStatus getStatus() {
		return null;
	}

	@Override
	public void initialize() {
		this.mMaster.initialize();
		this.mFollowers.stream().forEach(f -> f.initialize());
	}

	@Override
	public XLogString getLogString() {
		return null;
	}
}
