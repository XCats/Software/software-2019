package org.xcats.frc.lib.devices.sensors;

public interface XIntegratableSensor<T> extends XSensor<T> {
	T getCumulativeValue(boolean raw);

	T getCumulativeValue();
}
