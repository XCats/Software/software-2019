package org.xcats.frc.lib.wrappers;

import edu.wpi.first.wpilibj.Joystick;

public class XJoystick extends Joystick {
	private double mOutputConstant = 1;

	/**
	 * Construct an instance of a joystick. The joystick index is the USB port on
	 * the drivers station.
	 *
	 * @param port The port on the Driver Station that the joystick is plugged into.
	 */
	public XJoystick(int port) {
		super(port);
	}

	public void setInverted(boolean inverted) {
		if (inverted) {
			this.mOutputConstant = -1;
		} else {
			this.mOutputConstant = 1;
		}
	}

	@Override
	public double getRawAxis(int axis) {
		return super.getRawAxis(axis) * this.mOutputConstant;
	}

}
