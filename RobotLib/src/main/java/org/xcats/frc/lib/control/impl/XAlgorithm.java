package org.xcats.frc.lib.control.impl;

public abstract class XAlgorithm implements Runnable {
	public double mSetPoint;
	public double mCurrentValue;

	@Override
	public void run() {
		while (!Thread.interrupted()) {
			exec();
		}
	}

	public abstract void exec();
}
