package org.xcats.frc.lib;

/**
 * Defines a set of methods for all devices (sensors, arms, etc.) that have two
 * states.
 *
 * In the case of an arm this could be up or down, open or closed. In the case
 * of a sensor this could be on or off, rotating or not rotating.
 */
public interface XBinaryState {
	boolean getState();
}
