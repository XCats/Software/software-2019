package org.xcats.frc.lib.control;

import org.xcats.frc.lib.control.impl.XAlgorithm;
import org.xcats.frc.lib.devices.actuation.speedcontrol.XSpeedController;
import org.xcats.frc.lib.devices.sensors.XSensor;

public abstract class XBaseController implements XController {

	protected XSpeedController mMotor;
	protected XSensor<Double> mEncoder;
	private final XAlgorithm mAlgorithm;

	private double mSetPoint;

	public XBaseController(XSpeedController motor, XSensor<Double> encoder, XAlgorithm algorithm) {
		this.mMotor = motor;
		this.mEncoder = encoder;
		this.mAlgorithm = algorithm;
	}

	public void run() {
		Thread t = new Thread(mAlgorithm);
		t.start();
	}

	@Override
	public void alterSetPoint(double newSetPoint) {
		this.mSetPoint = newSetPoint;
	}

	@Override
	public double getSetPoint() {
		return this.mSetPoint;
	}
}
