package org.xcats.frc.lib;

import org.xcats.frc.lib.logging.XLogString;

/**
 * An interface implemented by any class that should be able to log to various log outputs.
 */
public interface XLoggable {
	XLogString getLogString();
}
