package org.xcats.frc.lib.devices;

import org.xcats.frc.lib.XBinaryState;

public interface XBinaryDevice extends XDevice, XBinaryState {

}
