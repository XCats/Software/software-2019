package org.xcats.frc.lib.devices.sensors.impl;

import org.xcats.frc.lib.devices.sensors.XLimitSwitch;

import edu.wpi.first.wpilibj.DigitalInput;

public class XWpiLimitSwitch implements XLimitSwitch {

	private final String mName;
	private final DigitalInput mInput;

	public XWpiLimitSwitch(String name, int port) {
		this.mInput = new DigitalInput(port);
		this.mName = name;
	}

	@Override
	public boolean getCurrentValue() {
		return this.mInput.get();
	}

	@Override
	public String getName() {
		return this.mName;
	}
}
