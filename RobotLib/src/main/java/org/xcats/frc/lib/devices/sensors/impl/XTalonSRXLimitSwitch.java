package org.xcats.frc.lib.devices.sensors.impl;

import org.xcats.frc.lib.devices.sensors.XLimitSwitch;

import com.ctre.phoenix.motorcontrol.can.TalonSRX;

public class XTalonSRXLimitSwitch implements XLimitSwitch {

	private final String mName;
	private final TalonSRX mTalon;
	private final boolean mIsFwdSwitch;

	public XTalonSRXLimitSwitch(String name, TalonSRX talon, boolean isFwdSwitch) {
		mName = name;
		mTalon = talon;
		mIsFwdSwitch = isFwdSwitch;
	}

	@Override
	public boolean getCurrentValue() {
		return mIsFwdSwitch ? mTalon.getSensorCollection().isFwdLimitSwitchClosed()
				: mTalon.getSensorCollection().isRevLimitSwitchClosed();
	}

	@Override
	public String getName() {
		return mName;
	}

}
