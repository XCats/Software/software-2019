package org.xcats.frc.lib.devices.sensors.impl;

import com.revrobotics.CANEncoder;
import com.revrobotics.CANSparkMax;
import org.xcats.frc.lib.devices.sensors.XEncoder;

public class XSparkMaxEncoder extends XBaseEncoder {
	private static final String ENCODER_TYPE = "SparkMax";
	private static final double ENCODER_TPR = 4096; // Encoder ticks per revolution

	private final CANEncoder mEncoder;
	private double mEncoderStartingValue;


	public XSparkMaxEncoder(String name, CANSparkMax sparkMax, double distancePerRotation, XEncoder.Units distanceUnit) {
		super(name, ENCODER_TYPE, ENCODER_TPR, distancePerRotation, distanceUnit);
		mEncoder = sparkMax.getEncoder();
	}

	@Override
	public void zero() {
		this.mEncoderStartingValue = mEncoder.getPosition();
	}

	@Override
	public double getRawDistanceTicks() {
		return this.mEncoder.getPosition() - this.mEncoderStartingValue;
	}

	@Override
	public double getRawVelocityTicks() {
		return this.mEncoder.getVelocity();
	}
}
