package org.xcats.frc.lib.devices.actuation.speedcontrol;

import org.xcats.frc.lib.CAN.XCANDevice;

@Deprecated
public interface XCANSpeedController extends XCANDevice, XSpeedController {
	void setBrakeMode(XBrakeMode brakeMode);

	XBrakeMode getBrakeMode();

	void setRampingRate(double timeToFullSpeed);
}
