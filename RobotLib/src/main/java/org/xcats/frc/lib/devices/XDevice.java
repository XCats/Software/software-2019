package org.xcats.frc.lib.devices;

import org.xcats.frc.lib.XInitializable;
import org.xcats.frc.lib.XLoggable;

/**
 * Interface for devices such as sensors, speed controllers, etc. to implement
 */
public interface XDevice extends XLoggable, XInitializable {
	String getName();
}
