package org.xcats.frc.lib.logging;

import java.time.Instant;
import java.util.Date;

@Deprecated
public class XLogString {

	private final Date mTimestamp;
	private final String mKey;
	private final String mValue;

	public XLogString(String key, String value) {
		this.mKey = key;
		this.mValue = value;
		this.mTimestamp = Date.from(Instant.now());
	}

	public String getKey() {
		return mKey;
	}

	public String getValue() {
		return mValue;
	}

	public Date getTimestamp() {
		return mTimestamp;
	}
}
