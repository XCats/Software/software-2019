package org.xcats.frc.lib.devices;

/**
 * Defines a set of methods for any mechanism on the robot that is limited, by
 * limit switches or otherwise
 */
public interface XLimitedDevice {
	boolean isAtLowerLimit();

	boolean isAtUpperLimit();
}
