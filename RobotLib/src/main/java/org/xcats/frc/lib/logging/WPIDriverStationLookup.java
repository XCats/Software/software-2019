package org.xcats.frc.lib.logging;

import edu.wpi.first.wpilibj.DriverStation;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.lookup.StrLookup;

@Plugin(name = "ds", category = "Lookup")
public class WPIDriverStationLookup implements StrLookup {

	@Override
	public String lookup(String key) {
		String out;

		DriverStation driverStation = DriverStation.getInstance();

		switch (key) {
			case "matchTime":
				out = Double.toString(driverStation.getMatchTime());
				break;
			case "matchNum":
				out = Integer.toString(driverStation.getMatchNumber());
				break;
			case "eventName":
				out = driverStation.getEventName();
				break;
			case "controlLocation":
				out = Integer.toString(driverStation.getLocation());
				break;
			default:
				out = "";
				break;
		}

		return out;
	}

	@Override
	public String lookup(LogEvent event, String key) {
		return this.lookup(key);
	}
}
