package org.xcats.frc.lib;

public interface XLoopable {
	/**
	 * Runs every robot cycle, reading values from inputs (Controllers, sensors)
	 */
	void getPeriodicInput();

	/**
	 * Runs every robot cycle, writing values to outputs (Motors, displays)
	 * based on the inputs read in {@link #getPeriodicInput()}.
	 */
	void writePeriodicOutput();
}
