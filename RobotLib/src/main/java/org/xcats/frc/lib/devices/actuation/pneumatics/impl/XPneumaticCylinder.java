package org.xcats.frc.lib.devices.actuation.pneumatics.impl;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import org.xcats.frc.lib.devices.XBinaryDevice;
import org.xcats.frc.lib.logging.XLogString;

public class XPneumaticCylinder implements XBinaryDevice {

	private DoubleSolenoid mSolenoid;
	private String mName;
	private final DoubleSolenoid.Value mExtendedValue;
	private final DoubleSolenoid.Value mRetractedValue;

	public XPneumaticCylinder(String name, DoubleSolenoid solenoid) {
		this(name, solenoid, false);
	}

	public XPneumaticCylinder(String name, DoubleSolenoid solenoid, boolean reversed) {
		this.mSolenoid = solenoid;
		this.mName = name;
		if (reversed) {
			this.mExtendedValue = DoubleSolenoid.Value.kReverse;
			this.mRetractedValue = DoubleSolenoid.Value.kForward;
		} else {
			this.mExtendedValue = DoubleSolenoid.Value.kForward;
			this.mRetractedValue = DoubleSolenoid.Value.kReverse;
		}
	}

	public void extend() {
		this.mSolenoid.set(mExtendedValue);
	}

	public void retract() {
		this.mSolenoid.set(mRetractedValue);
	}

	public void setExtended(boolean extended) {
		if (extended) {
			this.extend();
		} else {
			this.retract();
		}
	}

	@Override
	public boolean getState() {
		return mSolenoid.get() == mExtendedValue;
	}

	@Override
	public String getName() {
		return this.mName;
	}

	@Override
	public void initialize() {
		mSolenoid.set(mRetractedValue);
	}

	@Override
	public void stop() {
		this.mSolenoid.set(DoubleSolenoid.Value.kOff);
	}

	@Override
	public XLogString getLogString() {
		return null;
	}
}
