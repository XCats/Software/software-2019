package org.xcats.frc.lib.devices.actuation;

import org.xcats.frc.lib.devices.XDevice;

/**
 * The interface all actuators (Pneumatics, motors, etc) should implement
 */
public interface XActuator extends XDevice {
	void changeState();

	boolean isExtended();
}
