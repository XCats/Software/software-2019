package org.xcats.frc.lib.mutators;

import org.xcats.frc.lib.devices.sensors.XIntegratableSensor;
import org.xcats.frc.lib.devices.sensors.XSensor;
import org.xcats.frc.lib.logging.XLogString;

/**
 * Acts as a mutator class that connects an encoder to a gearbox, returning the
 * number of rotations of the output shaft
 */
public class XGearbox implements XIntegratableSensor<Double> {


	public enum SensorPosition {
		INPUT, OUTPUT;
	}

	private final XIntegratableSensor<Double> mSensor;
	private final double mRatio;
	private final SensorPosition mSensorPosition;

	public XGearbox(XIntegratableSensor<Double> sensor, double ratio, SensorPosition pos) {
		this.mSensor = sensor;
		this.mSensorPosition = pos;
		switch (pos) {
			case INPUT:
				this.mRatio = 1.0 / ratio;
				break;
			default:
				this.mRatio = ratio / 1;
				break;
		}
	}

	public SensorPosition getSensorPos() {
		return this.mSensorPosition;
	}

	public Double getRatio() {
		return this.mRatio;
	}

	public XSensor<Double> getSensor() {
		return this.mSensor;
	}

	@Override
	public void zero() {
		this.mSensor.zero();
	}

	@Override
	public Double getCurrentValue(boolean raw) {
		if (raw) {
			return this.mSensor.getCurrentValue(raw);
		} else {
			return this.mSensor.getCurrentValue() * mRatio;
		}
	}

	@Override
	public Double getCurrentValue() {
		return this.getCurrentValue(false);
	}

	@Override
	public Double getCumulativeValue(boolean raw) {
		if (raw) {
			return this.mSensor.getCumulativeValue(raw);
		} else {
			return this.mSensor.getCumulativeValue() * mRatio;
		}
	}

	@Override
	public Double getCumulativeValue() {
		return this.getCumulativeValue(false);
	}

	@Override
	public String getName() {
		return this.mSensor.getName();
	}

	@Override
	public void initialize() {

	}

	@Override
	public void stop() {

	}

	@Override
	public XLogString getLogString() {
		return null;
	}
}
