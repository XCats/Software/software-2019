package org.xcats.frc.lib.devices.sensors.impl;

import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

public class XTalonSRXPotentiometer extends XBasePotentiometer {

	private final TalonSRX mTalon;

	public XTalonSRXPotentiometer(String name, TalonSRX talon, double minPotValue, double minPotAngle,
			double maxPotValue, double maxPotAngle) {
		super(name, minPotValue, minPotAngle, maxPotValue, maxPotAngle);
		mTalon = talon;
		mTalon.configSelectedFeedbackSensor(FeedbackDevice.Analog);
	}

	@Override
	public double getRawValue() {
		return mTalon.getSelectedSensorPosition();
	}
}
