package org.xcats.frc.lib.devices.sensors.impl;

import com.revrobotics.CANDigitalInput;
import com.revrobotics.CANSparkMax;
import org.xcats.frc.lib.devices.sensors.XLimitSwitch;

public class XSparkMaxLimitSwitch implements XLimitSwitch {
	private final String mName;
	private final CANSparkMax mSparkMax;
	private final boolean mIsFwdSwitch;
	private final CANDigitalInput.LimitSwitchPolarity mSwitchPolarity;

	public XSparkMaxLimitSwitch(String name, CANSparkMax sparkMax, boolean isFwdSwitch, boolean switchPolarity) {
		mName = name;
		mSparkMax = sparkMax;
		mIsFwdSwitch = isFwdSwitch;
		mSwitchPolarity = switchPolarity ? CANDigitalInput.LimitSwitchPolarity.kNormallyClosed : CANDigitalInput.LimitSwitchPolarity.kNormallyOpen;
	}

	@Override
	public boolean getCurrentValue() {
		return mIsFwdSwitch ? mSparkMax.getForwardLimitSwitch(mSwitchPolarity).get()
				: mSparkMax.getReverseLimitSwitch(mSwitchPolarity).get();
	}

	@Override
	public String getName() {
		return mName;
	}
}
