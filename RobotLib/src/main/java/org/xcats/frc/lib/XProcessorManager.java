package org.xcats.frc.lib;

import org.xcats.frc.lib.processors.XProcessor;

import java.util.List;

public abstract class XProcessorManager implements XLoopable {
	protected List<XProcessor> mProcessors;

	@Override
	public void getPeriodicInput() {
		mProcessors.stream().forEach(x -> x.getPeriodicInput());
	}

	@Override
	public void writePeriodicOutput() {
		mProcessors.stream().forEach(x -> x.writePeriodicOutput());
	}
}
