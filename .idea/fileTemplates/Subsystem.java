package ${PACKAGE_NAME};

import org.usfirst.frc.xcats.lib.util.XSubsystem;
import org.usfirst.frc.xcats.lib.util.devices.actuation.XActuator;
import org.usfirst.frc.xcats.lib.util.devices.actuation.speedcontrol.XSpeedController;
import org.usfirst.frc.xcats.lib.util.devices.sensors.XSensor;
import org.usfirst.frc.xcats.lib.util.logging.XLogString;

/**
 * This class provides an template implementation of the XSubsystem interface.
 * It uses a controller / state design pattern.
 */
@SuppressWarnings("PMD")
public class ${NAME} implements XSubsystem {
	/**
	 * A controller class that contains all variables that are modified to control
	 * the subsystem
	 */
	public class Controller {
		
	}

	/**
	 * A state class is built that will represent the state of the subsystem
	 */
	public class State {
		
	}

	// Holds the desired state of the subsystem
	private State mDesiredState;

	// Holds the actual state of the subsystem
	private State mCurrentState;

	private Controller mController;

	// Injected Dependencies
	

	public ${NAME}() {
		
	}

	public State getDesiredState() {
		return this.mDesiredState;
	}

	public State getCurrentState() {
		return this.mCurrentState;
	}

	public Controller getController() {
		return this.mController;
	}

	@Override
	public void stop() {
		// No motion state here
	}

	@Override
	public void initialize() {
		/*
		 * This is where mDesiredState should be set to whatever the default state for the subsystem
		 * should be, and mActualState should be initialized with values read from the
		 * subsystem itself, or with values from mDesiredState.
		 */
	}

	@Override
	public XLogString getLogString() {
		return null;
	}

	@Override
	public void getPeriodicInput() {
		// Read values from controller into the desired state

		// Read value from sensors into current state (because this is current)
	}

	@Override
	public void writePeriodicOutput() {
		/*
		 * Compare desired state and current state, and then make appropriate changes to
		 * subsystem stuff
		 */
	}
}
